#include "music.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void _play_sound(char *file_name);

pthread_t *play_sound(char *file_name){
	
	pthread_t *music_thread;

	music_thread = (pthread_t *) malloc(sizeof(pthread_t));
	if (music_thread == NULL) return NULL;

	pthread_create(music_thread, NULL, (void * )_play_sound, file_name);

	return music_thread;
}

void _play_sound (char *file_name){
	
	char *buf;

	buf = (char *) malloc(strlen(PLAY_COMMAND) + strlen(file_name) + 1);
	if (buf == NULL) return;

	sprintf(buf, PLAY_COMMAND "%s", file_name);

	system(buf);

	free(buf);
}

void stop_sound(){
	
	system(STOP_COMMAND);
}