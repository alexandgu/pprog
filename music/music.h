/**
 * @file music.h
 * @author     Carlos Gómez-Lobo Hernaiz
 * @date       10 Jan 2019
 * @brief      Contains the prototypes and structures of the music ADT.
 *
 *             The music ADT manages the music that is being played during the
 *             game.
 */

#include "../utils/types.h"
#include <pthread.h>

#define JAZZ_TRACK "jazztrack.wav"
#define TRAP_TRACK "traptrack.wav"
#define PLAY_COMMAND "play -q music/tracks/"
#define STOP_COMMAND "killall play"


/**
 * @brief      Plays the track passed as argument.
 *
 * @param      file_name  The name of the track to be played.
 *
 * @return     Returns the thread created for playing the music.
 */
pthread_t *play_sound(char *file_name);

/**
 * @brief      Stops the track that is being played at the moment.
 */
void stop_sound ();