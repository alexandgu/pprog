#include "game.h"


Game * game_ini(Game_mode mode){
    Game * ret;
    ret = (Game *)malloc(sizeof(Game));
    ret->mode = mode;
    ret->foreign = list_ini(NULL, NULL, NULL, NULL);
    return ret;
}

void game_setme(Game *g, Player *p){
    assert(g != NULL);
    assert(p != NULL);
    g->me = p;
}
void game_setnetwork(Game *g, void * network){
    assert(g != NULL);
    assert(network != NULL);
    g->network = network;
}

Player * game_getforeigneplayer(Game * game, short id){
    assert(game != NULL);
    assert(id >= 0);
    return list_get(game->foreign, id-1);
}
short game_getid(Game * g){ 
    return player_get_id(g->me);
}
Player * game_getme(Game * game){
    assert(game != NULL);
    return game->me;
}
Map * game_getmap(Game * game){
    assert(game != NULL);
    return game->map;
}

void game_setmap(Game * g, Map * m){
    assert(g != NULL);
    assert(m != NULL);
    g->map = m;
}

Status game_addforeign(Game *g, Player * foreign){
    assert(g != NULL);
    assert(foreign != NULL);
    return list_insertFirst(g->foreign, foreign);
}

short game_getsizeforeign(Game *g){
    assert(g != NULL);
    return list_size(g->foreign);
}
