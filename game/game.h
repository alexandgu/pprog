#ifndef GAME_H
#define GAME_H

#include "../interface/player.h"
#include "../construction/map.h"
#include "../utils/includes.h"

typedef struct _Game Game;
struct _Game{
    Player * me;
    Map * map;
    List * foreign;
    Game_mode mode;
    void * network;
};
Game * game;

Game * game_ini(Game_mode mode);
Game * game_destruct(Game * game);

Player * game_getforeigneplayer(Game *, short id);
void game_setme(Game *, Player *);
void game_setnetwork(Game *, void *);
short game_getid(Game *);
Player * game_getme(Game *);
Map * game_getmap(Game *);
void game_setmap(Game *, Map *);
Status game_addforeign(Game *, Player *);
short game_getsizeforeign(Game *);

#endif
