##################################

CC = gcc -pthread -g 
CFLAGS =  -ansi -pedantic -Wall
valgrind = valgrind --leack-check:full -v --log-file="verror.log"

EXE = play 

##################################

all : $(EXE)

testgame: interface.o block.o refresh.o client_interface.o server_interface.o player.o map.o interaction.o functions.o list.o stack.o building.o testgame.o interface_manager.o permutaciones.o utils.o printer.o queue.o game.o music.o
	$(CC) interface.o interaction.o client_interface.o server_interface.o refresh.o player.o printer.o queue.o block.o map.o functions.o list.o stack.o building.o game.o testgame.o interface_manager.o permutaciones.o utils.o music.o -o testgame -lm
testgame.o: testgame.c interface/interface_manager.h utils/includes.h construction/map.h network/client_interface.h network/server_interface.h 
	$(CC)  -c testgame.c

play: interface.o block.o music.o refresh.o client_interface.o server_interface.o player.o map.o interaction.o functions.o list.o stack.o building.o test2.o interface_manager.o permutaciones.o utils.o printer.o queue.o game.o
	$(CC) interface.o interaction.o music.o client_interface.o server_interface.o refresh.o player.o printer.o queue.o block.o map.o functions.o list.o stack.o building.o game.o test2.o interface_manager.o permutaciones.o utils.o -o play -lm

test2.o: test2.c interface/interface_manager.h
	$(CC)  -c test2.c

test: interface.o block.o map.o printer.o queue.o functions.o list.o stack.o building.o test.o interface_manager.o permutaciones.o utils.o interaction.o game.o player.o refresh.o
	$(CC)  interface.o block.o printer.o queue.o map.o functions.o list.o stack.o building.o test.o interface_manager.o permutaciones.o utils.o interaction.o game.o player.o refresh.o -o test

test.o: test.c interface/interface_manager.h game/game.h interface/interaction.h
	$(CC)  -c test.c
	

testclient: client_interface.o testclient.o
	$(CC) client_interface.o testclient.o -o testclient

docu: */*.h
	doxygen Doxyfile

testclient.o: testclient.c network/client_interface.h
	$(CC) $(CLFAGS) -c testclient.c

testserver:  server_interface.o queue.o player.o testserver.o
	$(CC)  server_interface.o queue.o player.o   testserver.o -o testserver

testserver.o: testserver.c network/server_interface.h
	$(CC) $(CLFAGS) -c testserver.c

game.o: game/game.c game/game.h utils/includes.h construction/map.h interface/player.h
	$(CC) $(CFLAGS) -c game/game.c

interface_manager.o: interface/interface_manager.c interface/interface_manager.h interface/interface.h
	$(CC) -c interface/interface_manager.c

interaction.o: interface/interaction.h interface/interaction.c construction/building.h construction/block.h utils/includes.h network/client_interface.h
	$(CC) -c interface/interaction.c

interface.o: interface/interface.c interface/interface.h utils/types.h
	$(CC) -c interface/interface.c

printer.o: interface/printer.c interface/printer.h utils/queue.h
	$(CC)  -c interface/printer.c

refresh.o: interface/refresh.h interface/refresh.c construction/map.h construction/block.h utils/permutaciones.h
	$(CC) $(CFLAGS) -c interface/refresh.c

player.o: interface/player.c interface/player.h construction/building.h utils/includes.h utils/list.h
	$(CC) $(CFLAGS) -c interface/player.c

music.o: music/music.h music/music.c
	$(CC) -c music/music.c

block.o: construction/block.c construction/block.h utils/list.h utils/includes.h
	$(CC) $(CFLAGS) -c construction/block.c

queue.o: utils/queue.c utils/queue.h utils/includes.h utils/functions.h
	$(CC) $(CFLAGS) -c utils/queue.c

client_interface.o: network/client_interface.c network/client_interface.h utils/includes.h
	$(CC) $(CFLAGS) -c network/client_interface.c

server_interface.o: network/server_interface.c network/server_interface.h interface/player.h utils/queue.h utils/includes.h
	$(CC) $(CFLAGS) -c network/server_interface.c

building.o: construction/building.c construction/building.h utils/includes.h
	$(CC) $(CFLAGS) -c construction/building.c

map.o: construction/map.c construction/map.h utils/includes.h construction/block.h
	$(CC) $(CFLAGS) -c construction/map.c

functions.o: utils/functions.c utils/functions.h utils/includes.h
	$(CC) $(CFLAGS) -c utils/functions.c

list.o: utils/list.c utils/list.h utils/includes.h
	$(CC) $(CFLAGS) -c utils/list.c

stack.o: utils/stack.c utils/stack.h utils/includes.h utils/functions.h
	$(CC) $(CFLAGS) -c utils/stack.c

permutaciones.o: utils/permutaciones.c utils/permutaciones.h
	$(CC) $(CFLAGS) -c utils/permutaciones.c

utils.o: utils/utils.c utils/utils.h
	$(CC) $(CFLAGS) -c utils/utils.c




clean:
	rm -rf $(EXE) *.o */*.gch

run: 
	@echo ">>Executing test"
	./test 2>error.log

run2:
	@echo ">>Executing test"
	./test2 2>error.log

runv:
	@echo ">>Executing test with Valgrind"
	$(valgrind) ./test

runv2:
	@echo ">>Executing test2 with Valgrind"
	$(valgrind) ./test2 2>error.log

rungameclient:
	./testgame 2>errorclient.log

runvgameclient:
	$(valgtind) ./testgame 2>errorclient.log

rungameserver:
	./testgame 2>errorclient.log

runvgameserver:
	$(valgrind) ./testgame 2>errorclient.log