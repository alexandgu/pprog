#include "building.h"

/*** CONSTRUCTOR AND DESTRUCTOR*/
Building * building_ini(Building_type type, char owner, Block * parent)
{
    Building * ret;
    assert(owner >-1);
    assert(parent!=NULL);
    ret = (Building *)malloc(sizeof(Building));
    if(ret != NULL){
        ret->type = type;
        ret->drugs = 0L;
        ret->level = 0; 
        ret->owner = owner;
        ret->parent = parent;
    }
    return ret;
}
void building_free(void * b)
{   
    free(b);
}
void * building_copy(void* b)
{   int i;
    Building * ret;
    assert(b != NULL);
    ret = building_ini(((Building *)b)->type, ((Building *)b)->owner, ((Building *)b)->parent );
    /*We set the level*/
    for(i=0;i<((Building *)b)->level;i++){
        building_upgrade(ret);
    }
    return ret;
}

/*SETTER*/
void building_upgrade(Building * b)
{
    assert(b != NULL);
    b->level++;
}

void building_set_level(Building *b, int level){
    assert(b!=NULL);
    b->level=level;
}

void building_set_vigilance(Building *b, long vigilance){
    assert(b!=NULL);
    b->vigilance=vigilance;
}

void building_set_drugs(Building *b, unsigned long drugs){
    assert(b != NULL);
    b->drugs = drugs;
}
/*** GETTERS***/

unsigned long building_getdrugs(Building *b){
    assert(b != NULL);
    return b->drugs; 
}

long building_getvigilance(Building *b){
    assert(b!= NULL);
    return b->vigilance;
}

unsigned short building_getlevel(Building *b)
{
    assert(b!= NULL);
    return b-> level;
}
Building_type building_gettype(Building *b)
{
    if (b == NULL) return NULL_B;
    return b-> type;
}
unsigned char building_getowner(Building *b)
{
    assert(b!= NULL);
    return b->owner;
}

Block * building_getparent(Building *b)
{
    assert(b!=NULL);
    return b->parent;
}

Status building_printtofile(FILE *filepointer, void* voidpointer)
{
    Building *buildingpointer = (Building *)voidpointer;
    assert(buildingpointer != NULL);
    assert(filepointer != NULL);
    if(fwrite(&buildingpointer->level, sizeof(buildingpointer->level), 1, filepointer) != 1)
        return ERROR;
    if(fwrite(&buildingpointer->type, sizeof(buildingpointer->type), 1, filepointer) != 1)
        return ERROR;
    if(fwrite(&buildingpointer->owner, sizeof(buildingpointer->owner), 1, filepointer) != 1)
        return ERROR;
    if(fwrite(&buildingpointer->drugs, sizeof(buildingpointer->drugs), 1, filepointer) != 1)
        return ERROR;
    return OK;
}

Building * building_readfromfile(FILE *filepointer, Block * blockpointer) 
{
    Building_type type;
    unsigned short level;
    unsigned char owner;
    unsigned long drugs;
    Building * buildingpointer;
    assert(filepointer != NULL);
    if(fread(&level, sizeof(level), 1, filepointer) != 1)
        return NULL;
    if(fread(&type, sizeof(type), 1, filepointer) != 1)
        return NULL;
    if(fread(&owner, sizeof(owner), 1, filepointer) != 1)
        return NULL;
    if(fread(&drugs, sizeof(drugs), 1, filepointer) != 1)
        return NULL;
    buildingpointer = building_ini(type, owner, blockpointer);
    if(buildingpointer != NULL)
    {
        buildingpointer->level = level;
        buildingpointer->drugs = drugs;
    }
    return buildingpointer;
}


