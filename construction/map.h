#ifndef MAP_H
#define MAP_H

#include "../utils/utils.h"
#include "../utils/permutaciones.h"
#include "../utils/includes.h"
#include "block.h"

#define MAP_HEIGHT 100
#define MAP_WIDTH 100


typedef struct _Map Map;
/*will be hidden*/
struct _Map{
    Block ***cells;
    unsigned short height;
    unsigned short width;
    int current_coord_x;
    int current_coord_y;
    int offset_coord_x;
    int offset_coord_y;

};
/* Map_ini intializes map structure allocates memory for it
 * Input: short height 
 *        short width
 * Output: Map pointer with all blocks allocated
 */ 
Map * map_ini(unsigned short, unsigned short);

/* Map_destruct frees memory of the map structure
 * Input: (nullable)Map pointer 
 * Output:
 */ 
void map_destruct(Map *);

/* Returns a pointer to a block that is contained in the map (THE BLOCK ITSELF NOT A COPY)
 * Input: (not nullable) map pointer if pointer == null -> undefined behaviour 
 *        short height, short width (position in the array)
 * Output: Block pointer 
 */ 
Block* map_getBlock(Map*, unsigned short, unsigned short);

/*
Status map_resize(Map*, unsigned short, unsigned short);
*/

/* Prints a map to a binary file, printing values directly 
 * Input: (not nullable) map pointer if pointer == null -> undefined behaviour
 *        (not nullable) char pointer if pointer == null -> undefined behaviour 
 *              FILE will be open, if a file already exists it will be overwritten and erased
 * Output : OK, ERROR
 */
Status map_savetofile(Map *, char *);

/* Reads a map from a binary file, reading values in the same format that block_printtofile
 * Input: (not nullable) char pointer if pointer == null -> undefined behaviour 
 *              FILE will be open with modifier r
 * Output: a new map created with the data from the file
 */ 
Map * map_readfromfile(char *);
/*debugging function wont be here*/
void map_print(Map* m);
int aleat_num(int inf, int sup);
Status map_joincitywithhighway(Map * mappointer, int * departure, int * arrival);
Status map_joinvillage(Map *, unsigned short, unsigned short);
Status map_createVillagePath(Map *,unsigned short, unsigned short, unsigned short, unsigned short);

int map_getCurrentCoordY (Map *map);

int map_getCurrentCoordX (Map *map);

int map_getOffsetCoordY (Map *map);

int map_getOffsetCoordX (Map *map);

Status map_setCurrentCoordY (Map *map, int y);

Status map_setCurrentCoordX (Map *map, int x);

Status map_setOffsetCoordY (Map *map, int y);

Status map_setOffsetCoordX (Map *map, int x);

Status map_decreaseCurrentCoordX (Map *map);

Status map_decreaseCurrentCoordY (Map *map);

Status map_increaseCurrentCoordX (Map *map);

Status map_increaseCurrentCoordY (Map *map);

Status map_decreaseOffsetCoordX (Map *map);

Status map_decreaseOffsetCoordY (Map *map);

Status map_increaseOffsetCoordX (Map *map);

Status map_increaseOffsetCoordY (Map *map);

Block *map_getCurrentBlock (Map * map);

#endif
