#include "block.h"


Block * block_ini(Block_type type, unsigned short x, unsigned short y){
    Block *ret;
    assert(type < MAX_BLOCK_TYPE);
    ret = (Block *)malloc(sizeof(Block));
    if(ret != NULL)
    {
        ret->x = x;
        ret->y = y;
        switch(type)
        {
        case CITY:
            ret->transport = CITY_TRANSPORT;
            ret->type = CITY;
            ret->level = 0;
            ret->buildings = list_ini(building_free, NULL, building_printtofile, NULL);
            break;
        case DESERT:
            ret->transport = DESERT_TRANSPORT;
            ret->type = DESERT;
            ret->level = 0;
            ret->buildings = list_ini(building_free, NULL, building_printtofile, NULL);
            break;
        case HIGHWAY:
            ret->transport = HIGHWAY_TRANSPORT;
            ret->type = HIGHWAY;
            ret->level = 0;
            ret->buildings = list_ini(building_free, NULL, building_printtofile, NULL);
            break;
        case VILLAGE: 
            ret->transport = VILLAGE_TRANSPORT;
            ret->type = VILLAGE;
            ret->level = 0;
            ret->buildings = list_ini(building_free, NULL, building_printtofile, NULL);
            break;
        default:
            break;
        } 
    }
    return ret;
}

void block_destruct(Block * bl)
{
    if(bl != NULL)
    {
        list_free(bl->buildings);
    }
    free(bl);
}

void block_upgrade(Block *b){
    assert(b != NULL); 
    b->level++;
}
unsigned short block_gettransport(Block *bl)
{
    return bl->transport;
}

unsigned char block_gettype(Block *bl)
{
    if(bl == NULL) return CITY;
    return bl->type;
}
unsigned short block_getlevel(Block *bl)
{
    return bl->level;
}

int block_getnumbuildings(Block *bl)
{
    if (bl == NULL) return 0;
    return list_size(bl->buildings);
}

Building * block_getBuilding(Block *bl, int index, char buildingtype)
{
    Building * ret;
    int i;
    assert(bl != NULL);
    if(index>-1)
    {
        return (Building *)list_get(bl->buildings, index);
    }
    else
    {
        for(i = 0; i < list_size(bl->buildings);i++)
        {
            ret = (Building *)list_get(bl->buildings, i);
            if(building_gettype(ret) == buildingtype)
            {
                return ret;
            }
        }
    }
    return NULL;
}

int block_addBuilding(Block * bl, char buildingtype, char owner)
{
    Building * build;
    build = building_ini(buildingtype, owner, bl);
    if(list_insertLast(bl->buildings, build) == ERROR)
    {
        building_free(build);
        return ERROR;
    }
    return OK;
}

Status block_deleteBuilding(Block *bl, int index, char buildingtype)
{
    Building *build;
    int i;
    assert(bl);
    if(index>-1)
    {
        build = (Building *)list_get(bl->buildings, index);
        assert(build != NULL);
        building_free(build);
        return OK;
    }
    else
    {
        for(i = 0; i < list_size(bl->buildings);i++)
        {
            build = (Building *)list_get(bl->buildings, index);
            if(building_gettype(build) == buildingtype)
            {
                build = (Building *)list_extractIndex(bl->buildings, i);
                building_free(build);
                return OK;
            }
        }
    }
    return ERROR;
}

Status block_printtofile(Block * blockpointer, FILE * filepointer)
{
    int size;
    int i;
    assert(blockpointer != NULL);
    assert(filepointer != NULL);
    if(fwrite(&blockpointer->transport, sizeof(blockpointer->transport), 1, filepointer) != 1)
        return ERROR;
    if(fwrite(&blockpointer->type, sizeof(blockpointer->type), 1, filepointer) != 1)
        return ERROR;
    if(fwrite(&blockpointer->level, sizeof(blockpointer->level), 1, filepointer) != 1)
        return ERROR;
    size = list_size(blockpointer->buildings);
    if(fwrite(&size, sizeof(size), 1, filepointer) != 1)
        return ERROR;
    for(i = 0; i < size; i++)
        if(building_printtofile(filepointer, (Building *)list_get(blockpointer->buildings, i)) == ERROR)
            return ERROR;
    return OK;
}

Block * block_readfromfile(FILE * filepointer, unsigned short x, unsigned short y)
{ 
    int i;
    Block * blockpointer;
    Building * buildingpointer;
    unsigned short transport;
    unsigned char type;
    unsigned short level;
    int size;
    assert(filepointer != NULL);
    if(fread(&transport, sizeof(transport), 1, filepointer) != 1)
        return NULL;
    if(fread(&type, sizeof(type), 1, filepointer) != 1)
        return NULL;
    if(fread(&level, sizeof(level), 1, filepointer) != 1)
        return NULL;
    if(fread(&size, sizeof(size), 1, filepointer) != 1)
        return NULL;
    blockpointer = block_ini(type, x, y);
    blockpointer->level = level;
    blockpointer->transport = transport;
    /*Could use list_print as print function is set*/
    for(i = 0; i < size; i++)
    {
        buildingpointer = building_readfromfile(filepointer, blockpointer);
        if(buildingpointer == NULL)
            return NULL;
        list_insertFirst(blockpointer->buildings, buildingpointer);
    }
    return blockpointer;
}


unsigned short *block_getcoords(Block *b){
    unsigned short * ret;
    ret = (unsigned short *)malloc(sizeof(short)*2);
    if(ret != NULL){
        ret[0] = b->x;
        ret[1] = b->y;
    }
    return ret;
}
