#ifndef BUILDING_H
#define BUILDING_H

#include "../utils/includes.h"

#ifndef TYPEDEFBLOCK
#define TYPEDEFBLOCK
typedef struct _Block Block;
#endif
typedef struct _Building Building;

struct _Building{
    long vigilance;
    Building_type type;
    unsigned short level;
    unsigned char owner;
    unsigned long drugs;
    Block * parent;
};
/*Contructor, allocates memory for the building structure
 * Input: char type of the building to create
 *        char owner sets the owner of the building
 *        Block a pointer to the block where the building is placed
 * Output: Building pointer with memory allocated for the structure (level will be set to 0 by default)*/
Building * building_ini(Building_type type, char owner, Block *parent);

/*Destructor, frees memory for the building structure
 * Input: Building structure pointer to be freed
 * Output: 
 */ 
void building_free(void * b);

/* Copy function, allocates memory for new structure and set all the field in the building strucutre to the same
 * Input: (not nullable) Building pointer if pointer == null -> undefined behaviour 
 * Output: Building pointer (to be casted) 
 */ 
void * building_copy(void * b);

/* Adds +1 to the level field of the building
 * Input: (not nullable) Building pointer if pointer == null -> undefined behaviour 
 * Output: void (should be ERROR, OK?)
 */
void building_upgrade(Building * b);
void building_set_level(Building *b, int level);
void building_set_vigilance(Building *b, long vigilance);
void building_set_drugs(Building *b, unsigned long drugs);

/* Getters for the structure
 * Input: (not nullable) Building pointer if pointer == null -> undefined behaviour 
 * Output: the specified field
 */
unsigned long building_getdrugs(Building *b);
long building_getvigilance(Building *b);
unsigned short building_getlevel(Building *b); 
Building_type building_gettype(Building *b);
unsigned char building_getowner(Building *b);
Block * building_getparent(Building *b);

/* Prints a building to a binary file, printing values directly 
 * Input: (not nullable) Building pointer(to be casted) if pointer == null -> undefined behaviour
 *        (not nullable) FILE pointer if pointer == null -> undefined behaviour 
 *              FILE should already be open, data will be written to this file
 * Output : OK, ERROR
 */
Status building_printtofile(FILE *, void *);

/* Reads a building from a binary file, reading values in the same format that block_printtofile
 * Input: (not nullable) FILE pointer if pointer == null -> undefined behaviour 
 *              FILE should already be open, file position should already in the good offset
 * Output : new allocated Block pointer
 */ 
Building * building_readfromfile(FILE *, Block * );


#endif
