#ifndef BLOCK_H
#define BLOCK_H


#include "../utils/list.h"
#include "../utils/includes.h"
#include "building.h"

#ifndef TYPEDEFBLOCK
#define TYPEDEFBLOCK
typedef struct _Block Block;
#endif


struct _Block{
    unsigned short transport;
    unsigned char type;
    unsigned short level;
    unsigned short x;
    unsigned short y;
    List* buildings; 
};

/*Block ini, initialize block structure
 * Input: TYPE of block (char)
 * Output: Block pointer
 */

Block * block_ini(Block_type type, unsigned short, unsigned short);


/* Free block structure
 * Input: nullable Block pointer
 */
void block_destruct(Block *);

/* Structure getters
 * Input: (not nullable)Block pointer if pointer == null -> undefined behaviour
 * Output: The field required
 */ 
unsigned short block_gettransport(Block *);
unsigned char block_gettype(Block *);
unsigned short block_getlevel(Block *);
unsigned short *block_getcoords(Block *);
void block_upgrade(Block *b);
/* Get number of buildings
 * Input: (not nullable)Block pointer if pointer == null -> undefined behaviour
 * Output: (int) number of buildings in specified block
 */ 
int block_getnumbuildings(Block *);

/* Get a specific pointer to building in a given block
 * Input: (not nullable) Block pointer if pointer == null -> undefined behaviour
 *           int specifies the position in the array of buildings, set to <0 to discard the argument
 *           char specifies the type of the building to get (int must be <0 to be taken into account)
 * Output: pointer to the building specified
 */ 
Building * block_getBuilding(Block *, int , char);

/* Adds a building to a block, allocating memory for said building
 * Input: (not nullable) Block pointer if pointer == null -> undefined behaviour
 *          char, char parameters to building_ini
 * Output: OK, ERROR
 */ 
int block_addBuilding(Block *, char, char);


/* Deletes a building from a block, freeing memory for said building
 * Input: (not nullable) Block pointer if pointer == null -> undefined behaviour
 *           int specifies the position in the array of buildings, set to <0 to discard the argument
 *           char specifies the type of the building to get (int must be <0 to be taken into account)
 * Output: OK, ERROR
 */ 
Status block_deleteBuilding(Block *, int, char);


/* Prints a block to a binary file, printing values directly 
 * Input: (not nullable) Block pointer if pointer == null -> undefined behaviour
 *        (not nullable) FILE pointer if pointer == null -> undefined behaviour 
 *              FILE should already be open, data will be written to this file
 * Output : OK, ERROR
 */ 
Status block_printtofile(Block *, FILE *);

/* Reads a block from a binary file, reading values in the same format that block_printtofile
 * Input: (not nullable) FILE pointer if pointer == null -> undefined behaviour 
 *              FILE should already be open, file position should already in the good offset
 * Output : new allocated Block pointer
 */ 
Block * block_readfromfile(FILE *, unsigned short, unsigned short);
#endif
