#include "map.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

Map * map_ini(unsigned short height, unsigned short width)
{
    Map* map;
    int i;

    map = (Map *) malloc(sizeof(Map));
    if(map == NULL) return NULL;

    map->height = height;
    map->width = width;
    map->current_coord_y = 0;
    map->current_coord_x = 0;
    map->offset_coord_y = 0;
    map->offset_coord_x = 0;

    map->cells = (Block ***) malloc(width * sizeof(Block **));
    if(map->cells == NULL) return NULL;

    map->cells[0] = (Block **) malloc (width * height * sizeof(Block *));
    if (map->cells[0] == NULL){
        free(map->cells);
        free(map);
        return NULL;
    }

    for (i = 0; i < width; i++) map->cells[i] = &(map->cells[0][i * height]);
    
    return map;
}

void map_destruct(Map * map)
{
    
    if (map == NULL) return;

    free(map->cells[0]);
    free(map->cells);
    free(map);
}

Block* map_getBlock(Map * map, unsigned short height, unsigned short width)
{
    assert(map != NULL);
    if ((signed short)height > -1)
        if ((signed short)width > -1)
            if(map->height > height && map->width > width)
                return map->cells[height][width];
    return NULL;
}
/*
Status map_resize(Map * m, unsigned short, unsigned short)
{

}*/
Status map_savetofile(Map * map, char * file)
{
    int i, j;
    unsigned short *aux;
    FILE * filepointer;
    assert(map != NULL);
    assert(file != NULL);
    filepointer = fopen(file, "w");
    if(filepointer == NULL)
        return ERROR;
    aux = &map->height;
    fseek(filepointer, 0, SEEK_SET);
    if(fwrite(aux, sizeof(short), 1, filepointer) != 1)
        return ERROR;
    aux = &map->width;
    if(fwrite(aux, sizeof(short), 1, filepointer) != 1)
        return ERROR;
    for(i = 0; i < map->width; i++)
    {
        for(j = 0; j <  map->height; j++)
        {
            if(map->cells[i][j] == NULL){
                printf("%d %d", i, j);
                exit(1);
            }
            if(block_printtofile(map->cells[i][j], filepointer) == ERROR)
                return ERROR;
        }
    }
    fclose(filepointer);
    return OK;
}

Map * map_readfromfile(char *filename)
{
    unsigned short i;
    unsigned short  j;
    FILE * filepointer;
    Map * mappointer;
    Block * blockpointer;
    unsigned short height;
    unsigned short width;
    assert(filename != NULL);
    filepointer = fopen(filename, "r");
    if(filepointer == NULL)
        return NULL;
    fseek(filepointer, 0, SEEK_SET);
    if(fread(&height, sizeof(height), 1,filepointer) != 1)
        return NULL;
    if(fread(&width, sizeof(width), 1, filepointer) != 1)
        return NULL;
    mappointer = map_ini(height, width);
    if(mappointer == NULL)
    {
        fclose(filepointer);
        return NULL;
    }
    for(i = 0; i < width;i++)
    { 
        for(j = 0; j < height; j++)
        {
            blockpointer = block_readfromfile(filepointer, i ,j);
            mappointer->cells[i][j] = blockpointer;
        }
    }
    fclose(filepointer);
    return mappointer;
}

void map_print(Map* m)
{
    int i, j;
    char type;
    for(i = 0; i < m->width; i++)
    {
        for(j = 0; j < m->height; j++)
        {
            type = block_gettype(m->cells[j][i]);
            switch(type){
                case CITY:
                    printf(ANSI_COLOR_MAGENTA "%i", type);
                    break;
                case DESERT:
                    printf(ANSI_COLOR_GREEN "%i", type);
                    break;
                case ROUTE:
                    printf(ANSI_COLOR_CYAN "%i", type);
                    break;
                case HIGHWAY:
                    printf(ANSI_COLOR_CYAN "%i", type);
                    break;
                case VILLAGE:
                    printf(ANSI_COLOR_BLUE "%i", type);
                    break;
                default:
                    printf("%i", type);
                    break;
            }
        }
        printf("\n");
    }
}

int map_getCurrentCoordY (Map *map){

    if (map == NULL) return -1;

    return map->current_coord_y;
}

int map_getCurrentCoordX (Map *map){

    if (map == NULL) return -1;

    return map->current_coord_x;
}

Status map_setCurrentCoordY (Map *map, int y){

    if (map == NULL) return ERROR;

    map->current_coord_y = y;

    return OK;
}

Status map_setCurrentCoordX (Map *map, int x){

    if (map == NULL) return ERROR;

    map->current_coord_x = x;

    return OK;
}

Status map_decreaseCurrentCoordY (Map *map){

    if (map == NULL) return ERROR;

    map->current_coord_y--;

    return OK;
}

Status map_increaseCurrentCoordY (Map *map){

    if (map == NULL) return ERROR;

    map->current_coord_y++;

    return OK;
}

Status map_decreaseCurrentCoordX (Map *map){

    if (map == NULL) return ERROR;

    map->current_coord_x--;

    return OK;
}

Status map_increaseCurrentCoordX (Map *map){

    if (map == NULL) return ERROR;

    map->current_coord_x++;

    return OK;
}

int map_getOffsetCoordY (Map *map){

    if (map == NULL) return -1;

    return map->offset_coord_y;
}

int map_getOffsetCoordX (Map *map){

    if (map == NULL) return -1;

    return map->offset_coord_x;
}

Status map_setOffsetCoordY (Map *map, int y){

    if (map == NULL) return ERROR;

    map->offset_coord_y = y;

    return OK;
}

Status map_setOffsetCoordX (Map *map, int x){

    if (map == NULL) return ERROR;

    map->offset_coord_x = x;

    return OK;
}

Status map_decreaseOffsetCoordY (Map *map){

    if (map == NULL) return ERROR;

    map->offset_coord_y--;

    return OK;
}

Status map_increaseOffsetCoordY (Map *map){

    if (map == NULL) return ERROR;

    map->offset_coord_y++;

    return OK;
}

Status map_decreaseOffsetCoordX (Map *map){

    if (map == NULL) return ERROR;

    map->offset_coord_x--;

    return OK;
}

Status map_increaseOffsetCoordX (Map *map){

    if (map == NULL) return ERROR;

    map->offset_coord_x++;

    return OK;
}

Status map_joincitywithhighway(Map * mappointer, int * departure, int * arrival)
{
    int ox, ax, oy, ay;
    int i;
    int j;
    short xoffset;
    short yoffset;
    char type;
    assert(mappointer != NULL);
    assert(departure != NULL);
    assert(arrival != NULL);
    ox = departure[0];
    oy = departure[1];
    ax = arrival[0];
    ay = arrival[1];
    if(oy > ay)
    {
        /*path will be o -> a*/
        if(ox > ax)
        {
            i = ox; 
            j = oy;
            while(i != ax || j != ay)
            {
                if(j != ay){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                if(i != ax){
                    xoffset = aleat_num(0, 1) - 1;
                    i += xoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = HIGHWAY;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        break;
                }    
            }
        }
        else
        {
            i = ox; 
            j = oy;
            while(i != ax || j != ay)
            {
                if(j != ay){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                if(i != ax){
                    xoffset = aleat_num(0, 1);
                    i += xoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = HIGHWAY;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        break;
                }    
            }
        }
        
        

    }
    else if(oy < ay)
    {
        /*path will be to a-> o*/
        if(ax > ox)
        {
            i = ax; 
            j = ay;
            while(i != ox || j != oy)
            {
                if(i != ox){
                    xoffset = aleat_num(0, 1) - 1;
                    i += xoffset;
                }
                if(j != oy){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = HIGHWAY;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        break;
                }    
            }
        }
        else
        {
            i = ax; 
            j = ay;
            while(i != ox || j != oy)
            {
                if(i != ox){
                    xoffset = aleat_num(0, 1);
                    i += xoffset;
                }
                if(j != oy){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = HIGHWAY;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        break;
                }    
            }
        }
       
    }
    else
    {
        /*path will be straight line*/
        i = ox;
        j = oy;
        yoffset = 0;
        if(ox > ax)
        {
            xoffset = -1;
            while(i != ax)
            {
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = HIGHWAY;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY:
                    default:
                        break;
                }
                i += xoffset;
            }
        }
    }
    return OK;
}

Status map_joinvillage(Map * mappointer, unsigned short x, unsigned short y)
{
    short distancex = 1;
    short startx;
    short distancey = 1;
    short starty;
    short actualx;
    short actualy;
    short flag = ERROR;
    assert(mappointer != NULL);
    if(x < 0 || y < 0 || mappointer->height <= y || mappointer->width <= x)
        return ERROR;
    startx = x - distancex;
    if(startx < 0)
        startx = 0;
    starty = y - distancey;
    if(starty < 0)
        starty = 0;
    while(flag == ERROR)
    {
        flag = OK;
        if(startx > 0)
        {
            flag = ERROR;
            startx--;
            distancex++;  
        }
        if(startx + distancex < mappointer->width){
            distancex++;
            flag = ERROR;
        }
        if(starty > 0)
        {
            flag = ERROR;
            distancey++;
            starty--;
        }
        if(y + distancey < mappointer->height){
            distancey++;
            flag = ERROR;
        }
        for(actualx = startx; actualx < startx + distancex; actualx++)
        {
            for(actualy = starty; actualy < starty + distancey; actualy++)
            {
                switch(mappointer->cells[actualx][actualy]->type)
                {
                    case DESERT:
                        break;
                    default: /* ruta de caminos desde origen hasta actualx, actualy*/
                        if(map_createVillagePath(mappointer, x, y, actualx, actualy) == OK)
                            return OK;
                        else
                            return ERROR;
                }
            }
        }
    }
    return ERROR;
}

Status map_createVillagePath(Map * mappointer,unsigned short ox, unsigned short oy, unsigned short ax, unsigned short ay)
{
    int i;
    int j;
    short xoffset;
    short yoffset;
    char type;
    if(oy > ay)
    {
        /*path will be o -> a*/
        if(ox > ax)
        {
            i = ox; 
            j = oy;
            while(i != ax || j != ay)
            {
                if(j != ay){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                if(i != ax){
                    xoffset = aleat_num(0, 1) - 1;
                    i += xoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = ROUTE;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        return OK;
                }    
            }
        }
        else
        {
            i = ox; 
            j = oy;
            while(i != ax || j != ay)
            {
                if(j != ay){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                if(i != ax){
                    xoffset = aleat_num(0, 1);
                    i += xoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = ROUTE;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        return OK;
                }    
            }
        }
        
        

    }
    else if(oy < ay)
    {
        /*path will be to a-> o*/
        if(ax > ox)
        {
            i = ax; 
            j = ay;
            while(i != ox || j != oy)
            {
                if(i != ox){
                    xoffset = aleat_num(0, 1) - 1;
                    i += xoffset;
                }
                if(j != oy){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = ROUTE;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        return OK;
                }    
            }
        }
        else
        {
            i = ax; 
            j = ay;
            while(i != ox || j != oy)
            {
                if(i != ox){
                    xoffset = aleat_num(0, 1);
                    i += xoffset;
                }
                if(j != oy){
                    yoffset = aleat_num(0, 1) - 1;
                    j += yoffset;
                }
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = ROUTE;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY: 
                    default:
                        return OK;
                }    
            }
        }
       
    }
    else
    {
        /*path will be straight line*/
        i = ox;
        j = oy;
        yoffset = 0;
        if(ox > ax)
        {
            xoffset = -1;
            while(i != ax)
            {
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = ROUTE;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY:
                    default:
                        return OK;
                }
                i += xoffset;
            }
        }
        else
        {
            xoffset = 1;
            while(i != ax)
            {
                type = mappointer->cells[i][j]->type;
                switch(type)
                {
                    case DESERT:
                        mappointer->cells[i][j]->type = ROUTE;
                    case CITY:
                    case VILLAGE:
                    case HIGHWAY:
                    default:
                        return OK;
                }
                i += xoffset;
            }
        }
    }
    return OK;
}

Block *map_getCurrentBlock (Map * map){

    assert(map != NULL);
    return map->cells[map->current_coord_y][map->current_coord_x];
}
