#include "network/client_interface.h"
#include "interface/player.h"
int main(int argc, char const *argv[])
{
    char* nombre;
    pthread_t receiver;
    Client_interface *ci;
    ci = client_interface_ini();
    nombre = (char *)malloc(128);
    if(ci == NULL || nombre == NULL){
        printf("error");
        return 1;
    }
    printf("Introduce tu nombre por favor:\n");
    scanf("%s", nombre);
    send(ci->sockfd , nombre , strlen(nombre) , 0 ); 
    pthread_create(&receiver, NULL, client_readfromserver, ci);
    while(1){
        memset(nombre, 0, 128);
        printf("Escribe algo para reenviarlo al resto de jugadores:\n");
        scanf("%s", nombre);
        send(ci->sockfd, nombre, strlen(nombre), 0);
    }
    return 1;
}
