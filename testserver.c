#include "network/server_interface.h"
#include  <pthread.h>
#include  <stdlib.h>
#include  <stdio.h>

int print_players(Server_interface* si){
    int i;
    printf("Players connected:\n");
    for(i = 0; i < si->num_players; i++){
        if(si->ch[i] != NULL)
            printf("%s\n", si->ch[i]->name); 
    }
    return 0;
}
int main(int argc, char const *argv[])
{
    Server_interface *si;
    pthread_t starter, broadcaster;
    char type = 'p';
    int i;
    /* aqui ya nos conectariamos*/
    si = server_ini();
    pthread_create(&starter, NULL, server_clientstarter, si);
    pthread_create(&broadcaster, NULL, server_broadcaster, si);
    for(i = 10; i > -1; i--){
        printf("%d\n", i);
        sleep(2);
    }
    si->gtype = PLAYING;
    while(1)
        sleep(10);
    return 1;
}



