var searchData=
[
  ['image',['Image',['../interface_8h.html#a3ceaefc95f67897c6c3c0ea015ba5be0',1,'interface.h']]],
  ['image_5ftype',['Image_type',['../interface_8h.html#afcd3331a3a3490ad0320d3e54ab286e9',1,'interface.h']]],
  ['input_5fcoordinates',['input_coordinates',['../interface_8h.html#a94b3dbeaad741bea80dcc05b9d5a664a',1,'interface.c']]],
  ['interface_2eh',['interface.h',['../interface_8h.html',1,'']]],
  ['interface_5fmanager_2eh',['interface_manager.h',['../interface__manager_8h.html',1,'']]],
  ['interface_5fplayerinputnumber',['interface_playerInputNumber',['../interface_8h.html#ae3b086567ee68a1ab2d86c5f5f4124b7',1,'interface.c']]],
  ['interface_5fplayerinputtext',['interface_playerInputText',['../interface_8h.html#a4f488bbedbaa779986065b3240484ff7',1,'interface.c']]]
];
