/****************************************************************************
 *                            PRACTICA 4 PROGRAMACION II
 *                            CURSO 2017-2018
 *                            GRUPO 2101
 *                            NUMERO PAREJA 4
 *                            DANIEL BRITO
 *                            ALEJANDRO ANDRACA
 ****************************************************************************/
#ifndef PRACTICAPROGII_STACK_H
#define PRACTICAPROGII_STACK_H

#include "functions.h"
#include "includes.h"
typedef struct _Stack Stack;
typedef void(*destroy_element_function_type)(void*);
typedef void*(*copy_element_function_type)(const void*);
typedef int(*print_element_function_type)(FILE *, const void*);

/**------------------------------------------------------------------
Inicializa la pila reservando memoria.
Salida: NULL si ha habido error o la pila si ha ido bien
------------------------------------------------------------------*/
Stack * stack_ini();
/**------------------------------------------------------------------
Elimina la pila Entrada: la pila que se va a eliminar
------------------------------------------------------------------*/
void stack_destroy(Stack *);

/**------------------------------------------------------------------
Inserta un elemento en la pila.Entrada: un elemento y la pila donde insertarlo.
 Salida:ERROR si no logra insertarlo,
 OK si lo logra
------------------------------------------------------------------*/
Status stack_push(Stack *, const void*);
/**------------------------------------------------------------------
Extrae un elemento en la pila.
Entrada: la pila de donde extraerlo.
Salida: NULL si no logra extraerlo o el elemento extraido si lo logra.
Nótese que la pila quedará modificada
------------------------------------------------------------------*/
void * stack_pop(Stack *);
/**------------------------------------------------------------------
Copia un elemento (reservando memoria) sin modificar el top de la pila.
Entrada: la pila de donde copiarlo.
Salida: NULL si no logra copiarlo o el elemento si lo logra
------------------------------------------------------------------*/
void * stack_top(const Stack *);
/**------------------------------------------------------------------
Comprueba si la pila esta vacia.
Entrada: pila.
Salida: TRUE si está vacia
o FALSE si no lo esta
------------------------------------------------------------------*/
Bool stack_isEmpty(const Stack *);
/**------------------------------------------------------------------
Comprueba si la pila esta llena.
Al utiilizar reserva dinamica de memoria la pila no estara nunca llena
Entrada: pila.
Salida: TRUE si está llena o FALSE si no lo esta
------------------------------------------------------------------ */
Bool stack_isFull(const Stack *);
/**------------------------------------------------------------------
Imprime toda la pila, colocando el elemento en la cima al principio
de la impresión (y un elemento por línea).
Entrada: pila y fichero donde imprimirla.
Salida: Devuelve el número de caracteres escritos.
------------------------------------------------------------------*/
int stack_print(FILE*, const Stack*);


/**------------------------------------------------------------------
Imprime el estado de la pila diciendo si esta llena y/o vacia
Entrada: pila y fichero donde imprimirla.
Salida: Devuelve el número de caracteres escritos.
------------------------------------------------------------------*/
int stack_statusprint(FILE*, const Stack*);



/*Devuelve el numero de elementos que contiene una pila
* -1 si no hay pila o se produce error durante la ejecucion*/
int stack_size(Stack*);




#endif /*PRACTICAPROGII_STACK_H*/
