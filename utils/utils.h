/**
 *
 * Descripcion: Funciones de cabecera útiles
 *
 * Fichero: utils.h
 * Autor: Carlos Aguirre.
 * Version: 1.0
 * Fecha: 15-09-2016
 *
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <stdio.h>
#include "string.h"
#include <assert.h>
#include "time.h"

void swap(int * a, int * b);
void libera_permutaciones(int **, int );
int busca_minimo(int * tabla, int ip, int iu, int* num_obs);
int busca_maximo(int * tabla, int ip, int iu, int* num_obs);
void * mycopy(const void* c);

#endif
