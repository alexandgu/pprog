/****************************************************************************
 *                          PRACTICA 3 PROGRAMACION II
 *                          CURSO 2017-2018
 *                          GRUPO 2101
 *                          NUMERO PAREJA 4
 *                          DANIEL BRITO
 *                          ALEJANDRO ANDRACA
 ****************************************************************************/
#include "list.h"

struct _NodeList {
    void* data;
    struct _NodeList *next; } ;


struct _List {
    NodeList *node;
    destroy_elementlist_function_type destroy_element_function; 
    copy_elementlist_function_type copy_element_function; 
    print_elementlist_function_type print_element_function; 
    cmp_elementlist_function_type cmp_element_function;
};

NodeList * node_ini();
/*Destruye un node*/
void node_destroy(NodeList * m, destroy_elementlist_function_type f1);
Status node_setInfo(NodeList *, void *, copy_elementlist_function_type);
Status node_setNext(NodeList *, NodeList*);
NodeList * node_getNext(NodeList*);

List * list_ini(destroy_elementlist_function_type f1, copy_elementlist_function_type f2,
print_elementlist_function_type f3, cmp_elementlist_function_type f4){
    List * l;
    l = (List *)calloc(1, sizeof(List));
    if(!l)
        return NULL;
    l->destroy_element_function = f1;
    l->copy_element_function = f2;
    l->print_element_function = f3;
    l->cmp_element_function = f4;
    return l;
}
NodeList * node_ini(){
    NodeList * n;
    n = calloc(1, sizeof(NodeList));
    if(!n)
        return NULL;
    n->data = NULL;
    n->next = NULL;
    return n;
}

void node_destroy(NodeList * n, destroy_elementlist_function_type f1){
    if(!n)
        return;
    if(f1!=NULL){
    f1(n->data);
    }
    free(n);
}
Status node_setInfo(NodeList *n ,void *v, copy_elementlist_function_type f){
    if(!n)
        return ERROR;
    if(f != NULL)
        n->data = f(v);
    else 
        n->data = v;
    return OK;
}
void * node_getInfo(NodeList *n){
    if(n)
        return n->data;
    return NULL;
}
Status node_setNext(NodeList *n, NodeList*next){
    if(!n)
        return ERROR;
    n->next = next;
    return OK;
}
NodeList * node_getNext(NodeList *n){
    if(!n)
        return NULL;
    return n->next;
}
void list_free(List* list){
    NodeList * node, *next;
    if(!list)
        return;
    node = list->node;
    if(list_isEmpty(list)){
        free(list);
        return;
    }
    while(node->next){
        next = node->next;
        node_destroy(node, list->destroy_element_function);  
        node = next;
                   
    }
    node_destroy(node, list->destroy_element_function);
    free(list);
    
}

Status list_insertFirst(List* list,   void *elem){
    NodeList *n;
    n = node_ini();
    if(!n || !list)
        return ERROR;
    node_setNext(n, list->node);
    list->node = n;
    if(node_setInfo(n, elem, list->copy_element_function) == ERROR)
        return ERROR;
    return OK;
    
}
Status list_insertLast(List * list,   void *elem){
    NodeList *n, *next;
    n = node_ini();
    if(!n || !list)
        return ERROR; 
    if(node_setInfo(n, elem, list->copy_element_function) == ERROR)
        return ERROR;
    if(list_isEmpty(list)){
        list->node = n;
        return OK;
    }
    next = list->node;
    while(node_getNext(next))
        next = node_getNext(next);
    
    if(node_setNext(next, n) == ERROR)
        return ERROR;
    return OK;
}

Status list_insertInOrder(List *list,   void *pElem){
    NodeList *n, *next, *last; 
    int result;
    void *info;
    n = node_ini();
    if(!list || !n || !pElem || list->cmp_element_function)
        return ERROR;
    if(!node_setInfo(n, pElem, list->copy_element_function)){
        return ERROR;
    }
        
    if(list_isEmpty(list)){
        list->node = n;
    }else{
        last = list->node;
        info = last->data;
        result = list->cmp_element_function(pElem, info);
        if(result < 0){
            list->node = n;
            n->next = last;
            return OK;
        }
        while(last->next){
            next = last->next;
            info = next->data;
            result = list->cmp_element_function(pElem, info);
            if(result < 0){
                n->next = next;
                last->next = n;
                return OK;
            }
            last = next;
        }
        last->next = n;
    }
    return OK;
        
}
void * list_extractFirst(List * list){
    NodeList *pnl, *next;
    void *pv;
    if(!list)
        return NULL;
    
    pnl = list->node;
    if(!pnl)
        return NULL;
    next = pnl->next;
    if(list->copy_element_function){
        pv = list->copy_element_function(pnl->data);
        node_destroy(list->node, list->destroy_element_function);
    }else{
        pv = list->node->data;
    }
    list->node = next;
    return pv;
}
void * list_extractLast(List * list){
    NodeList *n, *last;
    void *pv;
    if(!list)
        return NULL;
    n = list->node;
    if(!n->next){
        list->node = NULL;
        pv = n->data;
        return pv;
    }
    while(node_getNext(n)){
        last = n;
        n = n->next;
    }
    last->next = NULL;
    return n->data;
}

void * list_extractIndex(List* list, int index){
    int i;
    NodeList *prev, *next, *current;
    assert(list != NULL);
    if(index == 0)
    {
        return list_extractFirst(list);
    }
    else if(index >= list_size(list))
    {
        return NULL;
    }
    else if(index == list_size(list) -1)
    {
        return list_extractLast(list);
    }
    else
    {
        prev = list->node;
        current = list->node->next;
        next = list->node->next;
        for(i = 1; i < index; i++)
        {
            prev = current;
            current = next;
            next = next->next;
        }
        prev->next = next;
        return current->data;
    }
    return NULL;
}
Bool list_isEmpty(  List* list){
    if(!list || !list->node)
        return TRUE;
    return FALSE;
}
  void* list_get(  List* list, int i){
    NodeList *n;
    int j;
    if(!list)
        return NULL;
    n = list->node;
    for(j = 0 ; j< i; j++){
        n = n->next;
    }
    if (n == NULL) return NULL;
    return n->data;
}

int list_size(List* list){
    int suma;
    NodeList *n;
    if(!list)
        return -1;
    suma = 0; 
    n = list->node;
    while(n){
        n = n->next;
        suma++;
    }
    return suma;
}

int list_print(FILE *fd,   List* list){
    NodeList * n;
    int suma;
    n = list->node;
    if(!fd || !list){
        return -1;
    }
    suma =0;
    while(n){
        suma += list->print_element_function(fd, n->data);
        n = n->next;
    }
    
    return suma;
}


