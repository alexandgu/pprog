/****************************************************************************
 *                          PRACTICA 4 PROGRAMACION II
 *                          CURSO 2017-2018
 *                          GRUPO 2101
 *                          NUMERO PAREJA 4
 *                          DANIEL BRITO
 *                          ALEJANDRO ANDRACA
 ****************************************************************************/
#include "queue.h"
struct _Queue {
        void** head;
        void** end;
        void** item;
        int reservado;
        pthread_mutex_t* lock;
        destroy_elementqueue_function_type destroy_element_function;
        copy_elementqueue_function_type copy_element_function;
        print_elementqueue_function_type print_element_function;
};


Queue* queue_ini(destroy_elementqueue_function_type f1, copy_elementqueue_function_type f2,
                 print_elementqueue_function_type f3){
        Queue * pq;
        pq = (Queue *)malloc(sizeof(Queue));
        
        if(!pq)
                return NULL;
        pq->destroy_element_function = f1;
        pq->copy_element_function = f2;
        pq->print_element_function = f3;
        pq->lock = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init(pq->lock, NULL);
        pq->item = (void **)calloc(MAXQUEUE, sizeof(void *));
        if(!pq->item) {
                free(pq);
                return NULL;
        }
        pq->reservado = MAXQUEUE;
        pq->end = pq->item;
        pq->head = pq->item;
        return pq;
}

void queue_destroy(Queue * q){
        int head, end, i;
        
        if(!q)
                return;
        head = q->head - q->item;
        end = q->end - q->item;
        if(!queue_isEmpty(q)){
                for(i = head; i <= end; i++){
                q->destroy_element_function(q->item[i]);
                i = i % q->reservado;
                }
        }
        
        free(q->item);
        free(q);
}
Bool queue_isEmpty(const Queue *q){
        if(!q){
                return TRUE;
        }
        if(q->head==q->end){
                return TRUE;
        }
        return FALSE;
}      

Bool queue_isFull(const Queue* q){
        if(q){
                return FALSE;
        }
        return TRUE;
}
/*revisar esta funcion porque lo mas probable es que de errores*/
Status queue_insert(Queue *q, const void* pElem){
        /*declaramos enteros head y end por comodidad para trabajar con ellos y para poder actualizarlos al hacer reallocs*/
        int size, iHead, iEnd;
        if(!q || !pElem)
                return ERROR;
        pthread_mutex_lock(q->lock);
        iEnd = q->end - q->item;
        iHead = q->head - q->item;
        if((iHead -1) == iEnd || q->end ==(q->item+q->reservado)){
                q->item = (void **)realloc(q->item, 2*q->reservado*sizeof(void*));
                q->end = &q->item[iEnd];
                q->head = &q->item[iHead];
                if(!q->item)
                        return ERROR;
                if(q->head > q->end){
                        size = q->head - q->item;
                        memcpy(q->item[q->reservado], q->item, size*sizeof(void*));
                        q->head = &q->item[q->reservado+iHead];
                }
                
                q->reservado = q->reservado*2;
        }
        if(q->copy_element_function != NULL)
                q->item[iEnd] = q->copy_element_function(pElem);
        else
                q->item[iEnd] = (void *)pElem;
        if (q->item[iEnd]==NULL){
                pthread_mutex_unlock(q->lock);
                return ERROR;
        }
    
        q->end = (q->end+1);
        /*El equivalente a hacer modulo tamaño cola con enteros*/
        if(q->end - (q->item+q->reservado) > 0){
                q->end -= q->reservado;
        }
        pthread_mutex_unlock(q->lock);
        return OK;
}

void * queue_extract(Queue *q){
        int iHead, iEnd, size;
        void ** aux;
        void ** newQueue;
        if(!q)
                return NULL;
        pthread_mutex_lock(q->lock);
        iHead = q->head - q->item;
        iEnd = q->end - q->item;
        aux = *q->head;
        
        if(q->head > q->end){/* Caso feo*/
                if((q->end - q->head) > (q->reservado/2)){
                        size = &q->item[q->reservado]-q->head;
                        newQueue = (void**)calloc(q->reservado/2, sizeof(void*));
                        memcpy(newQueue, q->head, size*sizeof(void*));
                        memcpy(&newQueue[size], q->item, iEnd);
                        free(q->item);
                        q->item = newQueue;
                        q->head = &q->item[iHead];
                        q->end = &q->item[iEnd];
                        q->reservado = q->reservado/2;

                }

        }else if(q->head < q->end){ /*Importante excluir el caso con la igualdad porque ahi la cola estaria vacia*/
                if((q->end - q->head) < (q->reservado/2)){
                        newQueue = (void **)calloc(q->reservado/2, sizeof(void*));
                        memcpy(newQueue, q->head, (q->end-q->head)*sizeof(void*));
                        free(q->item);
                        q->item = newQueue;
                        q->end = &q->item[q->end-q->head];
                        q->head = q->item;
                        q->reservado = q->reservado /2;
                }
        }
        *q->head = NULL;
        q->head++;
        pthread_mutex_unlock(q->lock);
        return aux;
}

int queue_print(FILE *pf, const Queue *q){
        int suma=0, pos;
        if(!q){
                return -1;
        }
        if(queue_isEmpty(q))
                return 0;
        pthread_mutex_lock(q->lock);
        for(pos = (q->head-q->item); &(q->item[pos])!= q->end; pos++){
                suma += printf("%s\n", q->item[pos]);
                if(pos == q->reservado)
                        pos -= q->reservado;
        }
        pthread_mutex_unlock(q->lock);
        return suma;
}

int queue_size(const Queue *q){
  int tam;
  if(!q){
    return -1;
  }
  if(q->head > q->end){
        tam = q->item + q->reservado - q->head;
        tam += q->end - q->item;
  }else{
        tam = q->end - q->head; 
  }
  return tam;
}

Status queue_printstatus(FILE *f, Queue *q){
        if(!f || !q){
                return ERROR;
        }
        if(queue_isEmpty(q)){
                fprintf(f, "Queue vacia.\n");
        }else{
                fprintf(f, "Cola con %i elementos. \n", queue_size(q));
        } 
        return OK;

}


