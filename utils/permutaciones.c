/**
 *
 * Descripcion: Implementacion de funciones de generacion de permutaciones
 *
 * Fichero: permutaciones.c
 * Autor: Carlos Aguirre
 * Version: 1.0
 * Fecha: 16-09-2017
 *
 */



#include "permutaciones.h"

/***************************************************/
/* Funcion: aleat_num Fecha:                       */
/* Autores: Claudia Sequeiros y Alejandro Andraca  */
/*                                                 */
/* Rutina que genera un numero aleatorio           */
/* entre dos numeros dados                         */
/*                                                 */
/* Entrada:                                        */
/* int inf: limite inferior                        */
/* int sup: limite superior                        */
/* Salida:                                         */
/* int: numero aleatorio                           */
/***************************************************/
int aleat_num(int inf, int sup)
{
  if(inf < sup)
    return rand()/(RAND_MAX + 1.0) * (sup-inf+1) + inf;
  return 0;
}

/***************************************************/
/* Funcion: genera_perm Fecha:                     */
/* Autores: Claudia Sequeiros y Alejandro Andraca  */
/*                                                 */
/* Rutina que genera una permutacion               */
/* aleatoria basada en algoritmo de Fisher-Yates   */
/*                                                 */
/* Entrada:                                        */
/* int n: Numero de elementos de la                */
/* permutacion                                     */
/* Salida:                                         */
/* int *: puntero a un array de enteros            */
/* que contiene a la permutacion                   */
/* o NULL en caso de error                         */
/***************************************************/
int* genera_perm(int N)
{
  int *perm;
  int i;
  assert(N > 0);
  perm = (int *)malloc(N*sizeof(perm[0]));
  if(perm == NULL)
    return NULL;
  for(i = 0; i < N; i++)
    perm[i] = i+1;
  for(i = 0; i < N; i++)
    swap(&(perm[i]), &(perm[aleat_num(i, N-1)]));


  return perm;
}

/***************************************************/
/* Funcion: genera_permutaciones Fecha:            */
/* Autores: Claudia Sequeiros y Alejandro Andraca  */
/*                                                 */
/* Funcion que genera n_perms permutaciones        */
/* aleatorias de tamanio elementos                 */
/*                                                 */
/* Entrada:                                        */
/* int n_perms: Numero de permutaciones            */
/* int N: Numero de elementos de cada              */
/* permutacion                                     */
/* Salida:                                         */
/* int**: Array de punteros a enteros              */
/* que apuntan a cada una de las                   */
/* permutaciones                                   */
/* NULL en caso de error                           */
/***************************************************/
int** genera_permutaciones(int n_perms, int N)
{
  int ** perms;
  int i;
  assert(n_perms > 0);
  assert(N > 0);
  perms = (int **)malloc(n_perms*sizeof(perms[0]));
  if(perms == NULL)
    return NULL;
  for(i = 0; i < n_perms; i++)
    perms[i] = genera_perm(N);
  return perms;
}
