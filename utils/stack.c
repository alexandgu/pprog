/****************************************************************************
*                            PRACTICA 4 PROGRAMACION II
*                            CURSO 2017-2018
*                            GRUPO 2101
*                            NUMERO PAREJA 4
*                            DANIEL BRITO
*                            ALEJANDRO ANDRACA
****************************************************************************/
#include "stack.h"
/* En stack_element.c */




struct _Stack {
        int top;
        void ** item;
        unsigned int reservado;
        destroy_element_function_type destroy_element_function;
        copy_element_function_type copy_element_function;
        print_element_function_type print_element_function;
};

Stack * stack_ini(destroy_element_function_type f1, copy_element_function_type f2,
                  print_element_function_type f3){
        Stack *st=NULL;
        st = (Stack*)calloc(1,sizeof(Stack));
        if(!st) return NULL;
        st->top = 0;
        st->item = (void **)calloc(MAXSTACK, sizeof(void *));
        if(!st->item) {
                free(st);
                return NULL;
        }
        st->reservado = MAXSTACK;
        st->destroy_element_function = f1;
        st->copy_element_function = f2;
        st->print_element_function = f3;
        return st;
}
void stack_destroy(Stack * stack){
        int i;
        if(stack) {
                if(stack->item) {
                        for(i=0; i<=stack->top; i++) {

                                stack->destroy_element_function(stack->item[i]);
                        }
                        free(stack->item);
                }
                free(stack);
        }
}

void * stack_top(const Stack *stack){
        if(!stack) {
                return NULL;
        }
        return stack->copy_element_function(stack->item[stack->top]);
}

Bool stack_isEmpty(const Stack *stack){
        if(!stack || !stack->top)
                return TRUE;
        return FALSE;
}

Bool stack_isFull(const Stack *stack){
        if(stack) {
                /*Como usamos memoria dinamica nunca llenamos la pila*/
                return FALSE;
        }
        return TRUE;
}


Status stack_push(Stack * p,const void * e){
        int i;
        void ** backup;
        /*Comprobar los argumentos*/
        if(p && e) {
                /*Comprobamos que haya sitio, si no realocamos y reservamos el doble de memoria*/
                if(p->top == p->reservado-1) {
                        backup =p->item;
                        p->item = (void **)realloc(p->item, sizeof(void *)*p->reservado*2);

                        if(!p->item) {
                                for(i = p->reservado; i > 0; i = i/2) {
                                        p->item = (void **)realloc(p->item, sizeof(void *)*(p->reservado+i));
                                        if(p->item)
                                                break;
                                        if(!p->item)
                                                p->item = backup;
                                }

                                if(!p->item) {
                                        p->item= backup;
                                        return ERROR;
                                }

                        }else{
                                p->reservado = 2*p->reservado;
                        }

                }
                /*Copiamos el elemento a añadir a la pila */
                p->item[p->top+1] = p->copy_element_function(e);
                /*Comprobamos que se haya copiado bien*/
                if(p->item[p->top+1]) {

                        p->top++;
                        return OK;
                }

        }
        return ERROR;

}

void * stack_pop(Stack * s){
        void * e = NULL;
        void ** backup;
        if(!s || stack_isEmpty(s))
                return NULL;
        e = s->item[s->top];
        s->item[s->top] = NULL;
        s->top--;
        /*Si estamos usando menos de la mitad de la memoria que hemos reservado
         * realocamos para liberar memoria*/
        if((2 * s->top)< s->reservado-1 && s->reservado > MAXSTACK) {
                backup = s->item;
                s->item = (void ** )realloc( s->item, (s->reservado/2)*sizeof(void *));
                if(!s->item) {
                        s->item = backup;
                }else{
                        s->reservado = s->reservado/2;
                }


        }

        return e;
}

int stack_print(FILE*f, const Stack* st){
        unsigned int i, suma =0;
        if(f && st) {
                for(i = st->top; i > 0; i--) {
                        suma += st->print_element_function(f, st->item[i]);
                        suma += fprintf(stdout, "\n");
                }
        }
        return suma;



}

int stack_statusprint(FILE *f, const Stack *st){
        int suma;
        if(!f || !st)
                return -1;
        suma = fprintf(f, "(");
        if(!stack_isFull(st))
                suma += fprintf(f, "no ");
        suma += fprintf(f, "llena, ");
        if(!stack_isEmpty(st))
                suma += fprintf(f, "no ");
        suma += fprintf(f, "vacia) " );
        return suma;
}

int stack_size(Stack* s){
        if(s)
                return s->top;
        return -1;
}


