/****************************************************************************
 *                            PRACTICA 4 PROGRAMACION II
 *                            CURSO 2017-2018
 *                            GRUPO 2101
 *                            NUMERO PAREJA 4
 *                            DANIEL BRITO
 *                            ALEJANDRO ANDRACA
 ****************************************************************************/
#ifndef PRACTICAPROGIILOCAL_INCLUDES_H
#define PRACTICAPROGIILOCAL_INCLUDES_H

#define _BSD_SOURCE /*no borrar esto que si no no compila*/
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <signal.h>
#include <ctype.h>          
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "utils.h"
#include <assert.h>
#include "types.h"

typedef enum {REST = 0, LAB = 1, TRAPHOUSE, POLICE_STATION, NULL_B} Building_type;
typedef enum {CITY = 0, DESERT = 1, HIGHWAY, VILLAGE, ROUTE, BLOCK_TYPE} Block_type;
typedef enum{ STARTING = 0, MAPBROADCAST = 1, PLAYING, ENDING} Game_type;
typedef enum{SINGLE = 0, CLIENT, SERVER} Game_mode;



typedef struct sockaddr_in sockin;
typedef struct sockaddr sock;

#define MAX_BLOCK_TYPE 5
#define MAX_PLAYERS 8
#define MAX_DESTROYED_LABS 5

#define GAMEOVER 2
#define WARNING 1234
#define DONTDOTHAT -2
#define NOMONEY -3

#define TRAPHOUSE_CONSTANT 50
#define LAB_CONSTANT 40
#define REST_CONSTANT 30
#define DRUG_CONSTANT 100

#define DESTROYED_CONSTANT 100
#define BESTPLACE_CONSTANT 6
#define GOODPLACE_CONSTANT 5
#define NORMALPLACE_CONSTANT 4  
#define BADPLACE_CONSTANT 3
#define WORSTPLACE_CONSTANT 2
#define FRONT_CONSTANT 6
#define VIGILANCE_LAB_CONSTANT 0.2
#define VIGILANCE_PLAYER_CONSTANT 0.04

#define CITY_CONSTANT 5
#define VILLAGE_CONSTANT 4
#define ROUTE_CONSTANT 3
#define HIGHWAY_CONSTANT 2
#define DESERT_CONSTANT 1

#define MAX_BUILDINGS 10
#define MAX_VIGILANCE 100
#define MAX_CAUGHT_LABS 5 
#define MAX_LEVEL 5

#define GAMEOVER_MONEY -100
#define GAMEOVER_VIGILANCE 100
#define WARNING_VIGILANCE 70
#define CAUGHT_LAB 100

#define RED 255,0,0
#define GREEN 0x41, 0xf0, 0x31

#define MAXQUEUE 128
#define MAXSTACK 128
#define MAX 8192
#define MAX_MOVES 4
#define MAP_MAX_HEIGHT 16
#define MAP_MAX_WIDTH 32
#define BUFF_SIZE 128
#define MAX_NAME 32
#define MAX_BUILDING_PER_PLAYER 128

#define CITY_TRANSPORT 10
#define CITY_VIGILANCE 10

#define DESERT_TRANSPORT 0
#define DESERT_VIGILANCE 0

#define HIGHWAY_TRANSPORT 10
#define HIGHWAY_VIGILANCE 8

#define VILLAGE_TRANSPORT 3
#define VILLAGE_VIGILANCE 2

#define ROUTE_TRANSPORT 4
#define ROUTE_VIGILANCE 1



#define MAX_REST_START 16
#define MAX_LABS_START 16
/*#ifndef CSI
#define CSI "\e]"
#endif*/

/*Network*/
#define PORT 20000
#define LENGTH 512
#define sin_size sizeof(sockin)

#endif
