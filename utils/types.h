/****************************************************************************
 *                            PRACTICA 4 PROGRAMACION II
 *                            CURSO 2017-2018
 *                            GRUPO 2101
 *                            NUMERO PAREJA 4
 *                            DANIEL BRITO
 *                            ALEJANDRO ANDRACA
 ****************************************************************************/

#ifndef PRACTICAPROGII_TYPES_H
#define PRACTICAPROGII_TYPES_H
typedef enum {
    
    ERROR = -1, OK = 0, END = 1
} Status;
typedef enum {
    FALSE = 0, TRUE = 1
} Bool;
#endif /*PRACTICAPROGII_TYPES_H*/
