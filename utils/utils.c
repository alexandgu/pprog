/**
 *
 * Descripcion: Implementacion de funciones útiles
 *
 * Fichero: utils.c
 * Autor: Carlos Aguirre Maeso
 * Version: 1.0
 * Fecha: 16-09-2017
 *
 */

 #include "utils.h"

 /***************************************************/
 /* Funcion: swap Fecha:                            */
 /* Autores: Claudia Sequeiros y Alejandro Andraca  */
 /*                                                 */
 /* Funcion que genera n_perms permutaciones        */
 /* aleatorias de tamanio elementos                 */
 /*                                                 */
 /* Entrada:                                        */
 /* int a: Puntero que apunta al primer entero      */
 /* int N: Puntero que apunta al segundo entero     */

 /*                                                 */
 /***************************************************/
 void swap(int * a, int *b){
   assert(a !=NULL);
   assert(b != NULL);
   if(*a != *b){
     *a ^= *b;
     *b ^= *a;
     *a ^= *b;
   }
 }

 /***************************************************/
 /* Funcion: libera_permutaciones Fecha:            */
 /* Autores: Claudia Sequeiros y Alejandro Andraca  */
 /*                                                 */
 /* Funcion que libera n_perms permutaciones        */
 /*                                                 */
 /* Entrada:                                        */
 /* int tabla: Tabla de permutaciones               */
 /* int n_perms: Número de permutaciones en la tabla*/
 /***************************************************/
 void libera_permutaciones(int ** tabla, int n_perms){
   int i;
   assert(tabla != NULL);
   assert(n_perms > 0);
   for(i = 0; i < n_perms; i++){
     free(tabla[i]);
   }
   free(tabla);
 }

 /***************************************************/
 /* Funcion: busca_minimo Fecha:                    */
 /* Autores: Claudia Sequeiros y Alejandro Andraca  */
 /*                                                 */
 /* Rutina que busca el mínimo en una subtabla      */
 /*                                                 */
 /* Entrada:                                        */
 /* int tabla: Tabla a ordenar                      */
 /* int ip: Índice donde empieza la subtabla        */
 /* int iu: Índice donde acaba la subtabla          */
 /* int num_obs: Contador del OB del algoritmo      */
 /* Salida:                                         */
 /* int: Devuelve el índice del menor valor de tabla*/
 /***************************************************/
 int busca_minimo(int * tabla, int ip, int iu, int* num_obs)
 {
   int i;
   int minimo = __INT_MAX__;
   int indice = -1;
   for(i = ip; i < iu; i++){
     (*num_obs)++; 
     if(tabla[i] < minimo){
       minimo = tabla[i];
       indice = i;
     }
   }
   return indice;
 }

 /***************************************************/
 /* Funcion: busca_minimo Fecha:                    */
 /* Autores: Claudia Sequeiros y Alejandro Andraca  */
 /*                                                 */
 /* Rutina que busca el máximo en una subtabla      */
 /*                                                 */
 /* Entrada:                                        */
 /* int tabla: Tabla a ordenar                      */
 /* int ip: Índice donde empieza la subtabla        */
 /* int iu: Índice donde acaba la subtabla          */
 /* int num_obs: Contador del OB del algoritmo      */
 /* Salida:                                         */
 /* int: Devuelve el índice del mayor valor de tabla*/
 /***************************************************/
 int busca_maximo(int * tabla, int ip, int iu, int* num_obs)
 {
   int i;
   int maximo = 0;
   int indice = -1;
   for(i = ip; i < iu; i++){
     (*num_obs)++; 
     if(tabla[i] > maximo){
       maximo = tabla[i];
       indice = i;
     }
   }
   return indice;
 }


void * mycopy(const void* c){
    char * string;
    string = (char *)malloc(strlen((char *)c) * sizeof (char) + 1);
    strcpy(string,  (const char *)c);
    return string;
}
