/****************************************************************************
 *                          PRACTICA 3 PROGRAMACION II
 *                          CURSO 2017-2018
 *                          GRUPO 2101
 *                          NUMERO PAREJA 4
 *                          DANIEL BRITO
 *                          ALEJANDRO ANDRACA
 ****************************************************************************/
#ifndef PRACTICAPROGII_LIST_H
#define PRACTICAPROGII_LIST_H

#include "../utils/includes.h"
#include "../utils/types.h"

typedef struct _List List;
typedef struct _NodeList NodeList;
void * node_getInfo(NodeList *n);
/* Tipos de los punteros a función soportados por la lista */
typedef void (*destroy_elementlist_function_type)(void*);
typedef void* (*copy_elementlist_function_type)(void*);
typedef Status (*print_elementlist_function_type)(FILE *, void*);
/* La siguiente función permite comparar dos elementos, 
 *devolviendo un número positivo, negativo o cero según si el primer argumento es mayor, 
 *menor o igual que el segundo argumento */
typedef int (*cmp_elementlist_function_type)(  void*,   void*);
/* Inicializa la lista reservando memoria e inicializa todos sus elementos. */
List* list_ini(destroy_elementlist_function_type f1, copy_elementlist_function_type f2,
print_elementlist_function_type f3, cmp_elementlist_function_type f4); 
/* Libera la lista y todos sus elementos. */
void list_free(List* list);
/* Inserta al principio de la lista realizando una copia del elemento. */ 
Status list_insertFirst(List* list,   void *elem);
/* Inserta al final de la lista realizando una copia del elemento. */ 
Status list_insertLast(List* list,   void *elem);
/* Inserta en orden en la lista realizando una copia del elemento. */ 
Status list_insertInOrder(List *list,   void *pElem);
/* Extrae del principio de la lista realizando una copia del elemento almacenado en dicho nodo. */ 
void * list_extractFirst(List* list);
/* Extrae del final de la lista realizando una copia del elemento almacenado en dicho nodo. */ 
void * list_extractLast(List* list);
/* Extrae del i-esimo indice de la lista realizando una copia del elemento almacenado en dicho nodo. */ 
void * list_extractIndex(List* list, int i);
/* Comprueba si una lista está vacía o no. */ 
Bool list_isEmpty(  List* list);
/* Devuelve el elemento i-ésimo almacenado en la lista. En caso de error, devuelve NULL. */ 
  void* list_get(  List* list, int i);
/* Devuelve el tamaño de una lista. */
int list_size(  List* list);
/* Imprime una lista (cada elemento en una nueva línea) devolviendo el número de caracteres escritos. */ 
int list_print(FILE *fd,   List* list);

#endif
