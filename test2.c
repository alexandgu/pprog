#include <stdio.h>
#include <pthread.h>

#include "interface/interface_manager.h"


int main(int argc, char  *argv[]){

	Map *m;
	game = NULL;
	int flag = EXIT_SUCCESS;

    m = map_readfromfile("fichero.txt");
    screen_setUp();
	
	q = queue_ini(destroy_function, copy_function, print_function);
	if (q == NULL) return EXIT_FAILURE;

	if (pthread_create(&print_thread, NULL, (void *) function_executor, q)){

		fprintf(stderr, "Error creating thread.\n");
		return ERROR;
	}

	if (mainScreen_manager() == ERROR) flag = EXIT_FAILURE;
	game_setmap(game, m);
	if (map_manager(m) == ERROR) flag = EXIT_FAILURE;

	map_destruct(m);
	screen_shutDown();

	return flag;
}