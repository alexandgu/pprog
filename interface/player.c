
#include "player.h"
 
/* 
    *****PLAYER****** 
                        */
Player *player_create (char *name, short id){
  Player *player;
  assert(name!=NULL);
  player = (Player* ) malloc(sizeof(Player));
    if(!player) return NULL;
  player->id = id;
  player->name = mycopy(name);
  player->money=100;
  player->n_labs=0;
  player->n_labs_destroyed=0; 
  player->n_restaurants=0;
  player->n_traphouse =0;
  player->vigilance=0;
  player->p_labs = list_ini(NULL, NULL, NULL, NULL);
  player->p_restaurants = (Building **)malloc(MAX_BUILDING_PER_PLAYER*sizeof(Building *));
  player->p_traphouse = (Building **)malloc(MAX_BUILDING_PER_PLAYER*sizeof(Building*));
  return player;
}
void player_destroy (Player *p){
  if(p){
    free(p->name);
    free(p->p_labs);
    free(p->p_restaurants);
    free(p);
  }
}
short player_get_id(Player *p){
  assert(p != NULL);
  return p->id;
}
void player_set_id(Player *p, short id)
{
  assert(p != NULL);
  p->id = id;
}  
char *player_get_name (Player *p){
    if(p){
      return p->name;
    }
    return NULL;
  }

  int player_printallnames(Player ** p, int n_players){
    int i;
    int sum = 0;
    if(p == NULL)
      return -1;
    for(i = 0; i < n_players; i++)
      sum += printf("Player number %d with name: %s", i, p[i]->name);
    return sum;
  }

  /* 
      ******MONEY****** 
                          */
  int player_set_money (Player *p, long amount){
    assert(p!=NULL);
      
    p->money=amount;
    return OK;
  }

  long player_get_money (Player *p){
    assert(p!=NULL);
    return p->money;
  }

  int player_show_money (Player *p){
    assert(p!=NULL);
      printf("Amount of money you have: %li\n",p->money);
    return OK;
  }

  /* 
      ******LABS****** 
                          */
  int player_increase_nlabs (Player *p){
    assert(p!=NULL);
      p->n_labs+=1;
    return OK;
  }

  int player_decrease_nlabs (Player *p){
    assert(p!=NULL);
      p->n_labs=p->n_labs-1;
      p->n_labs_destroyed+=1;
    return OK;
  }

  int player_get_nlabs (Player *p){
    assert(p!=NULL);
    return p->n_labs;
  }

  int player_show_nlabs (Player *p){
    assert(p!=NULL);
      printf("You have %d laboratories\n",p->n_labs);
    return OK;
  }

  int player_increase_nlabsdestroyed(Player *p){
    assert(p!=NULL);
      p->n_labs_destroyed += 1;
    return OK; 
  }

  int player_get_nlabsdestroyed (Player *p){
    assert(p!=NULL);
    return p->n_labs_destroyed;
  }

  int player_show_nlabsdestroyed (Player *p){
    assert(p!=NULL);
      printf("The police has discovered %d laboratories\n",p->n_labs_destroyed);
    return OK;
  }

  int player_add_lab(Player *p, Building *bu){
    assert(p!=NULL);
    assert(bu!=NULL);
    if(list_insertLast(p->p_labs,bu)==ERROR)
     return ERROR;
    player_increase_nlabs(p);
    return OK;
  }

  Building *player_get_lab(Player *p, int i){
    assert(p!=NULL);
    return list_get(p->p_labs,i);
  }

  /* 
      ******REST****** 
                        */
  int player_increase_nrest(Player *p){
    assert(p!=NULL);
      p->n_restaurants+=1;
    return OK;
  }

  int player_decrease_nrest(Player *p){
    assert(p!=NULL);
      p->n_restaurants=p->n_restaurants-1;
    return OK;
  }

  int player_get_nrest (Player *p){
    assert(p!=NULL);
    return p->n_restaurants;
  }

  int player_show_nrest(Player *p){
    assert(p!=NULL);
      printf("You have %d restaurants\n",p->n_restaurants);
    return OK;
  }

  int player_add_rest(Player *p, Building *bu){
    assert(p!=NULL);
    assert(bu!=NULL);

    p->p_restaurants[player_get_nrest(p)]=bu;
    player_increase_nrest(p);
    return OK;
  }

  Building *player_get_rest(Player *p, int i){
    assert(p!=NULL);
    return p->p_restaurants[i];
  }
int player_increase_ntraphouse(Player *p){
  assert(p!=NULL);
  p->n_traphouse++;
  return OK;
}
int player_get_ntraphouse(Player *p){
  assert(p!=NULL);
  return p->n_traphouse;
}
Building *player_get_traphouse(Player *p, int i){
  assert(p != NULL);
  if(i < p->n_traphouse)
    return p->p_traphouse[i];
  return NULL;
}
int player_add_traphouse(Player *p, Building *bu){
  assert(p!= NULL);
  p->p_traphouse[p->n_traphouse++] = bu;
  return OK;
}
/* 
    ******VIGILANCE****** 
                            */
long player_get_vigilance(Player *p){
  assert(p!=NULL);
  return p->vigilance;
}

int player_set_vigilance(Player *p, long vigilance){
  assert(p!=NULL);
  if(vigilance <0)
    p->vigilance = 0;
  else if(vigilance > 100)
    p->vigilance = 100;
  else
    p->vigilance = vigilance;
  return OK;
}

int player_add_vigilance(Player *p, long vigilance){
  assert(p!=NULL);
  if(vigilance <0)
    p->vigilance = 0;
  else if(vigilance > 100)
    p->vigilance = 100;
  else
    p->vigilance+=vigilance;
  if(p->vigilance <0)
    p->vigilance = 0;
  else if(p->vigilance > 100)
    p->vigilance = 100;
 
  return OK;
}

int player_show_vigilance(Player *p){
  assert(p!=NULL);
    printf("Your vigilance level is: %ld\n",p->vigilance);
  return OK;
}


