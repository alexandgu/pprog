/**
 * @file printer.h
 * @author     Carlos Gómez-Lobo Hernaiz
 * @date       10 Jan 2019
 * @brief      Contains the prototypes and structures of the printer ADT.
 *
 *             The printer ADT is the one which manages what function prints
 *             something on the screen at any moment.
 */

#ifndef PRINTER_H
#define PRINTER_H

#include "../utils/queue.h"
#include "../utils/types.h"

/**
 * @brief      Type of printing functions.
 */
typedef enum {TEXT = 0, BOXED_TEXT, IMAGE, BACKGROUND, STD, MAP, CURSOR_ERASE, SQUARE, 
	LETTER} Function_type;

typedef struct _Image Image;
typedef struct _Map Map;

/**
 * @brief      Type of text.
 */
typedef enum {SELECTED = 0, NOT_SELECTED} Text_t;
/**
 * @brief      Structure that stores the funcitons to print
 *
 *             In the Function structure the function is stored in the funciton
 *             variable, the function type in the type variable and its
 *             arguments in their corresponding.
 */
typedef struct function{

	void *function;
	Function_type type;
	int x, y, side;
	int *px, *py;
	int height, width;
	unsigned char r, g, b;
	char *text;
	char letter;
	void *source;
	char option;
	Text_t text_t;

} Function , *Pfunction;
/**
 * @brief Pointer function for the print_text function
 */
typedef Status (*printTextFunction) (char *text, int y, int x, unsigned char r, 
	unsigned char g, unsigned char b);
/**
 * @brief Pointer function for the print_letter funcion
 */
typedef Status (*printLetterFunction) (char letter, int *y, int *x);
/**
 * @brief Pointer function for the print_BoxedText function
 */
typedef Status (*printBoxedTextFunction) (char *text, int y, int x, Text_t type);
/**
 * @brief Pointer function for the print_image function
 */
typedef Status (*printImageFunction) (Image *image, int y, int x);
/**
 * @brief Pointer function for the print_background function
 */
typedef void (*printBackgroundFunction) (int y, int x, int height, int width, 
	unsigned char r, unsigned char g, unsigned char b);
/**
 * @brief Pointer function for standard function 
 * 
 * Its a function pointer for functions which recieve no arguments and has a void return
 * value.
 */
typedef void (*printStdFunction) ();
/**
 * @brief Pointer function for the print_minimap function
 */
typedef Status (*printMinimapFunction) (Map *map);
/**
 * @brief Pointer function for the erase_Cursor functions
 */
typedef void (*eraseCursorFunction) (char option);
/**
 * @brief Pointer function for the print_square function
 */
typedef void (*printSquareFunction) (int side, int y, int x,
	unsigned char r, unsigned char g, unsigned char b);

/**
 * @brief      Destroy element function. (Not needed)
 *
 * @param      function 	Function that destroys the queue elements
 */
void destroy_function (void *);
/**
 * @brief      Copy element function. (Not needed)
 *
 * @param		funciton 	Function that copys the queue elements.
 *
 * @return     Copied element
 */
void *copy_function (const void *);

/**
 * @brief      Print element function. (Not needed)
 *
 * @param      fp         Printing output
 * @param		function 	Function that prints the element
 *
 * @return     Number of elements printed.
 */
int print_function (FILE *fp, const void *);

/**
 * @brief      The function which extracts and executes the queue functions
 *
 * @param 		q     Printer queue pointer
 *
 * @return     OK or ERROR exit
 */
Status function_executor(Queue *q);

/**
 * @brief      Adds an image to the printer queue
 *
 * @param 	 q     Printer queue pointer
 * @param 	 img   The image to be printed
 * @param 	 y     Coordinate y
 * @param 	 x     Coordinate x
 *
 * @return    OK or ERROR exit
 */
Status add_image (Queue *q, Image *img, int y, int x);
Status print_image (Image *image, int y, int x);

/**
 * @brief      Adds the print_letter function to the printer queue.
 *
 * @param 	   q      Printer queue pointer
 * @param 	   letter  The letter to be printed 
 * @param 	   y       Coordinate y
 * @param 	   x       Coordinate x
 *
 * @return     OK or ERROR exit
 */
Status add_letter (Queue *q, char letter, int *y, int *x);
Status print_letter (char letter, int *y, int *x);

/**
 * @brief      Adds the print_text function to the printer queue.
 *
 * @param      q     The printer queue pointer
 * @param      text  The text to be printed
 * @param 	   y     Coordinate y
 * @param 	   x     Coordinate x
 * @param 	   r     Red color parameter
 * @param 	   g     Green color paramenter
 * @param 	   b     Blue color parameter
 *
 * @return     OK or ERROR exit
 */
Status add_text (Queue *q, char *text, int y, int x, unsigned char r, unsigned char g, unsigned char b);
Status print_text (char *text, int y, int x, unsigned char r, unsigned char g, unsigned char b);

/**
 * @brief      Adds the print_boxedText function to the prineter queue.
 *
 * @param      q     The printer queue pointer
 * @param      text  The text to be printed
 * @param 	   y     Coordinate y
 * @param 	   x     Coordinate x
 * @param 	   type  SELECTED or NOT_SELECTED text
 *
 * @return     OK or ERROR exit
 */
Status add_boxedText(Queue *q, char *text, int y, int x, Text_t type);
Status print_boxedText (char *text, int y, int x, Text_t type);

/**
 * @brief      Adds th print_backgraund funciton to the printer queue.
 *
 * @param      q     The printer queue pointer
 * @param      text  The text to be printed
 * @param 	   y     Coordinate y
 * @param 	   x     Coordinate x
 * @param 	   height  The height of the background
 * @param 	   width   The width of the background
 * @param 	   r     Red color parameter
 * @param 	   g     Green color paramenter
 * @param 	   b     Blue color parameter
 *
 * @return     OK or ERROR exit
 */
Status add_background (Queue *q, int y, int x, int height, int width, 
	unsigned char r, unsigned char g, unsigned char b);
void print_background (int y, int x, int height, int width, 
	unsigned char r, unsigned char g, unsigned char b);

/**
 * @brief      Adds a standard function to the printer queue.
 *
 * @param      q         The printer queue pointer
 * @param      function  The function to be called
 *
 * @return     OK or ERROR exit
 */
Status add_stdFunction (Queue *q, void *function);
void print_border ();

/**
 * @brief      Adds the print_minimap funciton to the printer queue.
 *
 * @param      q     The quarter
 * @param      map   The map
 *
 * @return     OK or ERROR exit
 */
Status add_minimap (Queue *q, Map *map);
Status print_minimap (Map *map);

/**
 * @brief      Adds the print_block function to the printer queue.
 *
 * @param      q     The printer queue pointer
 * @param      map   The map pointer
 *
 * @return     OK or ERROR exit
 */
Status add_blockMenu (Queue *q, Map *map);
Status print_blockMenu (Map *map);

/**
 * @brief      Adds the blockMenu_eraseCursor to the printer queue.
 *
 * @param      q         The queue pointer
 * @param 	   option    The optiojn to erase cursor from
 * @param      function  The function to erase the cursor
 *
 * @return     OK or ERROR exit
 */
Status add_blockMenu_eraseCursor (Queue *q, char option, void *function);
void blockMenu_eraseCursor (char option);

/**
 * @brief      Adds the print_square funciton to the printer queue.
 *
 * @param      q     The printer queue pointer
 * @param 	   side  The side of the square
 * @param 	   y     Coordinate y
 * @param 	   x     Coordinate x
 * @param 	   r     Red color parameter
 * @param 	   g     Green color paramenter
 * @param 	   b     Blue color parameter
 *
 * @return     OK or ERROR exit
 */
Status add_square (Queue *q, int side, int y, int x,
	unsigned char r, unsigned char g, unsigned char b);
void print_square (int side, int y, int x,
	unsigned char r, unsigned char g, unsigned char b);

#endif