
#ifndef REFRESH_H
#define REFRESH_H
#include "player.h"

#include "../construction/map.h"
#include "../construction/block.h"
#include "../utils/includes.h"
#include "../utils/permutaciones.h"
#include "../game/game.h"

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "../utils/types.h"
#define ERR -1

int bribe_flag, caught_lab;

int refresh_all(Player *p);

void refresh_clock ();

/*
    MONEY
            */
int refresh_money_formula(Player *p);
/* Calculate the money earned by the player from the restaurants and subtract money for every lab lost
    Input: pointer to player
    Output: OK / GAMEOVER
*/
long refresh_moneyrest (Block *b, Building *bu);
/* Calculate the money earned from a restaurant depending on where it is placed and its level
    Input: pointer to block / pointer to restaurant
    Output: money earned
*/
long refresh_moneylabsdest(int num_labsdest);
/* Calculate the money lost from a caught lab
    Input: number of caught labs
    Output: money lost
*/
int refresh_addmoney(Player *p, long money);
/* Add "money" to p->money
    Input: pointer to player
    Output: OK
*/
int refresh_subtractmoney(Player *p, long money);
/* Subtract "money" to p->money
    Input: pointer to player
    Output: OK / ERROR/ GAMEOVER
*/
int refresh_moneyspent(Player *p, Building *b);
/*Calculate the money spent by a player from buying a new building depending on where it is placed
    Input: pointer to player / pointer to new building
    Output: OK / ERROR / GAMEOVER
*/
int refresh_moneyspentupgrade_building(Player *p, Building *bu);
/*Calculate the money spent by a player from increasing a building´s level depending on where it is placed
    Input: pointer to player / pointer to building
    Output: OK / ERROR / GAMEOVER
*/
int refresh_moneyspentupgrade_block(Player *p, Block *b);
/*Calculate the money spent by a player from increasing a block´s level depending on its type
    Input: pointer to player / pointer to block
    Output: OK / ERROR / GAMEOVER
*/

/*
    VIGILANCE
                */
int refresh_vigilance_formula(Player *p);
/* Calculate the vigilance on the player based on the vigilance of his labs 
    Input: pointer to player
    Output: OK / GAMEOVER / WARNING
*/
long refresh_vigilancelab(Block *b, Building *bu);
/* Calculate the vigilance of a laboratory based on where it is placed and its level
    Input: pointer to block / pointer to lab
    Output: vigilance / WARNING / CAUGHTLAB
*/
long refresh_vigilancerest(Block *b, Building *bu);
/* Subtract vigilance because of the restaurants owned by the player
    Input: pointer to block / pointer to restaurant
    Output: vigilance to subtract
*/
long refresh_vigilancelabsdest(int num_labsdest);
/*Calculate the vigilance added because of the destroyed labs
    Input: number of lost labs
    Output: vigilance to add
*/

/*
    DRUGS
            */
int refresh_drugs_formula(Player *p);
/* Calculate the amount of drugs from every lab of the player 
    Input: pointer to player
    Output: OK 
*/
int refresh_druglab (Block *b, Building *bu);
/* Calculate the drugs created in a lab
    Input: pointer to block / pointer to lab
    Output: OK
*/
int refresh_subtract_drugs(Building *bu, long drugs);
/* Subtract "drugs" to bu->drugs
    Input: pointer to building
    Output: OK / ERROR
*/
int refresh_moneydrugdealing(Player *p, Building *bu);
/* Calculate the money earned by the sale of drugs depending on where the trap house is placed
    Input: pointer to player / pointer to trap house
    Output: OK
*/

/*
    EXTRAS
            */
int refresh_caughtlab(Player *p, int i);
/* Delete a lab (number i) from the list of labs of the player, decrease number of labs, increase number of labs destroyed
    Input: pointer to player / number of the caught lab
    Output: OK
*/
void refresh_police_raid(Player *p);
/* Decide randomly if the player loses a lab, pays a fine or stays the same
    Input: pointer to player
*/
int refresh_police_bribe(Player *p, long money);
/*Decide if the police takes the money or not basing of the amount you payed and in a random number
    Input: pointer to player / money for the bribe
    Output: OK (if the police takes the bribe) / ERROR (if they don't)
*/

Status show_bottomInfo(char *text, unsigned char r, unsigned char g, unsigned char b);

#endif
