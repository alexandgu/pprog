#include "interface.h"

#include <string.h>
#include <unistd.h>

struct _Image{

	Image_type type;
	int height;
	int width;
	unsigned char ***color;
};
		
Image *selector = NULL;
Image *village;
Image *route;
Image *highway;
Image *city;
Image *desert;
Image *pollos;
Image *lab;
Image *trap_house;
Image *restaurant;
Image *explored_icon;

void print_background (int y, int x, int height, int width, unsigned char r, unsigned char g, unsigned char b){

	int i, j;

	CHANGE_COLOR(r, g, b);

	for (i = y; i < height + y; i++){
		MOVE_CURSOR(i, x);
		for (j = x; j < x + width; j++) PRINT_SPACE;
	}
}

char *format (char letter){

	char *formatted_char, aux[2];

	formatted_char = (char *) malloc((strlen(LETTERS_PATH) + 6)*sizeof(char));
	if(formatted_char == NULL) return NULL;

	aux[0] = letter;
	aux[1] = 0;
	
	sprintf(formatted_char, "%s%s.txt", LETTERS_PATH, aux);

	return formatted_char;
}

Status print_letter (char letter, int *y, int *x){

	FILE *fp;
	char *file, read;
	int hcont1 = 0, hcont2 = 0, vcont = 0;
	Status flag = OK;

	switch (letter){

		case ' ':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(SPACE_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH SPACE_FILE);

			break;

		case '>':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(BLOCK_MENU_CURSOR_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH BLOCK_MENU_CURSOR_FILE);

			break;

		case '.':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(POINT_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH POINT_FILE);

			break;

		case ',':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(COMMA_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH COMMA_FILE);

			break;

		case '$':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(DOLLARS_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH DOLLARS_FILE);
			(*y)--;

			break;

		case '\'':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(APOSTROPHE_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH APOSTROPHE_FILE);
			(*y)--;

			break;

		case ':':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(TWO_DOTS_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH TWO_DOTS_FILE);

			break;

		case '-':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(HYPHEN_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH HYPHEN_FILE);

			break;

		case '?':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(Q_MARK_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH Q_MARK_FILE);

			break;

		case '!':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(E_MARK_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH E_MARK_FILE);

			break;

		case '/':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(SLASH_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH SLASH_FILE);

			break;


		default:

			file = format(letter);
			if (file == NULL) return ERROR;

			break;
	}

	fp = fopen(file, "r");
	if (fp == NULL){

		free(file);
		fprintf(stderr, "Couldn't open the file %c.\n", letter);
		return ERROR;
	}

	MOVE_CURSOR(*y, *x);

	read = getc(fp);

	while (flag == OK){

		switch (read){

			case ' ' : 
				MOVE_FORWARD;
				hcont1++; break;

			case '#': 
				PRINT_SPACE; 
				hcont1++; break;

			case '\n': 
				MOVE_CURSOR(++*y, *x);
				vcont++;
				if (hcont1 > hcont2) hcont2 = hcont1;
				hcont1 = 0;
				break;

			default: break;
		}

		if ((read = getc(fp)) < 0) flag = END;
	}

	if (hcont1 > hcont2) hcont2 = hcont1;

	*x+=hcont2;
	*y-=vcont;

	if (letter == '$' || letter == '\'') (*y)++;

	fclose(fp);
	free(file);

	return OK;
}

	Status print_text (char *text, int y, int x, unsigned char r, unsigned char g, unsigned char b){

		int i, len;;
		Status flag = OK;

		if (text == NULL || x < 0 || y < 0) return ERROR;

		CHANGE_COLOR(r, g, b);

		len = strlen(text);

		for (i = 0; i < len && flag == OK; i++){

			flag = print_letter(text[i], &y, &x);
			MOVE_FORWARD;
			x++;
		}

	return OK;
}

Status scan_int (FILE *fp){

	int i;

	if (fp == NULL) return -1;

	if(fread(&i, sizeof(int), 1, fp) != 1)
	;

	return i;
}

short scan_short(FILE *fp){

	short n;

	if (fp == NULL) return -1;

	if(fread(&n, sizeof(short), 1, fp) != 1)
	;

	return n;
}

char scan_byte(FILE *fp){

	char c;

	if (fp == NULL) return -1;

	if(fread(&c, sizeof(char), 1, fp) != 1) return ERROR;

	return c;
}

Status scan_NInt(FILE *fp, int n){

	int *i;

	if (fp == NULL) return ERROR;

	i = (int *) malloc (n * sizeof(int));
	if (i == NULL) return ERROR;

	if (fread (i, sizeof(int), n, fp) != n) return ERROR;
	free(i);

	return OK;
}

Status scan_NShort(FILE *fp, short n){

	short *s;

	if (fp == NULL) return ERROR;

	s = (short *) malloc (n * sizeof(short));
	if (s == NULL) return ERROR;

	if (fread (s, sizeof(short), n, fp) != n) return ERROR;
	free(s);

	return OK;
}

Status BMP_check (FILE *fp){

	char c[2];

	if (fp == NULL) return ERROR;

	if (fread(c, sizeof(char), 2, fp) != 2) return ERROR;

	if (c[0] != 'B' || c[1] != 'M') return ERROR;

	return OK;
}

Image_type BMP_init(FILE *fp, int *size){

	int jump, read;
	short type;
	Image_type imgtype;

	if(fp == NULL || size == NULL) return IMG_ERROR;

	if(BMP_check(fp) == ERROR) return IMG_ERROR;

	if (scan_NInt(fp, 2) == ERROR) return IMG_ERROR;

	if ((read = scan_int(fp)) < 0) return 0;

	jump = read/2 - 15;

	if (scan_int(fp) < 0) return ERROR;

	if ((size[1] = scan_int(fp)) < 0) return IMG_ERROR;
	if ((size[0] = scan_int(fp)) < 0) return IMG_ERROR;

	if (scan_short(fp) < 0) return IMG_ERROR;

	if ((type = scan_short(fp)) < 0) return IMG_ERROR;

	switch(type){

		case 24: imgtype = RGB_TYPE; break;
		case 32: imgtype = RGBALPHA_TYPE; break;
		default: return ERROR;
	}

	if (scan_NShort(fp, jump) == ERROR) return IMG_ERROR;

	return imgtype;
}

Status scan_pixel (FILE *fp, unsigned char *color, Image_type imgtype){

	unsigned char c[3];
	char dum;

	if (fp == NULL || color == NULL) return ERROR;

	if (fread (c, sizeof(char), 3, fp) != 3) return ERROR;
	if (imgtype == RGBALPHA_TYPE){
		if (fread(&dum, sizeof(char), 1, fp) != 1) return ERROR;
	}

	color[0] = (unsigned char) c[2];
	color[1] = (unsigned char) c[1];
	color[2] = (unsigned char) c[0];

	return OK;
}

Image *scan_image(char *file_name, Image_type type){

	int size[2];
	int i, j, n_pixels;
	Image *img;
	char *file;
	FILE *fp;
	Image_type img_color_type;

	if (file_name == NULL) return NULL; 

	file = (char *) malloc((strlen(IMAGES_PATH) + strlen(file_name) + 1) * sizeof(char));
	if (file == NULL) return NULL;

	strcpy(file, IMAGES_PATH);
	strcat(file, file_name);

	fp = fopen(file, "rb");
	if (fp == NULL) {

		fprintf(stderr, "File %s couldn't be opened.\n", file_name);
		return NULL;
	}

	if ((img_color_type = BMP_init(fp, size)) == IMG_ERROR){
		fclose(fp);
		fprintf(stderr, "Invalid format.\n");
		return NULL;
	}

	if (size[0] > SCREEN_HEIGHT || size[1] > SCREEN_WIDTH){
		fclose(fp);
		fprintf(stderr, "The image is too big.\n");
		return NULL;
	}

	img = (Image *) malloc(sizeof(Image));
	if (img == NULL) return NULL;

	img->height = size[0];
	img->width = size[1];

	n_pixels = size[0] * size[1];

	img->color = (unsigned char ***) malloc(size[0] * sizeof(unsigned char **));
	if (img->color == NULL) return NULL;

	img->color[0] = (unsigned char **) malloc(n_pixels * sizeof(unsigned char *));
	if (img->color[0] == NULL) return NULL;

	for (i = 0; i < size[0]; i++) img->color[i] = &(img->color[0][size[1] * i]);

	img->color[0][0] = (unsigned char *) malloc(3 * n_pixels * sizeof(unsigned char));
	if (img->color[0][0] == NULL) return NULL;

	for (i = 0; i < n_pixels; i++) img->color[0][i] = &(img->color[0][0][3 * i]);

	for (i = 0; i < size[0]; i++){
		for (j = 0; j < size[1]; j++){

			if (scan_pixel(fp, img->color[size[0] -i - 1][j], img_color_type) == ERROR) return NULL;
		}
	}

	img->type = type;

	free(file);
	fclose(fp);

	return img;
}

Status print_image (Image *image, int y, int x){

	int i = 0, j, height, width, jaux = 0;

	if (image == NULL) return ERROR;

	if (image->type == MAP_IMAGE){
		if (y > MAX_MAP_COORD_Y) return OK;
		if (x > MAX_MAP_COORD_X) return OK;

		if (y < 0){
			i = -y;
		}
		if (y + image->height >= MAX_MAP_COORD_Y) height = MAX_MAP_COORD_Y - y;
		else height = image->height;

		if (x < 0){
			jaux = -x;
			x = 1;
		}
		if (x + image->width >= MAX_MAP_COORD_X) width = MAX_MAP_COORD_X - x;
		else width = image->width;
	}
	else{
		height = image->height;
		width = image->width;
	}

	for (; i < height; i++){
		MOVE_CURSOR(y+i, x);
		for (j = jaux; j < width; j++){

			if(image->color[i][j][0] == 0 && image->color[i][j][1] == 0 && image->color[i][j][2] == 0) MOVE_FORWARD;
			else if(image->color[i][j][0] == 45 && image->color[i][j][1] == 255 && image->color[i][j][2] == 0){
				CHANGE_COLOR(BGCOLOR);
				PRINT_SPACE;
			}
			else {
				CHANGE_COLOR(image->color[i][j][0], image->color[i][j][1], image->color[i][j][2]);
				PRINT_SPACE;
			}
		}
	}

	return OK;
}

void destroy_image(Image *image){

	free(image->color[0][0]);
	free(image->color[0]);
	free(image->color);
	free(image);
}

void free_images(){

	destroy_image(selector);
	destroy_image(village);
	destroy_image(route);
	destroy_image(highway);
	destroy_image(city);
	destroy_image(desert);
	destroy_image(lab);
	destroy_image(restaurant);
	destroy_image(trap_house);
	destroy_image(explored_icon);
}

void empty_map(){

	add_background(q, 1, 1, MAX_MAP_COORD_Y - 1, MAX_MAP_COORD_X - 1, BGCOLOR);
}

void empty_screen(int r, int g, int b){

	add_background(q, 1, 1, SCREEN_HEIGHT, SCREEN_WIDTH, r, g, b);
}

Status generate_screen (Screen screen){

	switch(screen){

		case GAME_SCREEN:

			if (selector == NULL){

				selector = scan_image(SELECTOR, MAP_IMAGE);
				if (selector == NULL) return ERROR;

				village = scan_image(VILLAGE_LAYOUT, MAP_IMAGE);
				if (village == NULL) return ERROR;

				route = scan_image(ROUTE_LAYOUT, MAP_IMAGE);
				if (route == NULL) return ERROR;

				highway = scan_image(HIGHWAY_LAYOUT, MAP_IMAGE);
				if (highway == NULL) return ERROR;

				city = scan_image(CITY_LAYOUT, MAP_IMAGE);
				if (city == NULL) return ERROR;

				desert = scan_image(DESERT_LAYOUT, MAP_IMAGE);
				if (desert == NULL) return ERROR;

				lab = scan_image (LAB_LAYOUT, OTHER_IMAGE);
				if (lab == NULL) return ERROR;

				restaurant = scan_image (REST_LAYOUT, OTHER_IMAGE);
				if (lab == NULL) return ERROR;

				trap_house = scan_image (TRAP_HOUSE_LAYOUT, OTHER_IMAGE);
				if (lab == NULL) return ERROR;

				explored_icon = scan_image (EXPLORED_ICON_FILE, MAP_IMAGE);
				if (lab == NULL) return ERROR;
			}

			empty_screen(BGCOLOR);
			
			add_stdFunction(q, print_border);
			
			break;

		case MAIN_SCREEN: 

			empty_screen(BGCOLOR);

			pollos = scan_image(POLLOS_IMAGE, OTHER_IMAGE);
			if (pollos == NULL) return ERROR;

			add_image(q, pollos, POLLOS_IMG_COORD);

			add_boxedText(q, "NEW GAME", NEW_GAME_COORD, SELECTED);
			add_boxedText(q, "LOAD GAME", LOAD_GAME_COORD, NOT_SELECTED);

			break;

		default: return ERROR;
	}

	return OK;
}
Status print_boxedLetter (char letter, int *y, int *x, Text_t type){

	FILE *fp;
	char *file, read;
	int hcont1 = 0, hcont2 = 0, vcont = 0, i;
	Status flag = OK;

	MOVE_CURSOR(*y, *x);

	switch (letter){

		case ' ':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(SPACE_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH SPACE_FILE);

			break;

		case '>':

			file = (char *) malloc((strlen(LETTERS_PATH) + 
				strlen(BLOCK_MENU_CURSOR_FILE) + 1) * sizeof(char));
			if (file == NULL) return ERROR;

			strcpy(file, LETTERS_PATH BLOCK_MENU_CURSOR_FILE);

			break;

		default:

			file = format(letter);
			if (file == NULL) return ERROR;

			break;
	}

	fp = fopen(file, "r");
	if (fp == NULL){

		free(file);
		fprintf(stderr, "Couldn't open the file.");
		return ERROR;
	}

	read = getc(fp);

	while (flag == OK){

		switch (read){

			case ' ' : 
				MOVE_FORWARD;
				hcont1++; break;

			case '#': 
				PRINT_SPACE; 
				hcont1++; break;

			case '\n':
				vcont++;
				MOVE_CURSOR(*y + vcont, *x);
				if (hcont1 > hcont2) hcont2 = hcont1;
				hcont1 = 0;
				break;

			default: break;
		}

		if ((read = getc(fp)) < 0) flag = END;
	}

	if (hcont1 > hcont2) hcont2 = hcont1;

	MOVE_CURSOR(*y - 4, *x);

	for (i = 0; i < hcont2; i++) PRINT_SPACE;

	switch (type){

		case SELECTED:

			CHANGE_COLOR(RED);
			break;

		case NOT_SELECTED:

			CHANGE_COLOR(BGCOLOR);
			break;
		
		default: break;
	}

	MOVE_CURSOR(*y - 3, *x);

	for (i = 0; i < hcont2; i++) PRINT_SPACE;

	MOVE_CURSOR(*y + 7, *x);

	for (i = 0; i < hcont2; i++) PRINT_SPACE;

	CHANGE_COLOR (LETTER_COLOR);
	MOVE_CURSOR(*y + 8, *x);

	for (i = 0; i < hcont2; i++) PRINT_SPACE;


	*x+=hcont2;

	fclose(fp);
	free(file);

	return OK;
}

Status print_boxedText (char *text, int y, int x, Text_t type){

	int len, i;
	Status flag = OK;

	if (text == NULL || y <= 0 || x <= 0) return ERROR;

	CHANGE_COLOR(LETTER_COLOR);

	for (i = 0; i < 13; i++){

		MOVE_CURSOR(y + i, x);
		PRINT_SPACE;
	}

	switch (type){

		case SELECTED:

			CHANGE_COLOR(RED);
			break;

		case NOT_SELECTED:

			CHANGE_COLOR(BGCOLOR);
			break;
		
		default: break;
	}

	for (i = 1; i < 12; i++){

		MOVE_CURSOR(y + i, x + 1);
		PRINT_SPACE;
	}
	for (i = 1; i < 3; i++){

		MOVE_CURSOR(y + 1, x + i);

		PRINT_SPACE;
		MOVE_CURSOR(y + 11, x + i);
		PRINT_SPACE;
	}
	CHANGE_COLOR(LETTER_COLOR);

	for (i = 1; i < 3; i++){

		MOVE_CURSOR(y, x + i);

		PRINT_SPACE;
		MOVE_CURSOR(y + 12, x + i);
		PRINT_SPACE;
	}

	y += 4;
	x += 3;

	len = strlen(text);

	for (i = 0; i < len && flag == OK; i++){

		flag = print_boxedLetter(text[i], &y, &x, type);

		MOVE_CURSOR(y - 4, x);
		PRINT_SPACE;
		MOVE_CURSOR(y + 8, x);
		PRINT_SPACE;

		switch (type){

			case SELECTED:

				CHANGE_COLOR(RED);
				break;

			case NOT_SELECTED:

				CHANGE_COLOR(BGCOLOR);
				break;
			
			default: break;
		}

		MOVE_CURSOR(y - 3, x);
		PRINT_SPACE;
		MOVE_CURSOR(y + 7, x);
		PRINT_SPACE;
		CHANGE_COLOR(LETTER_COLOR);

		MOVE_CURSOR(x, y);
		MOVE_FORWARD;
		x++;
	} if (flag == ERROR) return ERROR;



	MOVE_CURSOR(y - 4, x);
	PRINT_SPACE;
	MOVE_CURSOR(y + 8, x);
	PRINT_SPACE;

	switch (type){

		case SELECTED:

			CHANGE_COLOR(RED);
			break;

		case NOT_SELECTED:

			CHANGE_COLOR(BGCOLOR);
			break;
		
		default: break;
	}

	for (i = 0; i < 11; i++){

		MOVE_CURSOR(y - 3 + i, x);
		PRINT_SPACE;
	}
	CHANGE_COLOR(LETTER_COLOR);

	for (i = 0; i < 13; i++){

		MOVE_CURSOR(y - 4 + i, x + 1);
		PRINT_SPACE;
	}

	return OK;
}

int x_mapCoord (int y, int x){ 

	int ret;

	if (layout == UP_LEFT || layout == L_LEFT || layout == BOTTOM_LEFT)
		ret = MAP_COORD_X + 22 * x;
	else if (layout == UP_RIGHT || layout == L_RIGHT || layout == BOTTOM_RIGHT)
		ret = MAP_COORD_X + 22 * x - 30;
	else ret = MAP_COORD_X + 22 * x - 10;

	return ret; 
}

int y_mapCoord (int y, int x){

	int ret;

	if (layout == L_UP || layout == UP_LEFT || layout == UP_RIGHT)
		ret = MAP_COORD_Y + 26 * y;
	else ret = MAP_COORD_Y + 26 * y - 14;
	if (x%2 == 1) ret += 13;

	return ret; 
}

Status print_block(int y, int x, Block *block){

	char levelbuf[4];
	int nbuild = block_getnumbuildings(block);

	switch(block_gettype(block)){

		case CITY:

			if (add_image (q, city, y, x) == ERROR) return ERROR;
			break;

		case DESERT:

			if (add_image (q, desert, y, x) == ERROR) return ERROR;
			break;

		case ROUTE:

			if (add_image (q, route, y, x) == ERROR) return ERROR;
			break;

		case HIGHWAY:

			if (add_image (q, highway, y, x) == ERROR) return ERROR;
			break;

		case VILLAGE:

			if (add_image (q, village, y, x) == ERROR) return ERROR;
			break;

		default: return ERROR;
	}

	if (nbuild > 0 && y < MAX_MAP_COORD_Y - 20){
		if(add_image(q, explored_icon, y + 11, x + 13) == ERROR)
			return ERROR;
		sprintf(levelbuf, "%hd", block_getlevel(block));

		if (block_getlevel(block) < 10){
			if (add_background(q, y + 18, x + 9, 5, 4, GREY));
		}
		else{
			if (add_background(q, y + 18, x + 9, 5, 9, GREY));
		}
		if (add_text(q, levelbuf, y + 18, x + 9, LETTER_COLOR) == ERROR) return ERROR;
	}

	return OK;
}

Status generate_map (Map *map){

	int i, j, oy, ox;

	if (map == NULL) return ERROR;
	oy = map_getOffsetCoordY(map);
	ox = map_getOffsetCoordX(map);

	if (layout != FULL_SCREEN)
		if (add_background(q, 1, 1, MAX_MAP_COORD_Y - 1, MAX_MAP_COORD_X - 1, BGCOLOR) == ERROR)
			return ERROR;
	fprintf(stderr, "1\n");
	for (j = 0; j < MAX_SHOWABLE_MAP_HEIGHT; j++){
		for (i = 0; i < MAX_SHOWABLE_MAP_WIDTH; i++){

			if (print_block (y_mapCoord(j, i), x_mapCoord(j, i), 
					map_getBlock(map, (unsigned short) oy + j, 
					(unsigned short) ox + i)) == ERROR){

				fprintf (stderr, "Error processing the image\n");
				return ERROR;
			}
			
		}
	}
	if (select_block(map) == ERROR) return ERROR;

	return OK;
}

Status select_block (Map *map){

	char coord[10];
	add_background(q, COORD_PRINT_COORD_Y, COORD_PRINT_COORD_X, 8, 30, BGCOLOR);
	sprintf(coord, "%d - %d", map_getCurrentCoordY(map), map_getCurrentCoordX(map));

	add_text (q, coord, COORD_PRINT_COORD_Y, COORD_PRINT_COORD_X, LETTER_COLOR);

	if (add_image(q, selector, y_mapCoord (layout_coord_y, layout_coord_x), 
		x_mapCoord (layout_coord_y, layout_coord_x)) == ERROR) return ERROR;

	return OK;
}

Status print_fullMap(Map *map){

	char input;

	if (map == NULL) return ERROR;

	add_background(q, FULL_MAP_COORD_Y, FULL_MAP_COORD_X - 5, MAP_HEIGHT + 2 * FULL_MAP_MARGIN, 
		MAP_WIDTH + 2 * FULL_MAP_MARGIN + 10, FULL_MAP_BG_COLOR);

	add_minimap(q, map);

	while((input = getchar()) != 'm' && input != 127);

	if(generate_map(map) == ERROR) return ERROR;
	if(select_block(map) == ERROR) return ERROR;

	return OK;
}

int square_coord_x (int factor){

	int x_margin = 10;
	int mwidth = MAX_MAP_COORD_X - 2 * BLOCK_MENU_COORD_X;
	int offwidth = mwidth - 2 * x_margin;
	int width_factor = ((offwidth / BLOCK_MENU_SQUARE_NUM) - BLOCK_MENU_SQUARE_SIDE) / 2;

	return BLOCK_MENU_COORD_X + x_margin+ width_factor + (offwidth / BLOCK_MENU_SQUARE_NUM) * factor;
}

Status print_blockMenu(Map *map){

	int i, mheight =  MAX_MAP_COORD_Y - 20, mwidth = MAX_MAP_COORD_X - 2 * BLOCK_MENU_COORD_X;
	

	if (map == NULL) return ERROR;

	add_background(q, BLOCK_MENU_COORD_Y, BLOCK_MENU_COORD_X, mheight, mwidth, FULL_MAP_BG_COLOR);

	for (i = 0; i < BLOCK_MENU_SQUARE_NUM; i++)
		add_square(q, BLOCK_MENU_SQUARE_SIDE, BLOCK_MENU_FIRST_SQUARE_Y, 
			square_coord_x (i), BLACK);

	return OK;
}

void print_border(){

	int i;

	CHANGE_COLOR(BLACK);

	MOVE_CURSOR(MAX_MAP_COORD_Y, 1);
	for (i = 0; i < SCREEN_WIDTH; i++) PRINT_SPACE;

	for (i = 0; i < SCREEN_HEIGHT; i++){

		MOVE_CURSOR(1 + i, MAX_MAP_COORD_X);
		PRINT_SPACE;
	}
}

Status print_minimap(Map *map){

	int i, j, cx = map_getCurrentCoordX(map), cy = map_getCurrentCoordY(map);

	for (i = 0; i < map->height; i++){

		MOVE_CURSOR(FULL_MAP_COORD_Y + FULL_MAP_MARGIN + i, FULL_MAP_COORD_X + FULL_MAP_MARGIN);
		for (j = 0; j < map->width; j++){

			switch(block_gettype(map_getBlock(map, (unsigned short) i, 
					(unsigned short) j))){

				case CITY:
					CHANGE_COLOR(CITY_COLOR);
					PRINT_SPACE;
					break;

				case DESERT:

					CHANGE_COLOR(DESERT_COLOR);
					PRINT_SPACE;
					break;

				case ROUTE:

					CHANGE_COLOR(ROUTE_COLOR);
					PRINT_SPACE;
					break;

				case HIGHWAY:

					CHANGE_COLOR(HIGHWAY_COLOR);
					PRINT_SPACE;
					break;

				case VILLAGE:

					CHANGE_COLOR(VILLAGE_COLOR);
					PRINT_SPACE;
					break;

				default: return ERROR;
			}
		}
	}

	CHANGE_COLOR(RED);
	MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN, cx + FULL_MAP_COORD_X + FULL_MAP_MARGIN);
	PRINT_SPACE;

	if (cy > 0){
		MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN - 1, cx + FULL_MAP_COORD_X + 
			FULL_MAP_MARGIN);
		PRINT_SPACE;
		if (cx > 0){
			MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN - 1, cx + FULL_MAP_COORD_X + 
				FULL_MAP_MARGIN - 1);
			PRINT_SPACE;
		}
		if (cx < MAP_WIDTH - 1){
			MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN - 1, cx + FULL_MAP_COORD_X + 
				FULL_MAP_MARGIN + 1);
			PRINT_SPACE;
		}
	}
	if (cx > 0){
		MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN, cx + FULL_MAP_COORD_X + 
			FULL_MAP_MARGIN - 1);
		PRINT_SPACE;
	}
	if (cx < MAP_WIDTH - 1){
		MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN , cx + FULL_MAP_COORD_X + 
			FULL_MAP_MARGIN + 1);
		PRINT_SPACE;
	}
	if (cy < MAP_HEIGHT - 1){
		MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN + 1, cx + FULL_MAP_COORD_X + 
			FULL_MAP_MARGIN);
		PRINT_SPACE;
		if (cx > 0){
			MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN + 1, cx + FULL_MAP_COORD_X + 
				FULL_MAP_MARGIN - 1);
			PRINT_SPACE;
		}
		if (cx < MAP_WIDTH - 1){
			MOVE_CURSOR(cy + FULL_MAP_COORD_Y + FULL_MAP_MARGIN + 1, cx + FULL_MAP_COORD_X + 
				FULL_MAP_MARGIN + 1);
			PRINT_SPACE;
		}
	}

	return OK;
}

void blockMenu_eraseCursor (char option){

	int i;

	CHANGE_COLOR(FULL_MAP_BG_COLOR);

	for(i = 1; i < 4; i++){

		MOVE_CURSOR(BLOCK_MENU_CURSOR_COORD_Y + i + 10 * (option - 1), BLOCK_MENU_CURSOR_COORD_X);
		printf("   ");
	}
}

void print_square (int side, int y, int x, 
	unsigned char r, unsigned char g, unsigned char b){

	int j;

	CHANGE_COLOR(r, g, b);

	MOVE_CURSOR(y, x);
	for (j = 0; j < side + 1; j++) PRINT_SPACE;

	for (j = 1; j < side - 2; j++){
		MOVE_CURSOR(y + j, x);
		PRINT_SPACE;

		MOVE_CURSOR(y + j, x + side);
		PRINT_SPACE;
	}

	MOVE_CURSOR(y + side - 2, x);
	for (j = 0; j < side + 1; j++) PRINT_SPACE;
}

void blockMenu_selectSquare (char pos){

	add_square(q, BLOCK_MENU_SQUARE_SIDE - 2, BLOCK_MENU_FIRST_SQUARE_Y + 1, 
		square_coord_x (pos) + 1, RED);
}

void blockMenu_deselectSquare (char pos){

	add_square(q, BLOCK_MENU_SQUARE_SIDE - 2, BLOCK_MENU_FIRST_SQUARE_Y + 1, 
		square_coord_x (pos) + 1, 
		FULL_MAP_BG_COLOR);
}

Status blockMenu_printBuilding (Building_type build, char pos, unsigned short level){

	char clevel[4];

	switch(build){

		case LAB:

			add_image(q, lab, BLOCK_MENU_FIRST_SQUARE_Y + 2, 
				square_coord_x(pos) + 3);

			break;

		case REST:

			add_image(q, restaurant, BLOCK_MENU_FIRST_SQUARE_Y + 2, 
				square_coord_x(pos) + 3);

			break;

		case TRAPHOUSE:

			add_image(q, trap_house, BLOCK_MENU_FIRST_SQUARE_Y + 2, 
				square_coord_x(pos) + 3);

			break;

		default: break;
	}

	sprintf(clevel, "%hu", level);

	if (level < 10){
		add_background(q, BLOCK_MENU_FIRST_SQUARE_Y + BLOCK_MENU_SQUARE_SIDE - 9, 
				square_coord_x(pos) + BLOCK_MENU_SQUARE_SIDE - 8, 5, 4, GREY);
		add_text (q, clevel, BLOCK_MENU_FIRST_SQUARE_Y + BLOCK_MENU_SQUARE_SIDE - 9, 
				square_coord_x(pos) + BLOCK_MENU_SQUARE_SIDE - 8, LETTER_COLOR);
	}
	
	else{
		add_background(q, BLOCK_MENU_FIRST_SQUARE_Y + BLOCK_MENU_SQUARE_SIDE - 9, 
				square_coord_x(pos) + BLOCK_MENU_SQUARE_SIDE - 13, 5, 9, GREY);
		add_text (q, clevel, BLOCK_MENU_FIRST_SQUARE_Y + BLOCK_MENU_SQUARE_SIDE - 9, 
				square_coord_x(pos) + BLOCK_MENU_SQUARE_SIDE - 13, LETTER_COLOR);
	}


	return OK;
}

void erase_bottomInfo (){

	add_background(q, MAX_MAP_COORD_Y + 2, 1, SCREEN_HEIGHT - MAX_MAP_COORD_Y - 2, MAX_MAP_COORD_X - 1, 
		BGCOLOR);
}

void print_bottomInfo (arg_struct_t *args){

	int i;

	erase_bottomInfo();

	if (strlen(args->text) < 52)
		add_text(q, args->text, BOTTOM_INFO_COORD_Y, BOTTOM_INFO_COORD_X, args->r, args->g, args->b);
	else{

		for (i = 52; args->text[i] != ' '; i--);
		args->text[i] = 0;
		add_text(q, args->text, BOTTOM_INFO_COORD_Y, BOTTOM_INFO_COORD_X, args->r, args->g, args->b);
		add_text(q, (args->text + i + 1), BOTTOM_INFO_COORD_Y + 10, BOTTOM_INFO_COORD_X, 
			args->r, args->g, args->b);
	}

	free(args->text);
	sleep(args->time_s);

	erase_bottomInfo();

	free(args);
}

void screen_setUp (){

	system("stty -echo");
	system("stty raw");
	printf("%c[?25l", 27);
	printf(CSI "2J");
}

void screen_shutDown (){

	printf("%c[?25h", 27);
	printf(CSI "0m");
	system("stty echo");
	system("stty cooked");
}

char *interface_playerInputText(int y, int x){

	char *text, buf;
	short size = 20;
	int cont = 0;

	text = (char *) malloc(size * sizeof(char));
	if (text == NULL) return NULL;

	while (1){

		switch (buf = getchar()){

			case BACK_KEY:

				free(text);
				return NULL;

			case ENTER_KEY:

				text[cont] = 0;
				return text;

			default:

				if ((buf >= '.' && buf <= '9') || (buf >='A' && buf <= 'Z')){

					text[cont++] = buf;

					add_letter (q, buf, &y, &x);
					add_letter (q, ' ', &y, &x);
				}
				break;
		}
	}
}

double interface_playerInputNumber(int y, int x){

	char *text, buf;
	double ret;
	short size = 20;
	int cont = 0;

	text = (char *) malloc(size * sizeof(char));
	if (text == NULL) return -1;

	while (1){

		switch (buf = getchar()){

			case BACK_KEY:

				free(text);
				return -1;

			case ENTER_KEY:

				text[cont] = 0;
				ret = atof(text);
				free(text);
				return ret;

			default:

				if (buf >= '.' && buf <= '9'){

					if (cont == size - 1){
						text = realloc(text, 2 * size);
						size*=2;
					}
					text[cont++] = buf;

					add_letter (q, buf, &y, &x);
					add_letter (q, ' ', &y, &x);
				}
				break;
		}
	}
}

Status input_coordinates(int *ret){

	if (ret == NULL) return ERROR;

	add_background(q, INPUT_COORD_SCREEN_COORD_Y, 
		 INPUT_COORD_SCREEN_COORD_X, INPUT_COORD_SCREEN_HEIGHT, 
		 INPUT_COORD_SCREEN_WIDTH, FULL_MAP_BG_COLOR);

	add_text (q, INPUT_COORD_SCREEN_MSG, INPUT_COORD_SCREEN_COORD_Y + 10, 
		INPUT_COORD_SCREEN_COORD_X + 20, LETTER_COLOR);

	do{
		ret[0] = floor(interface_playerInputNumber(INPUT_COORD_SCREEN_COORD_Y + 20, 
			INPUT_COORD_SCREEN_COORD_X + 30));
		if (ret[0] == -1) return ERROR;
	} while (ret[0] < 0 || ret[0] > MAP_HEIGHT);


	do{
		ret[1] = floor(interface_playerInputNumber(INPUT_COORD_SCREEN_COORD_Y + 20, 
			INPUT_COORD_SCREEN_COORD_X + 100));
		if (ret[1] == -1) return ERROR;
	} while (ret[1] < 0 || ret[1] > MAP_WIDTH);

	return OK;
}

void move_toBlock (Map *map){

	int coord[2];

	if (input_coordinates(coord) == ERROR){
		generate_map(map);
		return;
	}

	map_setCurrentCoordX (map, coord[1]);
	map_setCurrentCoordY (map, coord[0]);

	if (coord[0] > MAX_MAP_HEIGHT / 2){
		if (coord[1] > MAX_MAP_WIDTH / 2){
			if (coord[0] < MAP_HEIGHT - MAX_MAP_HEIGHT / 2){
				if (coord[1] < MAP_WIDTH - MAX_MAP_WIDTH / 2){

					layout = FULL_SCREEN;
					map_setOffsetCoordX (map, coord[1] - MAX_MAP_WIDTH / 2);
					map_setOffsetCoordY (map, coord[0] - MAX_MAP_HEIGHT / 2);
					layout_coord_y = map_getCurrentCoordY(map) - (coord[0] - MAX_MAP_HEIGHT / 2);
					layout_coord_x = map_getCurrentCoordX(map) - (coord[1] - MAX_MAP_WIDTH / 2);
						
				}
				else{
					layout = L_RIGHT;
					map_setOffsetCoordX (map, MAX_MAP_WIDTH - 2);
					map_setOffsetCoordY (map, coord[0] - MAX_MAP_HEIGHT / 2);
					layout_coord_y = map_getCurrentCoordY(map) - (coord[0] - MAX_MAP_HEIGHT / 2);
					layout_coord_x = map_getCurrentCoordX(map) - (MAX_MAP_WIDTH - 2);
				}
			}
			else{
				if (coord[1] < MAP_WIDTH - MAX_MAP_WIDTH / 2){

					layout = BOTTOM;
					map_setOffsetCoordX (map, coord[1] - MAX_MAP_WIDTH / 2);
					map_setOffsetCoordY (map, MAP_HEIGHT - MAX_MAP_HEIGHT);
					layout_coord_y = map_getCurrentCoordY(map) - (MAP_HEIGHT - MAX_MAP_HEIGHT);
					layout_coord_x = map_getCurrentCoordX(map) - (coord[1] - MAX_MAP_WIDTH / 2);
				}
				else{
					layout = BOTTOM_RIGHT;
					map_setOffsetCoordX (map, MAP_WIDTH - MAX_MAP_WIDTH - 2);
					map_setOffsetCoordY (map, MAP_HEIGHT - MAX_MAP_HEIGHT);
					layout_coord_y = map_getCurrentCoordY(map) - (MAP_HEIGHT - MAX_MAP_HEIGHT);
					layout_coord_x = map_getCurrentCoordX(map) - (MAP_WIDTH - MAX_MAP_WIDTH - 2);
				}
			}
		}
		else{
			if (coord[0] < MAP_HEIGHT - MAX_MAP_HEIGHT / 2){

				layout = L_LEFT;
				map_setOffsetCoordX (map, 0);
				map_setOffsetCoordY (map, coord[0] - MAX_MAP_HEIGHT / 2);
				layout_coord_y = map_getCurrentCoordY(map) - (coord[0] - MAX_MAP_HEIGHT / 2);
				layout_coord_x = map_getCurrentCoordX(map);
			}
			else{
				layout = BOTTOM_LEFT;
				map_setOffsetCoordX (map, 0);
				map_setOffsetCoordY (map, MAP_HEIGHT - MAX_MAP_HEIGHT);
				layout_coord_y = map_getCurrentCoordY(map) - (MAP_HEIGHT - MAX_MAP_HEIGHT);
				layout_coord_x = map_getCurrentCoordX(map);
			}
		}
	}
	else{
		if(coord[1] > MAX_MAP_WIDTH / 2){
			if (coord[1] < MAP_WIDTH - MAX_MAP_WIDTH / 2){

				layout = L_UP;
				map_setOffsetCoordX (map, coord[1] - MAX_MAP_WIDTH / 2);
				map_setOffsetCoordY (map, 0);
				layout_coord_y = map_getCurrentCoordY(map);
				layout_coord_x = map_getCurrentCoordX(map) - (coord[1] - MAX_MAP_WIDTH / 2);
			}
			else{
				layout = UP_RIGHT;
				map_setOffsetCoordX (map, MAP_WIDTH - MAX_MAP_WIDTH - 1);
				map_setOffsetCoordY (map,0);
				layout_coord_y = map_getCurrentCoordY(map);
				layout_coord_x = map_getCurrentCoordX(map) - (MAP_WIDTH - MAX_MAP_WIDTH - 1);
			}
		}
		else{

			layout = UP_LEFT;
			map_setOffsetCoordX (map, 0);
			map_setOffsetCoordY (map, 0);
			layout_coord_y = map_getCurrentCoordY(map);
			layout_coord_x = map_getCurrentCoordX(map);
		}
	}

	generate_map(map);
}

Status show_bottomInfo(char *text, unsigned char r, unsigned char g, unsigned char b){

	pthread_t bottom_info_thread;
	char *auxtext;
	arg_struct_t *auxargs;

	auxargs = (arg_struct_t *) malloc(sizeof(arg_struct_t));

	auxtext = (char *) malloc ((strlen(text) + 1) * 
		sizeof(char));
	if (auxtext == NULL) return ERROR;

	strcpy(auxtext, text);

	auxargs->text = auxtext;
	auxargs->r = r;
	auxargs->g = g;
	auxargs->b = b;
	auxargs->time_s = 10;

	pthread_create(&bottom_info_thread, NULL, (void *)print_bottomInfo, 
		auxargs);

	return OK;
}

Status print_money (int flag){

	char buf[50];

	while (flag){

		sprintf(buf, "%ld$", player_get_money(game_getme(game)));

		if (player_get_money(game_getme(game)) >= 0){
			if (add_text(q, buf, SIDE_INFO_COORD_Y, MONEY_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
		}
		else
			if (add_text(q, buf, SIDE_INFO_COORD_Y, MONEY_COORD_X, RED) == ERROR) return ERROR;

		
		sprintf(buf, "LABS: %d", player_get_nlabs(game_getme(game)));
		if (add_text(q, buf, SIDE_INFO_NLABS_COORD_Y, SIDE_INFO_COORD_X, LETTER_COLOR) == ERROR) return ERROR;

		sprintf(buf, "REST: %d", player_get_nrest(game_getme(game)));
		if (add_text(q, buf, SIDE_INFO_NREST_COORD_Y, SIDE_INFO_COORD_X, LETTER_COLOR) == ERROR) return ERROR;

		sprintf(buf, "HOUSES: %d", player_get_ntraphouse(game_getme(game)));
		if (add_text(q, buf, SIDE_INFO_NHOUSES_COORD_Y, SIDE_INFO_COORD_X, LETTER_COLOR) == ERROR) return ERROR;

		sprintf(buf, "VIGILANCE: %ld", player_get_vigilance(game_getme(game)));
		if (add_text(q, buf, SIDE_INFO_VIGILANCE_COORD_Y, SIDE_INFO_COORD_X, LETTER_COLOR));

		if (add_text(q, "DESTROYED", SIDE_INFO_VIGILANCE_COORD_Y + 10, SIDE_INFO_COORD_X, LETTER_COLOR)) return ERROR;
		sprintf(buf, "LABS: %d/%d", player_get_nlabsdestroyed(game_getme(game)), MAX_DESTROYED_LABS);
		if (add_text(q, buf,  SIDE_INFO_VIGILANCE_COORD_Y + 20,
			SIDE_INFO_COORD_X, LETTER_COLOR)) return ERROR;


		sleep(1);

		add_background(q, SIDE_INFO_COORD_Y - 1, MAX_MAP_COORD_X + 1, MAX_MAP_COORD_Y - SIDE_INFO_COORD_Y, 
			SCREEN_WIDTH - MAX_MAP_COORD_X - 2, BGCOLOR);
	}

	return OK;
}