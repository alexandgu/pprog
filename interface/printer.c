#include "printer.h"
#include <stdlib.h>
#include <stdio.h>


Status function_executor(Queue *q){

	Function *fp;
	int pthread_type, pthread_state;
	printTextFunction textfp;
	printLetterFunction letterfp;
	printBoxedTextFunction btextfp;
	printImageFunction imgfp;
	printBackgroundFunction bgfp;
	printStdFunction stdfp;
	printMinimapFunction mmapfp;
	eraseCursorFunction erasecfp;
	printSquareFunction squarefp;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &pthread_type);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &pthread_state);

	while (1){

		if (queue_isEmpty(q) == FALSE){

			fp = (Pfunction) queue_extract (q);
			if (fp == NULL){

				fprintf(stderr, "Error extracting function from quque.\n");
				return ERROR;
			}

			switch (fp->type){

				case TEXT:

					textfp = (printTextFunction) fp->function;
					textfp (fp->text, fp->y, fp->x, fp->r, fp->g, fp->b);

					free(fp->text);

					break;

				case BOXED_TEXT:

					btextfp = (printBoxedTextFunction) fp->function;
					btextfp (fp->text, fp->y, fp->x, fp->text_t);

					free(fp->text);

					break;

				case SQUARE:

					squarefp = (printSquareFunction) fp->function;
					squarefp (fp->side, fp->y, fp->x, fp->r, fp->b, fp->g);

					break;

				case IMAGE:

					imgfp = (printImageFunction) fp->function;
					if (imgfp ((Image *) fp->source, fp->y, fp->x) == ERROR){

						fprintf (stderr, "Error printing image.\n");
						return ERROR;
					}

					break;

				case BACKGROUND:

					bgfp = (printBackgroundFunction) fp->function;
					bgfp (fp->y, fp->x, fp->height, fp->width, fp->r, fp->g, fp->b);

					break;

				case STD:

					stdfp = (printStdFunction) fp->function;
					stdfp ();
					
					break;

				case MAP:

					mmapfp = (printMinimapFunction) fp->function;
					mmapfp ((Map *) fp->source);

					break;

				case CURSOR_ERASE:

					erasecfp = (eraseCursorFunction) fp->function;
					erasecfp (fp->option);

					break;

				case LETTER:

					letterfp = (printLetterFunction) fp->function;

					letterfp (fp->letter, fp->py, fp->px);

					break;

				default: fprintf(stderr, "Function type unspecified\n"); break;

			}
			fflush(stdout);
			free (fp);
		}
	}

	return OK;
}


void destroy_function (void * dum){


}

void *copy_function (const void *function){

	void *paux;

	paux = (Pfunction) malloc (sizeof(Function));
	if (paux == NULL) return NULL;

	memcpy(paux, function, sizeof(Function));

	return paux;
}

int print_function (FILE *fp, const void *dum){

	return 0;
}

Status add_image(Queue *q, Image *img, int y, int x){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = print_image;
	fp->source = img;
	fp->y =  y;
	fp->x = x;
	fp->type = IMAGE;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_text(Queue *q, char *text, int y, int x, unsigned char r, unsigned char g, unsigned char b){

	Function *fp;
	char *auxtext;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	auxtext = (char *) malloc ((strlen(text) + 1) * sizeof(char));
	if (auxtext == NULL) return ERROR;

	strcpy(auxtext, text);

	fp->function = print_text;
	fp->text = auxtext;
	fp->y = y;
	fp->x = x;
	fp->type = TEXT;
	fp->r = r;
	fp->g = g;
	fp->b = b;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_boxedText (Queue *q, char *text, int y, int x, Text_t type){

	Function *fp;
	char *auxtext;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	auxtext = (char *) malloc ((strlen(text) + 1) * sizeof(char));
	if (auxtext == NULL) return ERROR;

	strcpy(auxtext, text);

	fp->function = print_boxedText;
	fp->text = auxtext;
	fp->y = y;
	fp->x = x;
	fp->type = BOXED_TEXT;
	fp->text_t = type;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_background (Queue *q, int y, int x, int height, int width, 
	unsigned char r, unsigned char g, unsigned char b){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = print_background;
	fp->y = y;
	fp->x = x;
	fp->height = height;
	fp->width = width;
	fp->r = r;
	fp->g = g;
	fp->b = b;
	fp->type = BACKGROUND;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_stdFunction (Queue *q, void *function){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = function;
	fp->type = STD;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_minimap(Queue *q, Map *map){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = print_minimap;
	fp->source = (void *) map;
	fp->type = MAP;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_blockMenu (Queue *q, Map *map){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = print_blockMenu;
	fp->source = (void *) map;
	fp->type = MAP;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_blockMenu_eraseCursor (Queue *q, char option, void *function){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = function;
	fp->type = CURSOR_ERASE;
	fp->option = option;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_square (Queue *q, int side, int y, int x, 
	unsigned char r, unsigned char g, unsigned char b){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = print_square;
	fp->type = SQUARE;
	fp->side = side;
	fp->x = x;
	fp->y = y;
	fp->r = r;
	fp->g = g;
	fp->b = b;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}

Status add_letter (Queue *q, char letter, int *y, int *x){

	Function *fp;

	fp = (Pfunction) malloc (sizeof(Function));
	if (fp == NULL) return ERROR;

	fp->function = print_letter;
	fp->py = y;
	fp->px = x;
	fp->letter = letter;
	fp->type = LETTER;

	if (queue_insert(q, fp) == ERROR){

		fprintf(stderr, "Error inserting function in queue.\n");
		return ERROR;
	}

	free(fp);

	return OK;
}