#ifndef INTERACTION_H
#define INTERACTION_H

#include "player.h"
#include "../construction/block.h"
#include "../construction/building.h"
#include "../network/client_interface.h"
#include "../network/server_interface.h"
#include "../utils/includes.h"
#include "refresh.h"
#include <limits.h>
#include "../game/game.h"

#define MAX_TYPE_BUILDING 3

int interaction_increaselevel_building(Game *, Player *p, Building *);
int interaction_increaselevel_block(Game *, Player *p, Block *);
Player * interaction_newplayer(Game *, char * name);
int interaction_newbuilding(Game *, Player *p, Block *b, Building_type type_building);
int interaction_movedrug(Game *, Player *p, Building *origin, Building * destination);
int interaction_receivefromnetwork(Game * game_pointer, char * action);
int interaction_assaultplayer(Game * game, Player *p, Building * oponent_building);
/*If you pay you can turn your oponnent's lab to a 0 level, but it could happen to you instead of him...*/
#endif
