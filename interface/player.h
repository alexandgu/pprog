
#ifndef PLAYER_H
#define PLAYER_H

#include "../utils/includes.h"
#include "../construction/building.h"
#include "../utils/list.h"
#include "../utils/types.h"
#define ERR -1 

typedef struct _player Player;
struct _player {
  short id;
  char *name;
  long money;
  long vigilance;
  int n_labs;
  int n_labs_destroyed;
  int n_restaurants;
  int n_traphouse;
  Building ** p_traphouse;
  Building ** p_restaurants;
  List * p_labs;
};

/*
    PLAYER
            */
Player *player_create (char *name, short id);
/* Initialize player structure with a money amount of 100$
        Input: player name
        Output: pointer to player structure
*/
void player_destroy (Player *p);
/* Free player structure
        Input: pointer to free
*/
char *player_get_name (Player *p);
/* Return name of the player
        Input: not nullable pointer to player
        Output: name 
*/
short player_get_id(Player *p);
/* Return the id of the player
        Input: not nullable pointer to player
        Output: id
 */
void player_set_id(Player *p, short id);
/* Set the id of the player
        Input: player pointer/id
        Output: OK
*/

/*
    MONEY
            */
int player_set_money (Player *p, long amount);
/* Update the money of the player
        Input: player pointer/amount of money calculated with a function
        Output: OK
*/
long player_get_money (Player *p);
/* Return money of the player
        Input: not nullable pointer to player
        Output: amount of money
 */

/*
    LABS
            */
int player_increase_nlabs(Player *p);
/* Increase the number of labs of the player
        Input: pointer to player
        Output: OK
*/
int player_decrease_nlabs(Player *p);
/* Decrease the number of labs of the player (beacuase the police have caught one of them)
        Input: pointer to player
        Output: OK
*/
int player_get_nlabs(Player *p);
/* Return number of labs of the player
        Input: not nullable pointer to player
        Output: n_labs 
*/
int player_increase_nlabsdestroyed(Player *p);
/* Increase the number of labs lost by the player
        Input: pointer to player
        Output: OK
*/
int player_get_nlabsdestroyed(Player *p);
/* Return number of labs destroyed of the player
        Input: not nullable pointer to player
        Output: n_labsdestroyed
*/
Building *player_get_lab(Player *p, int i);
/* Get the laboratory number i from the list of labs of the player
        Input: pointer to player/number of the lab
        Output: pointer to the lab
*/
int player_add_lab(Player *p, Building *bu);
/* Add a laboratory to the array of labs of the player. Increase the number of labs
        Input: pointer to player/pointer to lab
        Output: OK
*/

/*
    REST
            */
int player_increase_nrest(Player *p);
/* Increase the number of restaurants of the player
        Input: pointer to player
        Output: OK
*/
int player_decrease_nrest(Player *p);
/* Decrease the number of restaurants of the player 
        Input: pointer to player
        Output: OK
*/
int player_get_nrest(Player *p);
/* Return number of restaurants of the player
        Input: not nullable pointer to player
        Output: n_rest 
*/
Building *player_get_rest(Player *p, int i);
/* Get the restaurant number i from the list of restaurants of the player
        Input: pointer to player/number of the rest
        Output: pointer to the rest
*/
int player_add_rest(Player *p, Building *bu);
/* Add a restaurant to the array of restaurants of the player. Increase the number of restaurants
        Input: pointer to player/pointer to rest
        Output: OK
*/

/*
    TRAPHOUSE
            */
int player_increase_ntraphouse(Player *p);
/* Increase the number of trap houses of the player
        Input: pointer to player
        Output: OK
*/
int player_decrease_ntraphouse(Player *p);
/* Decrease the number of trap houses of the player 
        Input: pointer to player
        Output: OK
*/
int player_get_ntraphouse(Player *p);
/* Return number of trap houses of the player
        Input: not nullable pointer to player
        Output: n_traphouses 
*/
Building *player_get_traphouse(Player *p, int i);
/* Get the trap house number i from the list of trap houses of the player
        Input: pointer to player/number of the trap house
        Output: pointer to the trap house
*/
int player_add_traphouse(Player *p, Building *bu);
/* Add a trap house to the array of trap houses of the player. Increase the number of trap houses
        Input: pointer to player/pointer to trap house
        Output: OK
*/

/*
    VIGILANCE
                */
long player_get_vigilance(Player *p);
/* Get the vigilance of the player
        Input: pointer to player
        Output: p->vigilance
*/
int player_set_vigilance(Player *p, long vigilance);
/* Update the vigilance of the player
        Input: player pointer/vigilance calculated with a function
        Output: OK
*/
int player_add_vigilance(Player *p, long vigilance);
/* Add "vigilance" to p->vigilance
        Input: pointer to player/vigilance to add
        Output: OK
*/


#endif


