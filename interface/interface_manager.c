#include "interface_manager.h"

#include <termios.h>
#include <stdlib.h>

Status map_manager(Map *map){

	Building *trap_house_aux;
	pthread_t show_money, game_clock, *music_thread;
	int money_flag = 1;
	int return_flag;

	if (map == NULL) return ERROR;

	layout = UP_LEFT;
	layout_coord_x = 0;
	layout_coord_y = 0;
	drug_transfer_flag = 0;
	game_over_flag = OK;
	
	pthread_create(&game_clock, NULL, (void *) refresh_clock, NULL);
	music_thread = play_sound(JAZZ_TRACK);

	if (generate_screen(GAME_SCREEN) == ERROR) return ERROR;
	if (generate_map(map) == ERROR) return ERROR;
	
	pthread_create(&show_money, NULL, (void *) print_money, &money_flag);

	while(1){

		if (game_over_flag == GAMEOVER){

			money_flag = 0;
			print_gameoverScreen();
			return OK;
		}
		if (bribe_flag == 1){
			show_bribeMenu();
			generate_map(map);
		}

		switch(getchar()){

			case '\e':

				getchar();

				switch(getchar()){

					case 'A': 

						move_selector(map, UP);
						break;
					case 'B': 


						move_selector(map, DOWN);
						break;
					case 'C': 

						move_selector(map, RIGHT);
						break;
					case 'D': 

						move_selector(map, LEFT);
						break;
					default: break;
				}
				break;

			case EXIT_KEY:
				
				pthread_cancel(print_thread);
				pthread_cancel(game_clock);
				money_flag = 0;
			 	stop_sound();
				free_images();
				free(music_thread);
				return OK;
				break;

			case MINIMAP_KEY:
				if(print_fullMap(map) == ERROR) return ERROR;
				break;

			case ENTER_KEY:

				switch (drug_transfer_flag){

					case 0:

						if (blockMenu_manager(map) == ERROR) return ERROR;
						break;

					case 1:

						trap_house_aux = block_getBuilding(map_getCurrentBlock(map), -1, TRAPHOUSE);
						if (trap_house_aux == NULL){

							show_bottomInfo(CANT_DELIVER_DRUG_TEXT, RED);
							break;
						}
						interaction_movedrug(game, game_getme(game), lab_aux, 
							block_getBuilding(map_getCurrentBlock(map), -1, LAB));

						show_bottomInfo(DRUG_DELIVER_SUCCESS_TEXT, GREEN);
						drug_transfer_flag = 0;

						break;

					default: break;
				}
				break;

			case CANCEL_KEY:

				drug_transfer_flag = 0;

				break;

			case JUMP_KEY:

				move_toBlock (map);

				break;

			case HELP_KEY:

				print_helpScreen();
				generate_map(map);

				break;

			case SHOW_PLAYERS_KEY:
				if(game->mode == SINGLE) break;
				show_players();
				break;

			case LEVEL_UP:
				return_flag=interaction_increaselevel_block(game, game_getme(game), map_getCurrentBlock(map));
				if(return_flag == NOMONEY)
					show_bottomInfo(NOT_ENOUGH_MONEY_TEXT, RED);
				if(return_flag == DONTDOTHAT)
					show_bottomInfo(NOT_UPGRADE_TEXT, RED);
				print_block(y_mapCoord(layout_coord_y, layout_coord_x), x_mapCoord(layout_coord_y, layout_coord_x), 
						map_getBlock(map, map_getCurrentCoordY(map), map_getCurrentCoordX(map)));
				select_block(map);

				break;		

			default: break;
		}

	}
	return OK;
}

Status move_selector(Map *map, Move move){


	int cx, cy;

	if (map == NULL) return ERROR;

	cx = map_getCurrentCoordX(map);
	cy = map_getCurrentCoordY(map);

	switch(move){

		case UP:

			if (cy > 0){
				if (layout_coord_y  > 0){

					print_block(y_mapCoord(layout_coord_y, layout_coord_x), x_mapCoord(layout_coord_y, layout_coord_x), 
						map_getBlock(map, (unsigned short) cy, (unsigned short) cx));
					map_decreaseCurrentCoordY(map);
					layout_coord_y--;
					select_block (map);
				}
				else if(layout_coord_y == 0){

					map_decreaseCurrentCoordY(map);
					map_decreaseOffsetCoordY(map);

					if (map_getCurrentCoordY(map) == 0){
						if (layout == L_LEFT){
							layout = UP_LEFT;
							empty_map();
						}
						else if (layout == L_RIGHT){
							layout = UP_RIGHT;
							empty_map();
						}
						else{
							layout = L_UP;
							empty_map();
						}
					}
					if (layout == BOTTOM) {
						layout = FULL_SCREEN;
						empty_map();
					}
					if (layout == BOTTOM_RIGHT){
						layout = L_RIGHT;
						empty_map();
					}
					if (layout == BOTTOM_LEFT){
						layout = L_LEFT;
						empty_map();
					}

					generate_map(map);
					select_block(map);
				}
			}
			else return OK;
			break;

		case DOWN:

			if (cy < MAP_HEIGHT - 1){
				if (layout_coord_y < MAX_MAP_HEIGHT - 1){

					print_block(y_mapCoord(layout_coord_y, layout_coord_x), 
						x_mapCoord(layout_coord_y, layout_coord_x), 
						map_getBlock(map, (unsigned short) cy, (unsigned short) cx));
					map_increaseCurrentCoordY(map);
					layout_coord_y++;
					select_block (map);
				}
				else if(layout_coord_y == MAX_MAP_HEIGHT - 1){

					map_increaseCurrentCoordY(map);
					map_increaseOffsetCoordY(map);

					if (map_getCurrentCoordY(map) == MAP_HEIGHT - 1){
						if (layout == L_LEFT){
							layout = BOTTOM_LEFT;
							empty_map();
						}
						else if (layout == L_RIGHT){
							layout = BOTTOM_RIGHT;
							empty_map();
						}
						else{
							layout = BOTTOM;
							empty_map();
						}
					}
					if (layout == L_UP){
						layout = FULL_SCREEN;
						empty_map();
					}
					if (layout == UP_RIGHT){
						layout = L_RIGHT;
						empty_map();
					}
					if (layout == UP_LEFT){
						layout = L_LEFT;
						empty_map();
					}

					generate_map(map);	
					select_block(map);
				}
			}
			else return OK;
			break;

		case RIGHT:

			if (cx < MAP_WIDTH - 1){

				if(layout_coord_x == MAX_MAP_WIDTH - 1){

					map_increaseCurrentCoordX(map);
					map_increaseOffsetCoordX(map);

					if (map_getCurrentCoordX(map) == MAX_MAP_WIDTH - 1){
						if (layout == L_UP) layout = UP_RIGHT;
						else if (layout == BOTTOM) layout = BOTTOM_RIGHT;
						else layout = L_RIGHT;
					}
					if (layout == L_LEFT){
						layout = FULL_SCREEN;
						empty_map();
					}
					if (layout == BOTTOM_LEFT){
						layout = BOTTOM;
						empty_map();
					}
					if (layout == UP_LEFT){
						layout = L_UP;
						empty_map();
					}

					generate_map(map);
					select_block(map);
				}

				else if (cx >=  MAP_WIDTH - 4){

					print_block(y_mapCoord(layout_coord_y, layout_coord_x), 
						x_mapCoord(layout_coord_y, layout_coord_x), 
						map_getBlock(map, (unsigned short) cy, (unsigned short) cx));
					layout_coord_x++;
					map_increaseCurrentCoordX(map);
					select_block (map);
				}
				else if (layout_coord_x < MAX_MAP_WIDTH - 1){

					print_block(y_mapCoord(layout_coord_y, layout_coord_x), 
						x_mapCoord(layout_coord_y, layout_coord_x), 
						map_getBlock(map, (unsigned short) cy, (unsigned short) cx));
					map_increaseCurrentCoordX(map);
					layout_coord_x++;
					select_block (map);
				}
			}
			else return OK;
			break;

		case LEFT:

			if (cx > 0){
				if (layout_coord_x > 0){

					print_block(y_mapCoord(layout_coord_y, layout_coord_x), 
						x_mapCoord(layout_coord_y, layout_coord_x), 
						map_getBlock(map, (unsigned short) cy, (unsigned short) cx));
					map_decreaseCurrentCoordX(map);
					layout_coord_x--;
					select_block (map);
				}
				else if(layout_coord_x == 0){

					map_decreaseCurrentCoordX(map);
					map_decreaseOffsetCoordX(map);

					if (map_getCurrentCoordX(map) == 0){
						if (layout == L_UP){
							layout = UP_LEFT;
							empty_map();
						}
						else if (layout == BOTTOM){
							layout = BOTTOM_LEFT;
							empty_map();
						}
						else{
							layout = L_LEFT;
							empty_map();
						}
					}
					if (layout == L_RIGHT){
						layout = FULL_SCREEN;
						empty_map();
					}
					if (layout == BOTTOM_RIGHT){
						layout = BOTTOM;
						empty_map();
					}
					if (layout == UP_RIGHT){
						layout = L_UP;
						empty_map();
					}

					generate_map(map);
					select_block(map);
				}
			}
			else return OK;
			break;

		default: break;
	}
	return OK;
}

Status blockMenu_manager (Map *map){

	char nbuild, buildingpos = 0, i, offset = 0;
	char option, exit_flag = 1, reprint_flag, maxoption;
	Block *block = map_getCurrentBlock(map);
	Building *bui;
	int id_flag,return_flag;

	while (exit_flag){

		reprint_flag = 1;
		option = 1;

		print_blockMenu(map);
		blockMenu_selectSquare (buildingpos - offset);
		
		nbuild = block_getnumbuildings(map_getCurrentBlock(map));

		for (i = 0; i < BLOCK_MENU_SQUARE_NUM && i < nbuild; i++) 
			blockMenu_printBuilding(building_gettype(block_getBuilding(block, i, 'a')), i, 
				building_getlevel(block_getBuilding(block, i, 'a')));

		blockMenu_showOptions(block_getBuilding(block, buildingpos, 'a'), &maxoption);

		while(reprint_flag){

			switch(getchar()){

				case '\e':

					getchar();

					switch(getchar()){

						case 'A': 

							if (option == 1) break;
							blockMenu_moveCursor(UP, &option);

							break;

						case 'B': 

							if (option == maxoption) break;
							blockMenu_moveCursor(DOWN, &option);

							break;

						case 'C':

							if (buildingpos == nbuild) break;
							if (buildingpos < BLOCK_MENU_SQUARE_NUM - 1){

								add_blockMenu_eraseCursor(q, buildingpos, blockMenu_deselectSquare);
								blockMenu_selectSquare (++buildingpos);
								blockMenu_eraseOptions();
								blockMenu_showOptions(block_getBuilding(block, buildingpos, 'a'), &maxoption);
								option = 1;
							}
							break;

						case 'D':

							if (buildingpos == 0) break;
							add_blockMenu_eraseCursor(q, buildingpos - offset, blockMenu_deselectSquare);
							blockMenu_selectSquare (--buildingpos - offset);
							option = 1;
							blockMenu_eraseOptions();
							blockMenu_showOptions(block_getBuilding(block, buildingpos, 'a'), &maxoption);
							break;

						default: break;
					}
					break;

				case BACK_KEY:

					reprint_flag = 0;
					exit_flag = 0;
					break;

				case ENTER_KEY:
					bui = block_getBuilding(block, buildingpos, 0);
					if(bui == NULL){
						switch (option){

								case 1:
									
									if (interaction_newbuilding(game, game_getme(game), 
										block, LAB) == DONTDOTHAT){

										show_bottomInfo(CANT_BUILD_TEXT1 LAB_TEXT CANT_BUILD_TEXT2, RED);
									}
									reprint_flag = 0;
									
								break;

								case 2:
									
									if (interaction_newbuilding(game, game_getme(game), 
										map_getCurrentBlock(map), REST) == DONTDOTHAT)
										show_bottomInfo(CANT_BUILD_TEXT1 REST_TEXT CANT_BUILD_TEXT2, RED);
									reprint_flag = 0;
									
								break;

								case 3:
									
									if (interaction_newbuilding(game, game_getme(game), 
										map_getCurrentBlock(map), TRAPHOUSE) == DONTDOTHAT)
										show_bottomInfo(CANT_BUILD_TEXT1 TRAP_HOUSE_TEXT CANT_BUILD_TEXT2, RED);
									reprint_flag = 0;
									
								break;
							} break;
						return OK;
					}
					
					id_flag = building_getowner(block_getBuilding(block, buildingpos, 'a')) != game_getid(game);
					switch(building_gettype(block_getBuilding(block, buildingpos, 'a'))){

						
						case LAB:

							switch (option){

								case 1:
									if(id_flag){
										break;
									}
									return_flag=interaction_increaselevel_building(game, game_getme(game), 
										block_getBuilding(block, buildingpos, 'a'));
									if (return_flag == NOMONEY)
										show_bottomInfo(NOT_ENOUGH_MONEY_TEXT, RED);
									if (return_flag == ERROR)
										show_bottomInfo(NOT_YOURS_TEXT, RED);
									if (return_flag == DONTDOTHAT)
										show_bottomInfo(NOT_UPGRADE_TEXT, RED);

									blockMenu_printBuilding(building_gettype(block_getBuilding(block, 
										buildingpos, 'a')), buildingpos - offset, 
										building_getlevel(block_getBuilding(block, buildingpos, 'a')));
									
								break;

								case 2:
									if(id_flag)
										break;
									drug_transfer_flag = 1;
									lab_aux = block_getBuilding(map_getCurrentBlock(map), buildingpos, 0);
									reprint_flag = 0, exit_flag = 0;

								break;

								default: break;
							} break;

						case REST:
						case TRAPHOUSE:

							switch(option){

								case 1:
									if(id_flag)
										break;

									return_flag=interaction_increaselevel_building(game, game_getme(game), 
										block_getBuilding(block, buildingpos, 'a'));
									if (return_flag == NOMONEY)
										show_bottomInfo(NOT_ENOUGH_MONEY_TEXT, RED);
									if (return_flag == ERROR)
										show_bottomInfo(NOT_YOURS_TEXT, RED);
									if (return_flag == DONTDOTHAT)
										show_bottomInfo(NOT_UPGRADE_TEXT, RED);
										
									blockMenu_printBuilding(building_gettype(block_getBuilding(block, 
										buildingpos, 'a')), buildingpos - offset, 
										building_getlevel(block_getBuilding(block, buildingpos, 'a')));

								break;

								default: break;
							} break;

						default: break;
					}
				break;

				default: break;
			}
		}
	}

	if(generate_map(map) == ERROR) return ERROR;
	if(select_block(map) == ERROR) return ERROR;

	return OK;
}

void blockMenu_moveCursor (Move move, char *option){

	switch(move){

		case UP:

			add_blockMenu_eraseCursor(q, *option, blockMenu_eraseCursor);

			add_text (q, BLOCK_MENU_CURSOR, BLOCK_MENU_CURSOR_COORD_Y + 10 * (--(*option) - 1), 
				BLOCK_MENU_CURSOR_COORD_X, LETTER_COLOR);

			break;

		case DOWN:

			add_blockMenu_eraseCursor(q, *option, blockMenu_eraseCursor);

			add_text (q, BLOCK_MENU_CURSOR, BLOCK_MENU_CURSOR_COORD_Y + 10 * (*option)++, 
				BLOCK_MENU_CURSOR_COORD_X, LETTER_COLOR);

			break;

		default: break;
	}
}

Status blockMenu_showOptions (Building *building, char *maxoption){
	int id_flag;
	add_text(q, BLOCK_MENU_CURSOR, BLOCK_MENU_CURSOR_COORD_Y, BLOCK_MENU_CURSOR_COORD_X, LETTER_COLOR);

	if(building == NULL){
		if (add_text(q, BUILD_LAB_TEXT, BLOCK_MENU_OPTIONS_COORD_Y, 
			BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
		if (add_text(q, BUILD_REST_TEXT, BLOCK_MENU_OPTIONS_COORD_Y + 10, 
			BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
		if (add_text(q, BUILD_TRAP_HOUSE_TEXT, BLOCK_MENU_OPTIONS_COORD_Y + 20, 
			BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
		*maxoption = 3;
		return OK;
	}
	id_flag = building_getowner(building) != player_get_id(game_getme(game));

		
	

	switch (building_gettype(building)){


		case LAB:

			if(id_flag){
				if (add_text(q, ASSAULT_PLAYER_TEXT, BLOCK_MENU_OPTIONS_COORD_Y, 
				BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
				*maxoption = 1;
				break;
			}else{
				if (add_text(q, UPGRADE_LAB_TEXT, BLOCK_MENU_OPTIONS_COORD_Y, 
				BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
				if (add_text(q, SEND_DRUG_TEXT, BLOCK_MENU_OPTIONS_COORD_Y + 10, 
				BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR)== ERROR) return ERROR;
				*maxoption = 2;
				break;
			}
			

		case REST:

			if(id_flag)
				break;
			if (add_text(q, UPGRADE_REST_TEXT, BLOCK_MENU_OPTIONS_COORD_Y, 
				BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
			
			*maxoption = 1;

			break;

		case TRAPHOUSE:

			if(id_flag)
				break;
			if (add_text(q, UPGRADE_TRAP_HOUSE_TEXT, BLOCK_MENU_OPTIONS_COORD_Y, 
				BLOCK_MENU_OPTIONS_COORD_X, LETTER_COLOR) == ERROR) return ERROR;
			
			*maxoption = 1;

		default: break;
	}

	return OK;
}

void blockMenu_eraseOptions(){

	add_background (q, BLOCK_MENU_FIRST_SQUARE_Y + BLOCK_MENU_SQUARE_SIDE + 1,
		BLOCK_MENU_COORD_X, 
		MAX_MAP_COORD_Y - (BLOCK_MENU_FIRST_SQUARE_Y + BLOCK_MENU_SQUARE_SIDE + 1 + 5),
		MAX_MAP_COORD_X - 2 * BLOCK_MENU_COORD_X,  
		FULL_MAP_BG_COLOR);
}

Status mainScreen_manager(){

	char pos = 1;
	pthread_t *music_thread;

	/*music_thread = play_sound(TRAP_TRACK);*/

	if (generate_screen (MAIN_SCREEN) == ERROR) return ERROR;

	while (1){

		switch(getchar()){

			case '\e':

				getchar();

				switch(getchar()){

					case 'C':

						mainScreen_moveSelector (RIGHT, &pos, GAME_SELECTION);
						break;

					case 'D':

						mainScreen_moveSelector (LEFT, &pos, GAME_SELECTION);
						break;

					default: break;
				}
				break;

			case BACK_KEY:

				break;

			case ENTER_KEY:

				switch (pos){

					case 1:

						mainScreen_gamemodeSelector();
						/*stop_sound();
						free(music_thread);*/
						return OK;

					break;

					default: break;
				}

				break;

			default: break;
		}
	}

	return OK;
}

Status mainScreen_gamemodeSelector (){

	char pos = 1;

	add_background (q, NEW_GAME_COORD, 13, 50, BGCOLOR);
	add_background (q, LOAD_GAME_COORD, 13, 60, BGCOLOR);
	add_boxedText (q, SINGLE_PLAYER_TEXT, NEW_GAME_COORD - 10, SELECTED);
	add_boxedText (q, MULTIPLAYER_TEXT, LOAD_GAME_COORD + 10, NOT_SELECTED);

	while (1){

		switch(getchar()){

			case '\e':

				getchar();

				switch(getchar()){

					case 'C':

						mainScreen_moveSelector (RIGHT, &pos, GAMEMODE_SELECTION);
						break;

					case 'D':

						mainScreen_moveSelector (LEFT, &pos, GAMEMODE_SELECTION);
						break;

					default: break;
				}
				break;

			case BACK_KEY:

				break;

			case ENTER_KEY:

				switch (pos){

					case 1:
						if(game == NULL){
							game = game_ini(SINGLE);
							player = interaction_newplayer(game, "Rigoberto");
							game_setme(game, player);
						}
						return OK;

					case 2:

						mainScreen_multiplayerSelection();

						return OK;

					default: break;

				}

				break;

			default: break;
		}
	}

	return OK;
}

void mainScreen_moveSelector (Move move, char *pos, Screen screen){

	switch (move){

		case RIGHT:

			if (*pos == MAIN_MENU_OPTIONS_NUM) break;

			switch (screen){

				case GAME_SELECTION:

					add_boxedText (q, NEW_GAME_TEXT, NEW_GAME_COORD, NOT_SELECTED);
					add_boxedText (q, LOAD_GAME_TEXT, LOAD_GAME_COORD, SELECTED);

					break;

				case GAMEMODE_SELECTION:

					add_boxedText (q, SINGLE_PLAYER_TEXT, NEW_GAME_COORD - 10, NOT_SELECTED);
					add_boxedText (q, MULTIPLAYER_TEXT, LOAD_GAME_COORD + 10, SELECTED);

					break;

				default: break;
			}
			(*pos)++;

			break;

		case LEFT:

			if (*pos == 1) break;

			switch (screen){

				case GAME_SELECTION:

					add_boxedText (q, NEW_GAME_TEXT, NEW_GAME_COORD, SELECTED);
					add_boxedText (q, LOAD_GAME_TEXT, LOAD_GAME_COORD, NOT_SELECTED);

					break;

				case GAMEMODE_SELECTION:

					add_boxedText (q, SINGLE_PLAYER_TEXT, NEW_GAME_COORD - 10, SELECTED);
					add_boxedText (q, MULTIPLAYER_TEXT, LOAD_GAME_COORD + 10, NOT_SELECTED);

					break;

				default: break;
			}
			(*pos)--;

			break;


		default: break;
	}	
}

void mainScreen_multiplayerSelection(){

	char option = 0, i;

	add_background(q, (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2, 
		(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2, MAIN_SCREEN_WP_HEIGHT, MAIN_SCREEN_WP_WIDTH, 
		MAIN_SCREEN_WP_BGCOLOR);

	add_text(q, HOSTING_QUESTION, MAIN_SCREEN_Q_COORD_Y, MAIN_SCREEN_Q_COORD_X, LETTER_COLOR);
	add_text(q, HOST_TEXT, MAIN_SCREEN_OP_COORD_Y, MAIN_SCREEN_OP1_COORD_X, LETTER_COLOR);
	add_text(q, CLIENT_TEXT, MAIN_SCREEN_OP_COORD_Y, MAIN_SCREEN_OP2_COORD_X, LETTER_COLOR);
	add_text(q, BLOCK_MENU_CURSOR, MAIN_SCREEN_OP_COORD_Y, MAIN_SCREEN_CURSOR_COORD_X, LETTER_COLOR);

	while(1){

		switch(getchar()){

			case '\e':

				getchar();

				switch (getchar()){

					case 'C':

						if (option == 1) break;

						CHANGE_COLOR(MAIN_SCREEN_WP_BGCOLOR);
						for (i = 0; i < 4; i++) {
							MOVE_CURSOR(MAIN_SCREEN_OP_COORD_Y + i, MAIN_SCREEN_CURSOR_COORD_X);
							printf("   ");
						}
						add_text(q, BLOCK_MENU_CURSOR, MAIN_SCREEN_OP_COORD_Y, MAIN_SCREEN_OP2_COORD_X - 6, LETTER_COLOR);
						option++;

					break;

					case 'D':

						if (option == 0) break;

						CHANGE_COLOR(MAIN_SCREEN_WP_BGCOLOR);
						for (i = 0; i < 4; i++) {
							MOVE_CURSOR(MAIN_SCREEN_OP_COORD_Y + i, MAIN_SCREEN_OP2_COORD_X - 6);
							printf("   ");
						}
						add_text(q, BLOCK_MENU_CURSOR, MAIN_SCREEN_OP_COORD_Y, MAIN_SCREEN_CURSOR_COORD_X, LETTER_COLOR);
						option--;

						break;

					default: break;
				}
				break;

			case ENTER_KEY:

				switch(option){

					case 0:

						mainScreen_waitingPlayersServer();
						return;

					case 1:

						mainScreen_waitingPlayersClient();
						return;

					default: break;
				}
			default: break;
		}
	}
}

void mainScreen_waitingPlayersServer(){
	Server_interface *si;
    pthread_t starter, broadcaster;
    int i;
    Player * player;
	pthread_t text_tread;
	Map * m;

	add_background(q, (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2, 
		(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2, MAIN_SCREEN_WP_HEIGHT, MAIN_SCREEN_WP_WIDTH, 
		MAIN_SCREEN_WP_BGCOLOR);

	pthread_create(&text_tread, NULL, (void *)mainScreen_WP_text, NULL);

	game = game_ini(SERVER);
	m = map_readfromfile("prueba.txt");
	game_setmap(game, m);
	si = server_ini(game);
    game_setnetwork(game, si);
    player = player_create("alex", 0);
	game_addforeign(game, player_create("lobo", 1));
	game_addforeign(game, player_create("clau", 2));
    game_setme(game, player);
    pthread_create(&starter, NULL, server_clientstarter, si);
	sleep(5);
    pthread_create(&broadcaster, NULL, server_broadcaster, si);
    sleep(5);
    si->gtype = MAPBROADCAST;
    sleep(10);
    si->gtype = PLAYING;
	pthread_cancel(text_tread);
	if (map_manager(m) == ERROR) exit(EXIT_FAILURE);
}
void *multiplayer_client(void *entry){
    char* nombre;
    Player * player;
    pthread_t receiver;
    Client_interface *ci;
    player = player_create("clau", 2);
    game_setme(game, player);
	game_addforeign(game, player_create("alex", 0));
	game_addforeign(game, player_create("lobo", 1));
    ci = client_interface_ini(game);
    game_setnetwork(game, ci);
    pthread_create(&receiver, NULL, client_readfromserver, ci);
    return NULL;
}

void mainScreen_waitingPlayersClient(){
	pthread_t this_thread;
	pthread_t text_tread;
	Client_interface*ci;
	Map * m;
	add_background(q, (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2, 
		(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2, MAIN_SCREEN_WP_HEIGHT, MAIN_SCREEN_WP_WIDTH, 
		MAIN_SCREEN_WP_BGCOLOR);

	pthread_create(&text_tread, NULL, (void *)mainScreen_WP_text, NULL);

	game = game_ini(CLIENT);
    pthread_create(&this_thread, NULL, multiplayer_client, NULL);
    sleep(15);
    ci = game->network;
    while(ci->gtype != PLAYING)
        sleep(1);
    m = game_getmap(game);

	pthread_cancel(text_tread);
	map_manager(m);

}

void mainScreen_WP_text(){

	int pthread_type, pthread_state;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &pthread_type);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &pthread_state);

	while (1){

		add_text(q, WAITING_FOR_PLAYERS_TEXT ".", (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2 + 20, 
				(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2 + 30, LETTER_COLOR);
		sleep(1);
		add_background(q, (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2, 
		(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2, MAIN_SCREEN_WP_HEIGHT, MAIN_SCREEN_WP_WIDTH, 
		MAIN_SCREEN_WP_BGCOLOR);

		add_text(q, WAITING_FOR_PLAYERS_TEXT "..", (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2 + 20, 
			(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2 + 30, LETTER_COLOR);
		sleep(1);
		add_background(q, (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2, 
		(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2, MAIN_SCREEN_WP_HEIGHT, MAIN_SCREEN_WP_WIDTH, 
		MAIN_SCREEN_WP_BGCOLOR);

		add_text(q, WAITING_FOR_PLAYERS_TEXT "...", (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2 + 20, 
			(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2 + 30, LETTER_COLOR);
		sleep(1);
		add_background(q, (SCREEN_HEIGHT - MAIN_SCREEN_WP_HEIGHT) / 2, 
		(SCREEN_WIDTH - MAIN_SCREEN_WP_WIDTH) / 2, MAIN_SCREEN_WP_HEIGHT, MAIN_SCREEN_WP_WIDTH, 
		MAIN_SCREEN_WP_BGCOLOR);
	}
}

void print_gameoverScreen(){

	add_background(q, (SCREEN_HEIGHT - GAME_OVER_SCREEN_HEIGHT) / 2, (SCREEN_WIDTH - GAME_OVER_SCREEN_WIDTH) / 2, 
		GAME_OVER_SCREEN_HEIGHT, GAME_OVER_SCREEN_WIDTH, MAIN_SCREEN_WP_BGCOLOR);

	add_text(q, GAME_OVER_TEXT, (SCREEN_HEIGHT - GAME_OVER_SCREEN_HEIGHT) / 2 + 22, 
		(SCREEN_WIDTH - GAME_OVER_SCREEN_WIDTH) / 2 + 30, LETTER_COLOR);

	while(getchar() != ENTER_KEY);

	add_background(q, (SCREEN_HEIGHT - GAME_OVER_SCREEN_HEIGHT) / 2, (SCREEN_WIDTH - GAME_OVER_SCREEN_WIDTH) / 2, 
		GAME_OVER_SCREEN_HEIGHT, GAME_OVER_SCREEN_WIDTH, MAIN_SCREEN_WP_BGCOLOR);

	add_text(q, GREETINGS_TEXT, (SCREEN_HEIGHT - GAME_OVER_SCREEN_HEIGHT) / 2 + 22, 
		(SCREEN_WIDTH - GAME_OVER_SCREEN_WIDTH) / 2 + 30, LETTER_COLOR);

	while(getchar() != ENTER_KEY);
}

void print_helpScreen(){

	char buf;

	add_background(q, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2, (MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2, 
		HELP_SCREEN_HEIGHT, HELP_SCREEN_WIDTH, FULL_MAP_BG_COLOR);

	add_text(q, HELP_KEY_TEXT, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2 + 20, 
		(MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2 + 15, LETTER_COLOR);

	add_text(q, BLOCK_MENU_KEY_TEXT, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2 + 30, 
		(MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2 + 15, LETTER_COLOR);

	add_text(q, LEVEL_UP_KEY_TEXT, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2 + 40, 
		(MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2 + 15, LETTER_COLOR);

	add_text(q, JUMP_TO_KEY_TEXT, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2 + 50, 
		(MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2 + 15, LETTER_COLOR);

	add_text(q, MINIMAP_KEY_TEXT, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2 + 60, 
		(MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2 + 15, LETTER_COLOR);

	add_text(q, GO_BACK_KEY_TEXT, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2 + 70, 
		(MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2 + 15, LETTER_COLOR);

	add_text(q, CANCEL_DRUG_KEY_TEXT, (MAX_MAP_COORD_Y - HELP_SCREEN_HEIGHT) / 2 + 80, 
		(MAX_MAP_COORD_X - HELP_SCREEN_WIDTH) / 2 + 15, LETTER_COLOR);

	while ((buf = getchar()) != 127 && buf != 'h');

}

Status show_bribeMenu(){

	char option = 0, i;
	long bribe_money;
	int return_flag;

	add_background(q, (MAX_MAP_COORD_Y - BRIBE_MENU_HEIGHT) / 2, 
		(MAX_MAP_COORD_X - BRIBE_MENU_WIDTH) / 2, BRIBE_MENU_HEIGHT, BRIBE_MENU_WIDTH, 
		FULL_MAP_BG_COLOR);

	add_text(q, BRIBE_QUESTION, BRIBE_MENU_Q_COORD_Y, BRIBE_MENU_Q_COORD_X, LETTER_COLOR);
	add_text(q, BRIBE_YES, BRIBE_MENU_OP_COORD_Y, BRIBE_MENU_OP1_COORD_X, LETTER_COLOR);
	add_text(q, BRIBE_NO, BRIBE_MENU_OP_COORD_Y, BRIBE_MENU_OP2_COORD_X, LETTER_COLOR);
	add_text(q, BLOCK_MENU_CURSOR, BRIBE_MENU_OP_COORD_Y, BRIBE_MENU_CURSOR_COORD_X, LETTER_COLOR);

	while(1){

		switch(getchar()){

			case '\e':

				getchar();

				switch (getchar()){

					case 'C':

						if (option == 1) break;

						CHANGE_COLOR(FULL_MAP_BG_COLOR);
						for (i = 0; i < 4; i++) {
							MOVE_CURSOR(BRIBE_MENU_OP_COORD_Y + i, BRIBE_MENU_CURSOR_COORD_X);
							printf("   ");
						}
						add_text(q, BLOCK_MENU_CURSOR, BRIBE_MENU_OP_COORD_Y, BRIBE_MENU_OP2_COORD_X - 6, LETTER_COLOR);
						option++;

					break;

					case 'D':

						if (option == 0) break;

						CHANGE_COLOR(FULL_MAP_BG_COLOR);
						for (i = 0; i < 4; i++) {
							MOVE_CURSOR(BRIBE_MENU_OP_COORD_Y + i, BRIBE_MENU_OP2_COORD_X - 6);
							printf("   ");
						}
						add_text(q, BLOCK_MENU_CURSOR, BRIBE_MENU_OP_COORD_Y, BRIBE_MENU_CURSOR_COORD_X, LETTER_COLOR);
						option--;

						break;

					default: break;
				}
				break;

			case ENTER_KEY:

				switch(option){

					case 0:

						add_background(q, (MAX_MAP_COORD_Y - BRIBE_MENU_HEIGHT) / 2, 
							(MAX_MAP_COORD_X - BRIBE_MENU_WIDTH) / 2, BRIBE_MENU_HEIGHT, BRIBE_MENU_WIDTH, 
							FULL_MAP_BG_COLOR);

						add_text(q, BRIBE_QUESTION, BRIBE_MENU_Q_COORD_Y, BRIBE_MENU_Q_COORD_X, LETTER_COLOR);
						bribe_money = floor(interface_playerInputNumber(BRIBE_MENU_INPUT_COORD_Y,
						 BRIBE_MENU_INPUT_COORD_x));
						return_flag=refresh_police_bribe(game_getme(game),bribe_money);
						if(return_flag==ERROR){
							refresh_caughtlab(game_getme(game), caught_lab);
							bribe_flag = 0;
							return ERROR;
						}
						bribe_flag = 0;
						return OK;

					case 1:

						refresh_caughtlab(game_getme(game), caught_lab);
						bribe_flag = 0;
						return OK;

					default: break;
				}
			default: break;
		}
	}

	return OK;
}

void show_players(){

	char buf;

	add_background(q, PLAYERS_SCREEN_COORD_Y, PLAYERS_SCREEN_COORD_X, 
		PLAYERS_SCREEN_HEIGHT, PLAYERS_SCREEN_WIDTH, FULL_MAP_BG_COLOR);
	add_text(q, player_get_name(game_getme(game)), PLAYERS_SCREEN_NAME_COORD_Y, 
		PLAYERS_SCREEN_NAME_COORD_X, LETTER_COLOR);

	add_text(q, player_get_name(game_getforeigneplayer(game, 0)), 
		PLAYERS_SCREEN_NAME_COORD_Y + 10, PLAYERS_SCREEN_NAME_COORD_X, LETTER_COLOR);

	add_text(q, player_get_name(game_getforeigneplayer(game, 1)), 
		PLAYERS_SCREEN_NAME_COORD_Y + 10, PLAYERS_SCREEN_NAME_COORD_X, LETTER_COLOR);

	while((buf = getchar()) != '\t' && buf != BACK_KEY);
}