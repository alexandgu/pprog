
#include "refresh.h"

int refresh_all(Player *p){
  int flag, back,i;
  Building *traphouse;
  if (p == NULL) return ERROR;
  back=OK;
  flag=refresh_vigilance_formula(p);
    if(flag==ERROR)
      return ERROR;
    else if(flag==GAMEOVER)
      return GAMEOVER;
    else if(flag==WARNING)
      back=WARNING;
  flag=refresh_money_formula(p);
    if(flag==ERROR)
      return ERROR;
  refresh_drugs_formula(p);
  for(i=0;i<player_get_ntraphouse(p);i++){
    traphouse=player_get_traphouse(p,i);
    if(!traphouse)
      continue;
    if(building_getdrugs(traphouse)!=0){
      refresh_moneydrugdealing(p,traphouse);
    }
  }
  
  return back;
}

void refresh_clock (){

  int cont = 0;
  int pthread_type, pthread_state;
  Player * lobo, *alex, *clau;
  if(game->mode == SINGLE)
    alex = game_getme(game);
  else{
    lobo = game_getme(game);
    switch(player_get_id(lobo)){
      /*did not have time to handle the case where number of players is unknown etc so I always create 3 team members*/
      /*one for each team member and I try to order them so all the refresh functions are called in the same order*/
      case 0:
        alex = lobo;
        lobo = game_getforeigneplayer(game, 0);
        clau = game_getforeigneplayer(game, 1);
        break;
      case 1:
        alex = game_getforeigneplayer(game, 0);
        clau = game_getforeigneplayer(game, 1);
        break;
      case 2:
        clau = lobo;
        alex = game_getforeigneplayer(game, 0);
        lobo = game_getforeigneplayer(game, 1);
        break;
    }
  }
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &pthread_type);
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &pthread_state);

  while(1){
    if(game->mode != SINGLE){ /*if we dont play alone we call refresh with other players*/
        refresh_all(lobo); 
        refresh_all(clau);
    }
    if(refresh_all(alex)==WARNING)
        show_bottomInfo("YOUR VIGILANCE LEVEL IS TOO HIGH",RED);
    cont++;
    if (cont == 3){
      if(game->mode == SINGLE)
        refresh_police_raid(alex);
      cont = 0;
    }

    sleep(10);
  }
}

/*
    MONEY
            */

int refresh_money_formula(Player *p){
  long money_amount;
  int num_labsdest, num_rest,i;
  Block *b;
  Building *bu;
  assert(p!=NULL);
  
  num_rest=player_get_nrest(p);
  money_amount=player_get_money(p);
  
  /*money earned per restaurant*/
  if(num_rest>0){
      for(i=0; i<num_rest; i++){
        bu=player_get_rest(p,i);
        b=building_getparent(bu);
         if(b==NULL) 
          return ERROR;
        money_amount+=refresh_moneyrest(b, bu);
      }
  }
  
  /*money lost for each caught laboratory*/
  num_labsdest=player_get_nlabsdestroyed(p);
  money_amount= money_amount - refresh_moneylabsdest(num_labsdest);
  player_set_money(p, money_amount);
  return OK;
}

long refresh_moneyrest (Block *b, Building *bu){
  long money = 0;
  unsigned char type_block;
  unsigned short level_block, level_building;
  level_block=block_getlevel(b);
  level_building=building_getlevel(bu);
  type_block=block_gettype(b);

  switch (type_block){
      case CITY:
          money+=REST_CONSTANT*(level_building+1)*BESTPLACE_CONSTANT;
      break;
      case VILLAGE:
          money+=REST_CONSTANT*(level_building+1)*NORMALPLACE_CONSTANT;
      break;
      case HIGHWAY:
          money+=REST_CONSTANT*(level_building+1)*GOODPLACE_CONSTANT;
      break;
      case ROUTE:
          money+=REST_CONSTANT*(level_building+1)*BADPLACE_CONSTANT;
      break;
      case  DESERT:
          money+=LAB_CONSTANT*(level_building+1)*WORSTPLACE_CONSTANT;
      break;
      default: break;        
  }
  money = money * (level_block +1);/*the money you earn per building depends ond the level of the block where it is built*/
  return money;
}

long refresh_moneylabsdest(int num_labsdest){
  long money=0;
  money+=DESTROYED_CONSTANT*num_labsdest;
  return money;
}

int refresh_addmoney(Player *p, long money){
  assert(p!=NULL);
  p->money+=money;
  return OK;
}

int refresh_subtractmoney(Player *p, long money){
  assert(p!=NULL);
  p->money=p->money-money;
  return OK;
}

int refresh_moneyspent(Player *p, Building *b){
  unsigned short level_building, level_block;
  unsigned char type_building, type_block;
  long money=0;
  Block *parent;
  assert(b!=NULL);
  assert(p!=NULL);

  level_building=building_getlevel(b);
  type_building=building_gettype(b);
  parent=building_getparent(b);
    if(parent==NULL) 
      return ERROR;
  type_block=block_gettype(parent);
  level_block=block_getlevel(parent);

  switch(type_block){
      case CITY:
        money+= CITY_CONSTANT*(level_block + 1);
        break;
      case HIGHWAY:
        money+= HIGHWAY_CONSTANT*(level_block + 1);
        break;
      case ROUTE:
        money+= ROUTE_CONSTANT*(level_block + 1);
        break;
      case VILLAGE:
        money+= VILLAGE_CONSTANT*(level_block + 1);
        break;
      case  DESERT:
        money+=DESERT_CONSTANT*(level_block + 1);
        break;
      default: break;
  }

  switch(type_building){
      case LAB:
        money=money+LAB_CONSTANT;
        break;
      case REST:
        money=money+REST_CONSTANT;
        break;
      case TRAPHOUSE:
        money=money+TRAPHOUSE_CONSTANT;
      default:break;
  }

  if(money>player_get_money(p))
    return DONTDOTHAT;

  refresh_subtractmoney(p, money);
  return OK;
}

int refresh_moneyspentupgrade_building(Player *p, Building *bu){
  unsigned short level_building, level_block;
  unsigned char type_building, type_block;
  long money=0;
  Block *parent;
  assert(bu!=NULL);

  level_building=building_getlevel(bu);
  type_building=building_gettype(bu);
  parent=building_getparent(bu);
    if(parent==NULL) 
      return ERROR;
  type_block=block_gettype(parent);
  level_block=block_getlevel(parent);

  switch(type_block){
      case CITY:
        money+= CITY_CONSTANT*(level_building+1)*(level_block + 1);
        break;
      case HIGHWAY:
        money+= HIGHWAY_CONSTANT*(level_building+1)*(level_block + 1);
        break;
      case ROUTE:
        money+= ROUTE_CONSTANT*(level_building+1)*(level_block + 1);
        break;
      case VILLAGE:
        money+= VILLAGE_CONSTANT*(level_building+1)*(level_block + 1);
        break;
      case  DESERT:
        money+=DESERT_CONSTANT*(level_building+1)*(level_block + 1);
        break;
      default: break;
  }

  switch(type_building){
      case LAB:
        money=money*LAB_CONSTANT;
        break;
      case REST:
        money=money*REST_CONSTANT;
        break;
      case TRAPHOUSE:
        money=money*TRAPHOUSE_CONSTANT;
        break;
      default:break;
  }

  if(money>player_get_money(p))
    return DONTDOTHAT;

  refresh_subtractmoney(p, money);
  return OK;
}

int refresh_moneyspentupgrade_block(Player *p, Block *b){
  unsigned short level_block;
  unsigned char type_block;
  long money=0;
  assert(b!=NULL);

  type_block=block_gettype(b);
  level_block=block_getlevel(b);

  switch(type_block){
      case CITY:
        money+= CITY_CONSTANT*(level_block+10);
        break;
      case HIGHWAY:
        money+= HIGHWAY_CONSTANT*(level_block+10);
        break;
      case ROUTE:
        money+= ROUTE_CONSTANT*(level_block+10);
        break;
      case VILLAGE:
        money+= VILLAGE_CONSTANT*(level_block+10);
        break;
      case  DESERT:
        money+=DESERT_CONSTANT*(level_block+10);
        break;
      default: break;
  }

  if(money>player_get_money(p))
    return DONTDOTHAT;
    
  refresh_subtractmoney(p, money);
  return OK;
}

/*
    VIGILANCE
                */
int refresh_vigilance_formula(Player *p){
    long vigilance=0,flag=0;
    int *lost_buildings, *warning_buildings;
    int num_labsdest, num_labs, num_rest,i, num_warnings=0, num_caught=0;
    char buf[100];
    Block *b;
    Building *bu;
    assert(p!=NULL);
  
    num_labs=player_get_nlabs(p);
    num_rest=player_get_nrest(p);
    lost_buildings=(int*)malloc(num_labs*sizeof(int));
      if(lost_buildings==NULL)
        return ERROR;
    warning_buildings=(int*)malloc(num_labs*sizeof(int));
      if(warning_buildings==NULL)
        return ERROR;

    /*vigilance per lab*/
    if(num_labs>0){
        for(i=0; i<num_labs; i++){
          bu=player_get_lab(p,i);
          b=building_getparent(bu);
           if(b==NULL) 
            return ERROR;
          flag=refresh_vigilancelab(b, bu);
          if (flag==CAUGHT_LAB){ /*if you lose the lab*/
            refresh_caughtlab(p,i);
            lost_buildings[num_caught]=i;
            i=i-1; /*Al ser una lista, desaparece el elemento y el siguiente reemplaza su lugar*/
            num_labs=num_labs-1;
            num_caught+=1;
          }else if(flag==WARNING_VIGILANCE){/*if you have to be carefull with a lab*/
            warning_buildings[num_warnings]=i;
            num_warnings+=1;
            vigilance+=flag;
          }else{
            vigilance+=flag;
          }
        }
    }

    if(num_caught>=MAX_CAUGHT_LABS){ /*if you have lose too many labs, you lose the game*/
      free(lost_buildings);
      free(warning_buildings);
      return GAMEOVER; 
    }

    /*vigilance deducted per restaurant*/
    if(num_rest>0){
        for(i=0; i<num_rest; i++){
          bu=player_get_rest(p,i);
          b=building_getparent(bu);
           if(b==NULL) 
            return ERROR;
          vigilance-=refresh_vigilancerest(b, bu);
        }
    }

    /*the vigilance increases for each caught lab*/
    num_labsdest=player_get_nlabsdestroyed(p);
    vigilance= vigilance + refresh_vigilancelabsdest(num_labsdest);
    vigilance=vigilance*VIGILANCE_PLAYER_CONSTANT; /*so it is a number between 0 and 100*/
    if(vigilance>=GAMEOVER_VIGILANCE){
      free(lost_buildings);
      free(warning_buildings);
      return GAMEOVER; 
    } 
    
    if(num_warnings!=0){
      sprintf(buf,"You have to check out this %d laboratories because their vigilance level is HIGH:",num_warnings);
      if(game_getme(game) == p)

      show_bottomInfo(buf,GREEN);
      for(i=0;i<num_warnings;i++){
        sprintf(buf,"Laboratory number %d",warning_buildings[i]);
        show_bottomInfo(buf,GREEN);
      }
    }

    if(num_caught!=0){
      sprintf(buf,"You have lost this %d laboratories because their vigilance level was too HIGH:",num_caught);
          if(game_getme(game) == p)

      show_bottomInfo(buf,RED);
      for(i=0;i<num_caught;i++){
        sprintf(buf,"Laboratory number %d",lost_buildings[i]);
            if(game_getme(game) == p)

        show_bottomInfo(buf,RED);
      }
    }

    free(lost_buildings);
    free(warning_buildings);
    player_set_vigilance(p, vigilance);
    if(vigilance>=WARNING_VIGILANCE)
      return WARNING;
    return OK;
}

long refresh_vigilancelab(Block *b, Building *bu){
    long vigilance=0;
    unsigned char type_block;
    unsigned short level_block, level_building;
    level_block=block_getlevel(b);
    level_building=building_getlevel(bu);
    type_block=block_gettype(b);

     switch (type_block){
        case CITY:
          vigilance+=LAB_CONSTANT*GOODPLACE_CONSTANT+(level_building*25);
          break;
        case VILLAGE:
          vigilance+=LAB_CONSTANT*NORMALPLACE_CONSTANT+(level_building*33);
          break;
        case HIGHWAY:
          vigilance+=LAB_CONSTANT*BESTPLACE_CONSTANT+(level_building*17);
          break;
        case ROUTE:
          vigilance+=LAB_CONSTANT*BADPLACE_CONSTANT+(level_building*41);
          break;
        case DESERT:
          vigilance+=LAB_CONSTANT*WORSTPLACE_CONSTANT+(level_building*49);
          break;
        default: break;
     }

    vigilance = vigilance + (level_block *35);/*the vigilance of a building depends on the level of its block*/
    vigilance=vigilance*VIGILANCE_LAB_CONSTANT;
    if(vigilance>=CAUGHT_LAB)
      return CAUGHT_LAB;
    else if(vigilance>=WARNING_VIGILANCE){
      building_set_vigilance(bu, vigilance);
      return WARNING;
    }
    building_set_vigilance(bu, vigilance);
    return vigilance;
}

long refresh_vigilancerest(Block *b, Building *bu){
    long vigilance=0;
    unsigned short level_block, level_building;
    level_block=block_getlevel(b);
    level_building=building_getlevel(bu);

    vigilance=vigilance+((level_building+5)*FRONT_CONSTANT);

    vigilance = vigilance * (level_block +1);/*the vigilance of a building depends on the level of its block*/
    return vigilance;
}

long refresh_vigilancelabsdest(int num_labsdest){
  long vigilance=0;
  vigilance+=DESTROYED_CONSTANT*num_labsdest;
  return vigilance;
}

/*
    DRUGS
            */
int refresh_drugs_formula(Player *p){
  int num_labs,i;
  Building *lab;
  Block *parent;
  assert(p!=NULL);

  num_labs=player_get_nlabs(p);
  for(i=0;i<num_labs;i++){
    lab=player_get_lab(p,i);
    parent=building_getparent(lab);
    refresh_druglab(parent,lab);
  }

  return OK;
}

int refresh_druglab (Block *b, Building *bu){
  long drugs=0;
  unsigned char type_block;
  unsigned short level_block, level_building;
  level_block=block_getlevel(b);
  level_building=building_getlevel(bu);
  type_block=block_gettype(b);

  switch (type_block){
      case CITY:
          drugs+=DRUG_CONSTANT*(level_building+1)*WORSTPLACE_CONSTANT;
      break;
      case VILLAGE:
          drugs+=DRUG_CONSTANT*(level_building+1)*NORMALPLACE_CONSTANT;
      break;
      case HIGHWAY:
          drugs+=DRUG_CONSTANT*(level_building+1)*BADPLACE_CONSTANT;
      break;
      case ROUTE:
          drugs+=DRUG_CONSTANT*(level_building+1)*GOODPLACE_CONSTANT;
      break;
      case  DESERT:
          drugs+=DRUG_CONSTANT*(level_building+1)*BESTPLACE_CONSTANT;
      break;
      default: break;
  }
  drugs = drugs * (level_block +1);/*the drugs you obtain per lab depends ond the level of the block where it is built*/
  building_set_drugs(bu,drugs);
  return OK;
}

int refresh_subtract_drugs(Building *bu, long drugs){
  assert(bu!=NULL);
  assert(drugs!=0);
  if(drugs>bu->drugs)
    return ERROR;
  bu->drugs=bu->drugs-drugs;
  return OK;
}

int refresh_moneydrugdealing(Player *p, Building *bu){
  long money=0,sale;
  Block *parent;
  long drugs;
  unsigned short level_building,level_block;
  unsigned char place;
  assert(p!=NULL);
  assert(bu!=NULL);
  assert(building_gettype(bu)==TRAPHOUSE);

  parent=building_getparent(bu);
  level_block=block_getlevel(parent);
  drugs=building_getdrugs(bu);
  place=block_gettype(parent);
  level_building=building_getlevel(bu);

  if(place==CITY){
    sale=10*(level_building+1);
    if(sale>drugs){
      building_set_drugs(bu,0);
      money+=CITY_CONSTANT*(level_block+1)*drugs;
    }else{
      money+=CITY_CONSTANT*(level_block+1)*sale;
      refresh_subtract_drugs(bu,sale);
    }
    refresh_addmoney(p,money);
  }else if(place==VILLAGE){
    sale=5*(level_building+1);
    if(sale>drugs){
      building_set_drugs(bu,0);
      money+=VILLAGE_CONSTANT*(level_block+1)*drugs;
    }else{
      money+=VILLAGE_CONSTANT*(level_block+1)*sale;
      refresh_subtract_drugs(bu,sale);
    }
    refresh_addmoney(p,money);
  }else{
    return ERROR;
  }
  return OK;
}

/*
    EXTRAS
            */
int refresh_caughtlab(Player *p, int i){
  assert(p!=NULL);
  list_extractIndex(p->p_labs,i);
  
  player_increase_nlabsdestroyed(p);
  player_decrease_nlabs(p);

  return OK; 
}

void refresh_police_raid(Player *p){
  int num,num_labs;
  char buf[128];
  assert(p!=NULL);
  num_labs=player_get_nlabs(p);
  if(num_labs==0)
    return;
  num=aleat_num(1,20);;
  if(num==1||num==2){
    caught_lab=aleat_num(0,num_labs);
    bribe_flag = 1;
    sprintf(buf,"THE POLICE HAVE FOUND YOUR LABORATORY NUMBER %d IN A POLICE RAID",caught_lab);
    if(game_getme(game) == p)
      show_bottomInfo(buf,RED);
    
    
  }else if(num>=8 && num<=10){
    refresh_subtractmoney(p,player_get_money(p)/2);
    player_add_vigilance(p,10);
    if(game_getme(game) == p)
      show_bottomInfo("THE POLICE HAVE FINED YOU BECAUSE OF SOME IRREGULARITIES IN YOUR ACCOUNTS",RED);
  }else{
    if(game_getme(game) == p)
      show_bottomInfo("CONGRATULATIONS! THE POLICE DON\'T HAVE PROOFS AGAINST YOU. YOU ARE SAFE... FOR NOW",GREEN);
  }
}

int refresh_police_bribe(Player *p, long money){
  int num;
  assert(p!=NULL);

  num=aleat_num(1,10);
  if(money>=1000&&num>=5){
    refresh_subtractmoney(p,money);
    if(game_getme(game) == p)
      show_bottomInfo("YOU HAVE BRIBED THE POLICE",GREEN);
    return OK;
  }else if(money<1000){
    player_add_vigilance(p,20);
    player_set_money(p,0);
    if(game_getme(game) == p)
      show_bottomInfo("THE POLICE DID NOT TAKE YOUR MONEY. YOU HAVE LOST ALL OF YOUR MONEY AND YOUR VIGILANCE HAS INCREASED.",RED);
    return ERROR;
  }else if(num>=8){
    refresh_subtractmoney(p,money);
     if(game_getme(game) == p)

      show_bottomInfo("YOU HAVE BRIBED THE POLICE",GREEN);
    return OK;
  }else{
    player_add_vigilance(p,20);
    player_set_money(p,0);
        if(game_getme(game) == p)
          show_bottomInfo("THE POLICE DID NOT TAKE YOUR MONEY. YOU HAVE LOST ALL OF YOUR MONEY AND YOUR VIGILANCE HAS INCREASED.",RED);
    return ERROR;
  }
}

