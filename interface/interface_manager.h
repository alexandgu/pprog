/**
 * @file interface_manager.h
 * @author     Carlos Gómez-Lobo Hernaiz
 * @date       10 Jan 2019
 * @brief      Contains the prototypes and structures of the interface_manager
 *             ADT.
 *
 *             The interface_manaer ADT manages the interface calling the
 *             interface functions, interacts with the player through the
 *             keyboard and calls the functions that make the game mechanics to
 *             work.
 */

#ifndef INTERFACE_MANAGER
#define INTERFACE_MANAGER

#include "interface.h"
#include "../utils/types.h"
#include "../utils/queue.h"
#include "../music/music.h"

#define BUILD_LAB_TEXT "BUILD LABORATORY"
#define BUILD_REST_TEXT "BUILD RESTAURANT"
#define BUILD_TRAP_HOUSE_TEXT "BUILD TRAP HOUSE"
#define UPGRADE_LAB_TEXT "UPGRADE LAB"
#define UPGRADE_REST_TEXT "UPGRADE RESTAURANT"
#define UPGRADE_TRAP_HOUSE_TEXT "UPGRADE TRAP HOUSE"
#define SEND_DRUG_TEXT "SEND DRUG TO TRAP HOUSE"
#define CANT_DELIVER_DRUG_TEXT "THERE AIN\'T A TRAP HOUSE HERE MAN"
#define DRUG_DELIVER_SUCCESS_TEXT "DRUG DELIVERED SUCCESSFULLY"
#define CANT_BUILD_TEXT1 "YOU CAN\'T BUILD A "
#define CANT_BUILD_TEXT2 " HERE"
#define NOT_ENOUGH_MONEY_TEXT "YOU DON\'T HAVE ENOUGH MONEY"
#define NOT_UPGRADE_TEXT "YOU HAVE REACHED THE MAX LEVEL"
#define NOT_YOURS_TEXT "THIS BUILDING IS NOT YOURS"
#define ASSAULT_PLAYER_TEXT "ASSAULT PLAYER"
#define LAB_TEXT "LAB"
#define REST_TEXT "RESTAURANT"
#define TRAP_HOUSE_TEXT "TRAP HOUSE"
#define NEW_GAME_TEXT "NEW GAME"
#define LOAD_GAME_TEXT "LOAD GAME"
#define SINGLE_PLAYER_TEXT "SINGLE PLAYER"
#define MULTIPLAYER_TEXT "MULTIPLAYER"
#define WAITING_FOR_PLAYERS_TEXT "WAITING FOR OTHER PLAYERS"
#define GAME_OVER_TEXT "GAME OVER"
#define GREETINGS_TEXT "THANKS FOR PLAYING"
#define BLOCK_MENU_KEY_TEXT "SPACE - SHOW BLOCK MENU, SELECT"
#define HELP_KEY_TEXT "H - HELP SCREEN"
#define LEVEL_UP_KEY_TEXT "L - LEVEL UP BLOCK"
#define MINIMAP_KEY_TEXT "M - SHOW MINIMAP"
#define JUMP_TO_KEY_TEXT "J - JUMP TO BLOCK"
#define GO_BACK_KEY_TEXT "DEL - GO BACK"
#define CANCEL_DRUG_KEY_TEXT "C - CANCEL DRUG DELIVERY"
#define HOST_TEXT "HOST"
#define CLIENT_TEXT "CLIENT"
#define BRIBE_QUESTION "DO YOU WANT TO TRY TO BRIBE THEM?"
#define BRIBE_YES "YES"
#define BRIBE_NO "NO"
#define BRIBE_MONEY_QUESTION "hOW MUCH WILL YOU OFFER?"
#define HOSTING_QUESTION "WHO WILL YOU BE?"
#define MAIN_MENU_OPTIONS_NUM 2
#define MAX_BUILDS_PER_BLOCK 6

#define LEVEL_UP 'l'

/**
 * @brief      Type of movements.
 */
typedef enum {UP = 1, DOWN, RIGHT, LEFT} Move;

pthread_t print_thread;
Player *player;
int drug_transfer_flag, game_over_flag;
Building *lab_aux;

/**
 * @brief      Manages the map interaction with the player and its generation.
 *
 * @param      map   The map used in the game.
 *
 * @return     OK or ERROR on exit.
 */
Status map_manager(Map *map);

/**
 * @brief      Moves the selector from one block to another based on the move
 *             passed as argument.
 *
 * @param      map   The map used in the game.
 * @param      move  The move to be executed.
 *
 * @return     OK or ERROR on exit.
 */
Status move_selector(Map *map, Move move);

/**
 * @brief      Manages the block menu .
 *
 * @param      map   The map used in the game.
 *
 * @return     OK or ERROR on exit.
 */
Status blockMenu_manager (Map *map);

/**
 * @brief      Moves the cursor on the block menu based on the move and changes
 *             the option value.
 *
 * @param      move    The move to be executed.
 * @param      option  The option to be changed.
 */
void blockMenu_moveCursor (Move move, char *option);
/**
 * @brief      Erases the current options on the block menu.
 */
void blockMenu_eraseOptions();

/**
 * @brief      Shows the proper options on the block menu and changes the
 *             maxoption variable.
 *
 * @param      building   The building that determines the options to be shown.
 * @param      maxoption  The maximun number of oprions depending on the
 *                        building.
 *
 * @return     OK or ERROR on exit.
 */
Status blockMenu_showOptions (Building *building, char *maxoption);

/**
 * @brief      Manages the main screen.
 *
 * @return     OK or ERROR on exit,
 */
Status mainScreen_manager ();

/**
 * @brief      Moves the selector on the screen passed as argument.
 *
 * @param      move    The move ot be executed.
 * @param      pos     The position of the cursor.
 * @param      screen  The current screen.
 */
void mainScreen_moveSelector (Move move, char *pos, Screen screen);

/**
 * @brief      Manages the gamemode selection screen.
 *
 * @return     OK or ERROR on exit.
 */
Status mainScreen_gamemodeSelector ();

/**
 * @brief      Shows the waiting for players window text.
 */
void mainScreen_WP_text();

/**
 * @brief      Prints the help screen.
 */
void print_helpScreen();

/**
 * @brief      Manages the screen where you select if you are the host or a
 *             client.
 */
void mainScreen_multiplayerSelection();

/**
 * @brief      Manages what happens while waiting players when you are the host.
 */
void mainScreen_waitingPlayersServer();

/**
 * @brief      Manages what happens while waiting players when you are a client.
 */
void mainScreen_waitingPlayersClient();

/**
 * @brief      Shows the players in hte multiplayer gamemode.
 */
void show_players();

#endif