#include "interaction.h"

typedef enum {NEWPLAYER = 0, NEWBUILDING, INCREASELEVELBUILDING, INCREASELEVELBLOCK, MOVEDRUGS, ASSAULTPLAYER } action_type;

int interaction_receivefromnetwork(Game * game_pointer, char * action);
int interaction_sendovernetwork(Game * game_pointer, int sockfd, action_type type, ...);
Player * _interaction_newplayer(Game * game, char * name);
int _interaction_newbuilding(Game *, Player * p, Block * b, Building_type type_building);
int _interaction_movedrug(Game *game, Player *p, Building * origin, Building * dest);
int _interaction_increaselevel_building(Game * game, Player * p, Building * b);
int _interaction_increaselevel_block(Game * game, Player * p, Block * b);
int _interaction_assaultplayer(Game * game, Player *p, Building *oponent_building);

/*deprecated, players are created in the interface manager directly*/
Player * interaction_newplayer(Game * game, char* name){
    Client_interface *ci;
    if(SINGLE == game->mode || SERVER == game->mode)
        return _interaction_newplayer(game, name);
    else if(CLIENT == game->mode){
        ci = game->network;
        interaction_sendovernetwork(game, NEWPLAYER, ci->sockfd, name);
    }
    return NULL;

}

Player * _interaction_newplayer(Game * game, char * name) {
    Player * p;
    p = player_create(name, 1); 
    return p;
}

int interaction_newbuilding(Game * game, Player * p, Block * b, Building_type type_building){
    Client_interface *ci;
    Server_interface *si;
    int i;
    /*in this file all public functions keep the same structure*/
    if(SINGLE == game->mode)/*if we play alone we execute the interaction with the private function*/
        return _interaction_newbuilding(game, p, b, type_building);
    else if (CLIENT == game->mode){/*if we play alone we send it to the server*/
        ci = game->network;/*the action will be performed once the server sends it back to us*/
        return interaction_sendovernetwork(game, ci->sockfd,  NEWBUILDING, p, b, type_building);
    }
    /*if we are the server we should broadcast the interaction order and then execute it here*/
    si = game->network;
    for(i = 0; i < si->num_players; i++){
        if(si->remotesockfd[i] > -1)
            interaction_sendovernetwork(game,si->remotesockfd[i], NEWBUILDING,  p, b, type_building);
    }
    return _interaction_newbuilding(game, p, b, type_building);
}

int interaction_movedrug(Game *game, Player *p, Building * origin, Building * dest){
    Client_interface *ci;
    int i;
    Server_interface *si;
    if(SINGLE == game->mode){
        return _interaction_movedrug(game, p, origin, dest);
    }else if(CLIENT == game->mode){
        ci = game->network;
        return interaction_sendovernetwork(game, ci->sockfd, MOVEDRUGS, p, origin, dest);
    }
    si = game->network;
    for(i = 0; i < si->num_players; i++){
        if(si->remotesockfd[i] > -1)
            interaction_sendovernetwork(game,si->remotesockfd[i], MOVEDRUGS, p, origin, dest);
    }
    return _interaction_movedrug(game, p, origin, dest);
}

int _interaction_movedrug(Game *game, Player *p, Building * origin, Building * dest){
    if(building_gettype(origin) != LAB)
        return ERROR;
    if(building_gettype(dest) != TRAPHOUSE)
        return ERROR;
    dest->drugs += origin->drugs;
    building_set_drugs(origin,0);
    return OK;
}

int _interaction_newbuilding(Game * game, Player * p, Block * b, Building_type type_building) {
    Block_type type_block;
    char id;
    Building *bu = NULL;
    assert(p != NULL);
    assert(b != NULL);


    if(type_building==TRAPHOUSE  &&   block_gettype(b)!=VILLAGE && block_gettype(b)!=CITY)
        return ERROR;

    if(block_getBuilding(b,-1,type_building)!=NULL){
        return DONTDOTHAT;
    }

    if(player_get_nlabs(p)+player_get_nrest(p)+player_get_ntraphouse(p)==MAX_BUILDING_PER_PLAYER){
        return ERROR;
    }
    /*NEW LAB OR REST*/
    type_block = block_gettype(b);
    id = player_get_id(p);
    if ((block_addBuilding(b, type_building, id)) == ERROR)
        return ERROR;
        
    /*money spent*/
    switch (type_building) {
        case LAB:
            /*creating a lab*/
            bu = block_getBuilding(b, -1, LAB);
            if(player_add_lab(p,bu)==ERROR){
                block_deleteBuilding(b,-1,type_building);
                return ERROR;
            }
            break;
        case REST:
            /*creating a rest*/
            bu = block_getBuilding(b, -1, REST);
            if(player_add_rest(p,bu)==ERROR){
                block_deleteBuilding(b,-1,type_building);
                return ERROR;
            }
            break;
        case TRAPHOUSE: 
            bu = block_getBuilding(b, -1, TRAPHOUSE);
            if(player_add_traphouse(p, bu)==ERROR){
                block_deleteBuilding(b,-1,type_building);
                return ERROR;
            }
            break; 
        default: break;
    }
    if(refresh_moneyspent(p, bu)==DONTDOTHAT)
        return NOMONEY;
    return OK;
}

int interaction_increaselevel_building(Game * game, Player * p, Building * b) {
    Client_interface * ci;
    Server_interface *si;
    int i;
    if(SINGLE == game->mode)
        return _interaction_increaselevel_building(game, p, b);
    else if (CLIENT == game->mode){
        ci = game->network;
        return interaction_sendovernetwork(game, ci->sockfd,INCREASELEVELBUILDING,  p, b);
    }
    si = game->network;
    for(i = 0; i < si->num_players; i++){
        if(si->remotesockfd[i] > -1)
            interaction_sendovernetwork(game, si->remotesockfd[i], INCREASELEVELBUILDING,  p, b);
    }
    return _interaction_increaselevel_building(game, p, b);
}

int _interaction_increaselevel_building(Game * game, Player * p, Building * b) {
    assert(p != NULL);
    assert(b != NULL);

    if(building_getlevel(b)==MAX_LEVEL)
        return DONTDOTHAT;
    if(player_get_id(p) != building_getowner(b))
        return ERROR;
    if(refresh_moneyspentupgrade_building(p, b)==DONTDOTHAT) 
        return NOMONEY;
    building_upgrade(b);
    return OK;
}

int interaction_increaselevel_block(Game * game, Player * p, Block * b) {
    Client_interface * ci;
    Server_interface *si;
    int i;
    if(SINGLE == game->mode)
        return _interaction_increaselevel_block(game, p, b);
    else if (CLIENT == game->mode){
        ci = game->network;
        return interaction_sendovernetwork(game, ci->sockfd,INCREASELEVELBLOCK,  p, b);
    }
    si = game->network;
    for(i = 0; i < si->num_players; i++){
        if(si->remotesockfd[i] > -1)
            interaction_sendovernetwork(game, si->remotesockfd[i],INCREASELEVELBLOCK,  p, b);
    }
    return _interaction_increaselevel_block(game, p, b);
}

int _interaction_increaselevel_block(Game * game, Player * p, Block * b) {
    assert(p != NULL);
    assert(b != NULL);

    if(block_getlevel(b)==MAX_LEVEL)
        return DONTDOTHAT;
    if(refresh_moneyspentupgrade_block(p, b)==DONTDOTHAT) 
        return NOMONEY;
    block_upgrade(b); 
    return OK;
}
/*this function receives a buffer of bytes*/
/*it will decode the buffer into an interaction function call with all its argument*/
/*once done decoding it will execute the interaction */
/*this is what happens in the other side of the network right after a call to sendovernetwork is performed*/
int interaction_receivefromnetwork(Game * game_pointer, char * action)
{
    action_type type;
    Player * player_pointer;
    Building * building_pointer;
    Building * building_pointer_dest;
    short player_id;
    short x; 
    short y;
    Block * block_pointer;
    Map * map_pointer;
    Building_type type_building;
    type = *(action_type *)action;
    action += sizeof(action_type);
    switch(type){
        case NEWBUILDING:
            player_id = *(short *)action;
            if(player_id == game_getid(game))
                player_pointer = game_getme(game);
            else
                player_pointer = game_getforeigneplayer(game_pointer, player_id);
            if(!player_pointer)
                return ERROR;
            action += sizeof(short);
            x = *(short *)action;
            action += sizeof(short);
            y = *(short *)action;
            action += sizeof(short);
            type_building = *(Building_type *) action;
            map_pointer = game_getmap(game_pointer);
            block_pointer = map_getBlock(map_pointer,x, y);
            if(_interaction_newbuilding(game_pointer, player_pointer, block_pointer, type_building) == ERROR)
                return ERROR;
            break;
        case INCREASELEVELBUILDING:
            player_id = *(short *)action;
            if(player_id == game_getid(game))
                player_pointer = game_getme(game);
            else
                player_pointer = game_getforeigneplayer(game, player_id);
            action += sizeof(short);
            x = *(unsigned short *)action;
            action += sizeof(short);
            y = *(unsigned short *)action;
            action += sizeof(short);
            type_building = *(Building_type *) action;
            map_pointer = game_getmap(game_pointer);
            block_pointer = map_getBlock(map_pointer,x, y);
            building_pointer = block_getBuilding(block_pointer, -1, type_building);
            if(_interaction_increaselevel_building(game_pointer, player_pointer, building_pointer) == ERROR)
                return ERROR;
            break;
        case INCREASELEVELBLOCK:
            player_id = *(short *)action;
            if(player_id == game_getid(game))
                player_pointer = game_getme(game);
            else
                player_pointer = game_getforeigneplayer(game_pointer, player_id);
            action += sizeof(short);
            x = *(unsigned short *)action;
            action += sizeof(short);
            y = *(unsigned short *)action;
            action += sizeof(short);
            map_pointer = game_getmap(game_pointer);
            block_pointer = map_getBlock(map_pointer,x, y);
            if(_interaction_increaselevel_block(game_pointer, player_pointer, block_pointer) == ERROR)
                return ERROR;
            break;
        case MOVEDRUGS:
            player_id = *(short *)action;
            if(player_id == game_getid(game))
                player_pointer = game_getme(game);
            else
                player_pointer = game_getforeigneplayer(game, player_id);
            action += sizeof(short);
            x = *(short *)action;
            action += sizeof(short);
            y = *(short *)action;
            action += sizeof(short);
            block_pointer = map_getBlock(game_getmap(game), x, y);
            building_pointer = block_getBuilding(block_pointer, -1, LAB);
            x = *(short *)action;
            action += sizeof(short);
            y = *(short *)action;
            block_pointer = map_getBlock(game_getmap(game), x, y);
            building_pointer_dest = block_getBuilding(block_pointer, -1, LAB);
            if(_interaction_movedrug(game, player_pointer, building_pointer, building_pointer_dest) == ERROR)
                return ERROR;
            break;
        case ASSAULTPLAYER:
            player_id = *(short *)action;
            if(player_id == game_getid(game))
                player_pointer = game_getme(game);
            else
                player_pointer = game_getforeigneplayer(game_pointer, player_id);
            action += sizeof(short);
            x = *(unsigned short *)action;
            action += sizeof(short);
            y = *(unsigned short *)action;
            action += sizeof(short);
            map_pointer = game_getmap(game_pointer);
            block_pointer = map_getBlock(map_pointer,x, y);
            building_pointer = block_getBuilding(block_pointer, -1, LAB);
            if(_interaction_assaultplayer(game_pointer, player_pointer, building_pointer) == ERROR)
                return ERROR;
            break;
        default:
            return ERROR;
    }
    return OK;
}
/*This function is responsible to send all types of interaction over the network*/
/*each interaction receives different arguments, thats why we have the variables arguments*/
/*this function will retrieve arguments, transform them into bytes with data that uniquely identifies each argument*/
/*then it will send it over the network specified by the socket file descriptor in the second argument*/
int interaction_sendovernetwork(Game * game_pointer, int sockfd, action_type type, ...){
    va_list ap;
    char * message;
    Player * player_pointer;
    Block * block_pointer;
    Building * building_pointer;
    unsigned short *coords;
    FILE * f;
    Building_type building_type;
    short size;
    va_start(ap, type);
    switch(type){
        case NEWBUILDING:
            player_pointer = va_arg(ap, Player *);
            block_pointer = va_arg(ap, Block *);
            building_type = va_arg(ap, Building_type);
            assert(player_pointer != NULL);
            assert(block_pointer != NULL);
            size = sizeof(size) + 3 * sizeof(short) + sizeof(building_type) + sizeof(action_type);
            message = malloc(size);
            *(short *)message = size;
            message += sizeof(size);
            *(action_type *)message = type;
            message += sizeof(action_type);
            *(short *)message = game_getid(game_pointer);
            message += sizeof(short);
            coords = block_getcoords(block_pointer);
            *(short *)message = coords[0];
            message += sizeof(short);
            *(short *)message = coords[1];
            message += sizeof(short);
            *(Building_type *)message = building_type;
            message -= 3*sizeof(short) + sizeof(size) + sizeof(action_type);
            va_end(ap);
            return send(sockfd, message, size, 0);
        case INCREASELEVELBUILDING:
            player_pointer = va_arg(ap, Player *);
            building_pointer = va_arg(ap, Building *);
            assert(player_pointer != NULL);
            assert(building_pointer != NULL);
            block_pointer = building_getparent(building_pointer);
            building_type = building_gettype(building_pointer);
            size = sizeof(size) + 3 * sizeof(short) + sizeof(building_type) + sizeof(action_type);
            message = malloc(size);
            *(short *)message = size;
            message += sizeof(size);
            *(action_type *)message = type;
            message += sizeof(action_type);
            *(short *)message = game_getid(game_pointer);
            message += sizeof(short);
            coords = block_getcoords(block_pointer);
            *(short *)message = coords[0];
            message += sizeof(short);
            *(short *)message = coords[1];
            message+= sizeof(short);
            *(Building_type *)message = building_type;
            message -=  3*sizeof(short) + sizeof(size) + sizeof(action_type);
            va_end(ap);
            return send(sockfd, message, size, 0);
        case INCREASELEVELBLOCK:
            player_pointer = va_arg(ap, Player *);
            block_pointer = va_arg(ap, Block *);
            assert(player_pointer != NULL);
            assert(block_pointer != NULL);
            size = sizeof(size) + 3 * sizeof(short) + sizeof(action_type);
            message = malloc(size);
            *(short *)message = size; 
            message += sizeof(size);
            *(action_type *)message = type;
            message += sizeof(action_type);
            *(short *)message = game_getid(game_pointer);
            message += sizeof(short);
            coords = block_getcoords(block_pointer);
            *(short *)message = coords[0];
            message += sizeof(short);
            *(short *)message = coords[1];
            message -= sizeof(size) + 2*sizeof(short) + sizeof(action_type);
            va_end(ap);
            return send(sockfd, message, size, 0);
        case MOVEDRUGS:
            player_pointer = va_arg(ap, Player *);
            assert(player_pointer != NULL);
            size = sizeof(size) + 5 * sizeof(short) + sizeof(action_type);
            message = malloc(size);
            *(short *)message = size;
            message += sizeof(size);
            *(action_type *)message = type;
            message += sizeof(action_type);
            *(short *)message = game_getid(game_pointer);
            message += sizeof(short);
            block_pointer = va_arg(ap, Block *);
            assert(block_pointer != NULL);
            coords = block_getcoords(block_pointer);
            *(short *)message = coords[0];
            message += sizeof(short);
            *(short *)message = coords[1];
            message += sizeof(short);
            block_pointer = va_arg(ap, Block *);
            assert(block_pointer != NULL);
            coords = block_getcoords(block_pointer);
            *(short *)message = coords[0];
            message += sizeof(short);
            *(short *)message = coords[1];
            message -= 4*sizeof(short) + sizeof(size) + sizeof(action_type);
            va_end(ap);
            return send(sockfd, message, size, 0);
        case ASSAULTPLAYER:
            player_pointer = va_arg(ap, Player *);
            building_pointer = va_arg(ap, Building  *);
            size = sizeof(size) + sizeof(action_type) + 3*sizeof(short);
            message = malloc(size);
            *(short *)message = size;
            message += sizeof(size);
            *(action_type *)message = type;
            message += sizeof(action_type);
            *(short *)message = player_get_id(player_pointer);
            message += sizeof(short);
            coords = block_getcoords(building_getparent(building_pointer));
            *(short *)message = coords[0];
            message += sizeof(short);
            *(short *)message = coords[1];
            message -= sizeof(size)+ sizeof(action_type) + 2*sizeof(short);
            va_end(ap);
            return send(sockfd, message, size,0);
        default:
            return -1;
    }
    va_end(ap);
    return OK;
}

int interaction_assaultplayer(Game * game, Player *p, Building * oponent_building){
    Client_interface * ci;
    Server_interface *si;
    int i;
    if(SINGLE == game->mode)
        return _interaction_assaultplayer(game, p, oponent_building);
    else if (CLIENT == game->mode){
        ci = game->network;
        return interaction_sendovernetwork(game, ci->sockfd,INCREASELEVELBLOCK,  p, oponent_building);
    }
    si = game->network;
    for(i = 0; i < si->num_players; i++){
        if(si->remotesockfd[i] > -1)
            interaction_sendovernetwork(game, si->remotesockfd[i],INCREASELEVELBLOCK,  p, oponent_building);
    }
    return _interaction_assaultplayer(game, p, oponent_building);
}

int _interaction_assaultplayer(Game * game, Player *p, Building *oponent_building){
    Building *lab;
    assert(p!=NULL);
    assert(oponent_building!=NULL);
    assert(building_gettype(oponent_building)==LAB);

    if(player_get_money(p)<2000||player_get_nlabs(p)==0){
        return DONTDOTHAT;
    }else{
        refresh_subtractmoney(p,2000);
        if(aleat_num(1,8)<=2){
            lab=player_get_lab(p,0);
            building_set_level(lab,0);
            show_bottomInfo("Your laboratory number 0 is now on level 0...Haha karma is a bitch",RED);
            return OK;
        }
        building_set_level(oponent_building,0);
        show_bottomInfo("You have assaulted your oponent succesfully!",GREEN);
    }

    return OK;
}


