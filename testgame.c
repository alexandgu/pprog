#include "utils/includes.h"
#include "construction/map.h"
#include "utils/includes.h"
#include "network/server_interface.h"
#include "network/client_interface.h"
#include "interface/interface_manager.h"

Map * m;
void *single_game(void *);
void *multiplayer_client(void *);
void *multiplayer_host(void *);
void exit_game();

int main(int argc, char ** argv){
    char game_type = 'a';
    int flag = EXIT_SUCCESS;
    Client_interface *ci;
    m = map_readfromfile("fichero.txt");
    pthread_t this_thread;
	system("stty -echo");
	system("stty raw");
    printf("Welcome to the Breaking Bad Game:\n");
    q = queue_ini(destroy_function, copy_function, print_function);
	if (q == NULL) return EXIT_FAILURE;
	if (pthread_create(&print_thread, NULL, (void *) function_executor, q)){
    	    fprintf(stderr, "Error creating thread.\n");
	        return ERROR;
	}
    do{
        printf("Press 0 for single player\n");
        printf("Press 1 for multi player\n");
        scanf("%c", &game_type);
    }while(game_type != '0'  && game_type != '1');
    if(game_type == '1'){
        do{
            printf("Press 2 to connect to another game\n");
            printf("Press 3 to host the game\n");
            scanf("%c", &game_type);
        }while(game_type != '2' && game_type != '3');
        if('3' == game_type){
            game = game_ini(SERVER);
            game_setmap(game, m);
            multiplayer_host(NULL);
        }else if('2' == game_type){
            game = game_ini(CLIENT);
            pthread_create(&this_thread, NULL, multiplayer_client, NULL);
            sleep(1);
            ci = game->network;
            while(ci->gtype != PLAYING)
                sleep(1);
            m = game_getmap(game);
	        if (map_manager(m) == ERROR) flag = EXIT_FAILURE;
        }
    }else{
        game = game_ini(SINGLE);
        game_setmap(game, m);
        if (mainScreen_manager() == ERROR) flag = EXIT_FAILURE;
	    if (map_manager(m) == ERROR) flag = EXIT_FAILURE;
    }
    return EXIT_FAILURE;
}
void *multiplayer_client(void *entry){
    char* nombre;
    Player * player;
    pthread_t receiver;
    Client_interface *ci;
    player = player_create("client", -1);
    game_setme(game, player);
    ci = client_interface_ini(game);
    game_setnetwork(game, ci);
    client_sendmessage(ci, "client");
    pthread_create(&receiver, NULL, client_readfromserver, ci);
    return NULL;
}
void *multiplayer_host(void *entry){
    
    /* aqui ya nos conectariamos*/
    exit(1);
    return NULL;
}

void *single_game(void *entry){
    Map *m;
    m = map_readfromfile("map.bbg");
	printf("%c[?25l", 27);
	printf(CSI "2J");
    if (mainScreen_manager() == ERROR) exit(EXIT_FAILURE);
	if (map_manager(m) == ERROR) exit(EXIT_FAILURE);
	/*if (map_manager(m) == ERROR) 
        goto ERROR;
	ERROR:
	map_destruct(m);
	printf("%c[?25h", 27);
	printf(CSI "0m");*/
	exit_game();
    return NULL;
}

void exit_game(){
    system("stty echo");
	system("stty cooked");
    exit(EXIT_SUCCESS);
}
