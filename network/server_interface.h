/* *************************************************** 
 *
 *                     PPROG PROJECT
 * 
 *  YOU SHALL NEVER USE THIS CODE
 * 
 * *************************************************** */
#ifndef SERVER_INTERFACE_H
#define SERVER_INTERFACE_H

#include "../utils/includes.h"
#include "../interface/player.h"
#include "../utils/queue.h"
#include "../game/game.h"
#include "../interface/interaction.h"


#define BACKLOG MAX_PLAYERS


typedef enum {LAB_FOUNG = 0, LAB_ATTACKED = 1, DRUG_FOUN} Message_Type;

typedef struct _server_interface Server_interface;
typedef struct _client_handler Client_handler;


struct _server_interface{
    /* Defining Variables */
	int localsockfd;
    int num_players;
    int remotesockfd[MAX_PLAYERS]; 
    pthread_t threads[MAX_PLAYERS];
	sockin* addr_remote[MAX_PLAYERS]; /* server addr */
    sockin* addr_local;
	char revbuf[LENGTH]; /*Receiver buffer*/
    Queue* actionsq;
    Game_type gtype;
    Client_handler * ch[MAX_PLAYERS];
    Game * game;
};
 struct _client_handler{
    int sockfd;
    sockin* addr_rem;
    char buf[LENGTH];
    Queue* actionsq;
    char* name;
    Game_type* gtype;
};
/*Initializes the server interface structure
  Will allocate memory for said structure and a buffer to read/send bytes
  Will try to open a socket where clients will connect in case of failure will exit the execution
  Creates a queue where it stores all the messages it receives
  Also creates an array of structure client_handlers which will handle the connections with the clients and read data from clients
  */
Server_interface * server_ini();

/*will accept incoming connections to the socket opened in server_ini() 
  Input for this function is a pointer to server_interface, calling it with other pointer will be undefined behaviour (SEG FAULT MOST LIKELY)
  Will create a thread with each incoming connection
  the function that will actually handle the connection is client_talker(which is private)
  will allocate memory for a client handler for each incoming connection
  returns always NULL*/
void* server_clientstarter(void * entry);

/*This function will broadcast all information to all connected clients
 That is:
 checking if clients are still connecting
 broadcast the playing map
 broadcast all interactions sent from other players
 always return NULL (not expected to return nothing, will be an infinite while)*/
void *server_broadcaster(void *entry);

/*Sends the map of the game using the socket file descriptor passed as argument
    File needs to be called fichero.txt
*/
Status server_sendmap(int sockfd);


Status generate_map(Map *m);
#endif
