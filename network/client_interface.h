/* *************************************************** 
 *
 *                     PPROG PROJECT
 * 
 *  YOU SHALL NEVER USE THIS CODE
 * 
 * *************************************************** */
#ifndef client_interface_h
#define client_interface_h

#include "../utils/includes.h"
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <signal.h>
#include <ctype.h>          
#include <arpa/inet.h>
#include <netdb.h>
#include "errno.h"
#include  "../interface/interaction.h"
#include "../game/game.h"

typedef struct _client_interface Client_interface;

struct _client_interface{
    /* Defining Variables */
	int sockfd; 
	sockin * addr_remote; /* server addr */
	char revbuf[LENGTH]; /*Receiver buffer*/
	Game_type gtype;
	Game *game;
};
/*Allocates memory for client_interface and connects to a hardcoded IP
in case of failiure will return NULL, otherwise a pointer to a client_interface*/
Client_interface* client_interface_ini(Game * game);
/*reads a map from the socked specified in ci, the socket should be clean when calling this function and
  the first bytes output from recv() should be the first bytes sent from server_sendmap 
  when using this function it should be synchronized in some way with the other side of the connection
  this function will overwrite anything writen in the file called map.bbg*/
Status client_receivemapfromserver(Client_interface * ci);

/*this function will read all the instructions/interactions from the server
  Entry should be a pointer to a client_interface, different pointer will be undefined behaviour (SEGFAULT most likely)
  return NULL (should never return something as it is in an infinite loop*/
void * client_readfromserver(void *entry);

/*this function allows to simply send message to the server from other .c files and will handle all the socket/size etc etc*/
Status client_sendmessage(Client_interface * ci, char * message);


#endif
