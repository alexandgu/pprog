#include "client_interface.h"

Status client_updatebuffer(Client_interface * ci);
Status client_handlenewplayer(Client_interface *ci);
Client_interface* client_interface_ini(Game * game)
{
	/* Variable Definition */
	int sockfd; 
	int result;
	sockin* remote_addr;
	Client_interface* ci;
	ci = (Client_interface *)malloc(sizeof(Client_interface));
	remote_addr = (sockin *)malloc(sizeof(sockin));

	if(ci == NULL  || remote_addr == NULL)
		return NULL;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
		return NULL;
	remote_addr->sin_family = AF_INET; 
	remote_addr->sin_port = htons(PORT); 
	inet_pton(AF_INET, "192.168.43.159", &remote_addr->sin_addr); 
	bzero(&(remote_addr->sin_zero), 8);

	result = connect(sockfd, (sock *)remote_addr, sizeof(sock));
	fprintf(stderr, "Result is %d\n", result);
	if(result < 0)
		return NULL;
	ci->sockfd = sockfd;
	ci->game = game;
	ci->addr_remote = remote_addr;
	ci->gtype = STARTING;
	return ci;
}

Status client_receivemapfromserver(Client_interface * ci) 
{
	char * fr_name = "map.bbg";
	FILE * fr = fopen(fr_name, "w");
	int fr_block_sz = 0;
	if (fr == NULL)
		fprintf(stderr, "Couldn't open file on client\n");
	else {
		bzero(ci->revbuf, LENGTH);
		while ((fr_block_sz = recv(ci->sockfd, ci->revbuf, LENGTH, 0)) > 0) {
			int write_sz = fwrite(ci->revbuf, sizeof(char), fr_block_sz, fr);
			if (write_sz < fr_block_sz) {
				fprintf(stderr, "File write failed.\n");
				fclose(fr);
				return ERROR;
			}
			bzero(ci->revbuf, LENGTH);
			if (fr_block_sz == 0 || fr_block_sz != 512) {
				break;
			}
		}
		if (fr_block_sz < 0) {
			fprintf(stderr, "Network error: %s\n", strerror(errno));
			fclose(fr);
			return ERROR;
		}
		fclose(fr);
	}
	return OK;
}

void * client_readfromserver(void *entry){
	Client_interface* ci = (void *)entry;
	char * message;
	int received = 0;
	message = ci->revbuf;
	while(STARTING == ci->gtype){
		client_updatebuffer(ci);
		if(!strcmp(message, "starting"))
			continue;
		if(!strcmp(message, "broadcasting")){
			ci->gtype = MAPBROADCAST;
			break;
		}
		if(!strcmp(message, "playing")) /*should not happened*/
			continue;
	}
	while(MAPBROADCAST == ci->gtype){
		client_updatebuffer(ci);
		if(!strcmp(ci->revbuf, "playing")){
			fprintf(stderr, "GOT IT\n");
			ci->gtype = PLAYING;
		}
		if(!received && !strcmp(ci->revbuf, "map")){
			client_receivemapfromserver(ci);
			game_setmap(ci->game, map_readfromfile("map.bbg"));
			received = 1;
		}
	}
	while(PLAYING == ci->gtype){
		/*read from the server*/
		client_updatebuffer(ci);
		/*perform the interaction asked by the server*/
		interaction_receivefromnetwork(ci->game, ci->revbuf);
		/*this sleep should not be necessary because recv() is a blocking function*/
		/*however my instinct tells me to keep it here so here it is*/
		usleep(100000);
	}
	return NULL;
}
/*just an easy way to send messages to the server*/
Status client_sendmessage(Client_interface * ci, char * message){
	short size;
	char * outgoing_buffer;
	size = strlen(message) + sizeof(size);
	outgoing_buffer = malloc(sizeof(size) + strlen(message));
	*(short *)outgoing_buffer = size;
	strcpy(outgoing_buffer + sizeof(size), message);
	if(send(ci->sockfd, outgoing_buffer, size, 0) == -1)
		return ERROR;
	free(outgoing_buffer);
	return OK;
}

Status client_updatebuffer(Client_interface * ci){
	short size;
	int block_size;
	size = 0;
	memset(ci->revbuf, 0, LENGTH);
	block_size = recv(ci->sockfd, &size, sizeof(size), 0);/*we first read how many bytes the message contains*/
	if(block_size < 0){
		fprintf(stderr, "Error updating the buffer %s", strerror(errno) );
		return ERROR;
	}else if(0 == block_size){
		fprintf(stderr, "Update buffer error block_size = 0\n");
		return ERROR;
	}
	if(size < LENGTH)
		block_size = recv(ci->sockfd, ci->revbuf, size, 0);/*we read what we are told*/
	else 
		block_size = recv(ci->sockfd, ci->revbuf, LENGTH, 0);
	if(block_size <= 0){
		fprintf(stderr, "Error updating buffer once size was read %s", strerror(errno));
		return ERROR;
	}
	return OK;
}

Status client_handlenewplayer(Client_interface * ci){
	short id;
	char * message;
	message = ci->revbuf;
	id = *(short *)ci->revbuf;
	message += sizeof(id);
	if(strcmp(message, player_get_name(game_getme(ci->game)))){
		/*we create a new player with the given info*/
		return game_addforeign(ci->game, player_create(message, id));
	}else{
		/*we set the id we are told*/
		player_set_id(game_getme(ci->game), id);
		return OK;
	}	
}
