/* *************************************************** 
 *
 *                     PPROG PROJECT
 * 
 *  YOU SHALL NEVER USE THIS CODE
 * 
 * *************************************************** */
#include "server_interface.h"

Status server_sendmessage(Server_interface *, char *);
Status server_handlenewplayer(Server_interface * si, char * message);
Status clienthandler_updatebufferstring(Client_handler * ch);
Status server_broadcastseed(Server_interface *si);
void * client_talker(void *); 
void * buffercopier(const void *);

Status clienthandler_updatebuffer(Client_handler *);
Status server_updatenames(Game *game, int sockfd);
Status server_broadcast(Server_interface *si, char * message);

Server_interface * server_ini(Game * game){
    Server_interface * si;
    int i;
    si = (Server_interface *)malloc(sizeof(Server_interface));
    si->addr_local = (sockin *)malloc(sizeof(sockin));
    si->gtype = STARTING;
    si->game = game;
    for(i = 0; i < MAX_PLAYERS; i++){
        si->remotesockfd[i] = -1;
    }
    si->num_players = 0;
    si->localsockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(si->localsockfd < 0)
        return NULL;
	/* Fill the client socket address struct */
	si->addr_local->sin_family = AF_INET; /* Protocol Family */
	si->addr_local->sin_port = htons(PORT); /* Port number */
	si->addr_local->sin_addr.s_addr = INADDR_ANY; /* AutoFill local address */
    si->actionsq = queue_ini(free, buffercopier, NULL);
	bzero(&(si->addr_local->sin_zero), 8); /* Flush the rest of struct */

	/* Bind a special Port */
	if( bind(si->localsockfd, (sock*)si->addr_local, sizeof(sock)) == -1 )
	{
		fprintf(stderr, "ERROR: Failed to bind Port. (errno = %d)\n", errno);
		exit(1);
	}
    if(listen(si->localsockfd,BACKLOG) == -1)
	{
		fprintf(stderr, "ERROR: Failed to listen Port. (errno = %d)\n", errno);
	}
    return si;
}

void* client_talker(void * entry){
    Client_handler *ch  = (Client_handler *)entry;

    while(STARTING == *(ch->gtype)){
        /*this is commented because it was buggy and we needed to present something*/
        /*clienthandler_updatebufferstring(ch);
        queue_insert(ch->actionsq, ch->buf);*/
        sleep(1);
    }
    while(MAPBROADCAST == *(ch->gtype)){
        /*nobody should be speaking to us right now*/
        sleep(1);
    }
    while(PLAYING == *(ch->gtype)){
        /*read message*/
        clienthandler_updatebuffer(ch);
        /*safe to insert from multiple threads simultaneously because of the mutex*/
        queue_insert(ch->actionsq, ch->buf);
    }
    while(ENDING == *(ch->gtype))
    {
        /*thhis should handle the removal of the player or the inactivity*/
        sleep(10);
    }
    return NULL;
}

void* server_clientstarter(void * entry)
{
    int nsockfd;
    Client_handler *ch;
    unsigned int sinsize = sin_size;
    Server_interface* si = (Server_interface *)entry;

    while(si->num_players < MAX_PLAYERS && STARTING == si->gtype)
	{
		/* Wait a connection, and obtain a new socket file despriptor for single connection */
		if ((nsockfd = accept(si->localsockfd, (sock *)&si->addr_remote[si->num_players], &sinsize)) == -1) 
		{
		    fprintf(stderr, "ERROR: Obtaining new Socket Despcritor. (errno = %s)\n", strerror(errno));
			exit(1);
		}
		else {
            ch = (Client_handler *)malloc(sizeof(Client_handler));
            ch->sockfd = nsockfd;
            /*server_updatenames(si->game, nsockfd);*/
            si->remotesockfd[si->num_players] = nsockfd; /*keep the sockfd to broadcast messages*/
            si->ch[si->num_players] = ch;
            ch->addr_rem = si->addr_remote[si->num_players]; /*store the sockfd in order to read*/
            ch->actionsq = si->actionsq;
            ch->gtype = &si->gtype;
            pthread_create(&si->threads[si->num_players], NULL, client_talker, ch);
            si->num_players++;
        }
	}
    return NULL;
}

void* server_broadcaster(void * entry)
{
    Server_interface* si = (Server_interface *)entry;
    int i;
    time_t t;
    unsigned int seed;
    char*  message;
    int sent = 0;
    message = malloc(1024);
    /*here we wait to process the info received from the clients*/
    /*the queue stores all the messages from the clients*/
    while(STARTING == si->gtype){
        
        usleep(1000000);
    }
    while(MAPBROADCAST == si->gtype){
        server_sendmessage(si, "broadcasting");
        for(i = 0; i < si->num_players; i++){
            if(!sent){
                server_sendmessage(si, "map");
                server_sendmap(si->remotesockfd[i]);
                sent = 1;
            }
        }
        sleep(1);
    }
    /*server_broadcastseed(si);*/
    server_sendmessage(si, "playing");
    while(PLAYING == si->gtype)
    {
        /*safe to extract from this thread because of mutex*/
        if(!queue_isEmpty(si->actionsq)){
            message = queue_extract(si->actionsq);
            server_broadcast(si, message);
            interaction_receivefromnetwork(si->game, message+sizeof(short));
        }
        usleep(100000);
    }
    while(ENDING == si->gtype){
        usleep(10000);
    }
    return NULL;
}
/*sends a map to a client*/
Status server_sendmap(int sockfd)
{
	FILE *fs = fopen("prueba.txt", "r");
    char sdbuf[LENGTH];
	int fs_block_sz; 
    if(fs == NULL)
	{
		printf("ERROR: File %s not found.\n", "breaking_bad");
		return ERROR;
	} 
    bzero(sdbuf, LENGTH); 
	while((fs_block_sz = fread(sdbuf, sizeof(char), LENGTH, fs)) > 0)/*read in 512 blocks until one is not complete whiche means EOF*/
	{
        /*send what you read*/
	    if(send(sockfd, sdbuf, fs_block_sz, 0) < 0)
	    {
	        fprintf(stderr, "ERROR: Failed to send file %s. (errno = %d)\n", "breaking_bad", errno);
	        return ERROR;
	    }
	    bzero(sdbuf, LENGTH);
	}
    return OK;
}
/*this should broadcast a seed to intialize the random calls*/
/*this is important so all the refresh calls output the same in all the computers*/
Status server_broadcastseed(Server_interface *si){
    char * message;
    time_t t;
    return OK;
    if(server_sendmessage(si, "seed") == ERROR)
        return ERROR;
    message = malloc(sizeof(short) + sizeof(int));
    *(short *)message = sizeof(short)+sizeof(int);
    message += sizeof(short);
    *(int *)message = time(&t);
    srand(*(int *)message);
    message -= sizeof(short);
    if(server_broadcast(si, message) == ERROR)
        return ERROR;
    return OK;
}
/*broadcast a bufer of bytes*/
Status server_broadcast(Server_interface * si, char * data){
    int i;
    int block_size;
    short size;
    size = *(short *)data;
    for(i = 0; i < si->num_players; i++){
        if(si->remotesockfd[i] > -1)
            block_size = send(si->remotesockfd[i], data, size, 0);
        if(block_size < 0){
            fprintf(stderr,"We got a network error while trying to send a message %s\n", strerror(errno));
            /*we dont exit here the function because the show must go on!*/
        }
    }
    return OK;
}
/*broadcast a string with text note here we use strlen because it is a string*/
Status server_sendmessage(Server_interface * si, char * message){
    int i;
    int block_size;
    memset(si->revbuf, 0, LENGTH);
    *(short *)si->revbuf = strlen(message);
    strcpy(si->revbuf + sizeof(short), message);
    for(i = 0; i < si->num_players; i++){
        fprintf(stderr, "sending :%s\n", message);
        if(si->remotesockfd[i] > -1)
            block_size = send(si->remotesockfd[i], si->revbuf, sizeof(short) + strlen(message), 0);
        if(block_size < 0){
            fprintf(stderr,"We got a network error while trying to send a message %s\n", strerror(errno));
            /*we dont exit here the function because the show must go on!*/
        }
    }
    return OK;

}
/*updates the buffer for a given client handler this is what actually receives the message from the clients*/
Status clienthandler_updatebuffer(Client_handler * ch){
    short size;
	int block_size;
    FILE *f;
    char * data;
	size = 0;
	memset(ch->buf, 0, LENGTH);
	block_size = recv(ch->sockfd, &size, sizeof(size), 0);/*we peek how many bytes we should read*/
    data = malloc(size);
	if(block_size < 0){
		fprintf(stderr, "Error updating the buffer %s", strerror(errno) );
		return ERROR;
	}else if(0 == block_size){
		fprintf(stderr, "Handle error block_size = 0\n");
		return ERROR;
	}
    *(short *)data = size;
	if(size < LENGTH){/*if we have not surpassed our buffer we read what we were told*/
		block_size = recv(ch->sockfd, data+sizeof(short), size-sizeof(short), 0);
        memcpy(ch->buf, data, size);
    }
	else {
		block_size = recv(ch->sockfd, data+sizeof(short), LENGTH, 0); /*preventing potential memory overflow*/
        memcpy(ch->buf, data, LENGTH);
    }
	if(block_size <= 0){
		fprintf(stderr, "Error updating buffer once size was read %s", strerror(errno));
		return ERROR;
	}
    free(data);
	return OK;
}

/*this fucntion is used when we expect a string from the client*/
Status clienthandler_updatebufferstring(Client_handler * ch){
    short size;
	int block_size;
    FILE *f;
	memset(ch->buf, 0, LENGTH);
	block_size = recv(ch->sockfd, &size, sizeof(size), 0);
	if(block_size < 0){
		fprintf(stderr, "Error updating the buffer %s", strerror(errno) );
		return ERROR;
	}else if(0 == block_size){
		fprintf(stderr, "Handle error block_size = 0\n");
		return ERROR;
	}
	if(size < LENGTH)
		block_size = recv(ch->sockfd, ch->buf,  size, 0);
	else 
		block_size = recv(ch->sockfd,ch->buf , LENGTH, 0);
	if(block_size <= 0){
		fprintf(stderr, "Error updating buffer once size was read %s", strerror(errno));
		return ERROR;
	}
	return OK;
}
/*this would be used to handle the names and the ids of new players connected as
 * those should be broadcasted to all the clients*/
Status server_handlenewplayer(Server_interface * si, char * message){
    Player * player;
    short id;
    char * tosend_message;
    short size;
    fprintf(stderr, "Creating player %s\t", message);
    player = player_create(message, game_getsizeforeign(si->game)+1);
    game_addforeign(si->game, player);
    id =game_getsizeforeign(si->game);
    fprintf(stderr, "with id %d\n", id);
    size = sizeof(id) + sizeof(size) + strlen(message);
    tosend_message = malloc(size);
    *(short *)tosend_message = size;
    tosend_message += sizeof(short);
    *(short *)tosend_message = id;
    strcpy(tosend_message + 2*sizeof(id), message);
    if(server_broadcast(si, tosend_message) == ERROR){
        free(tosend_message);
        return ERROR;
    }
    return OK;
}
/*This one is used to send a list with all the connected players*/
/*this should be called to all clients between STARTING and MAPBROADCAST*/

Status server_updatenames(Game *game, int sockfd){
    char * message;
    Player * player;
    char * name;
    short id;
    short size;
    int i;
    message = malloc(512);
    i = game_getsizeforeign(game);
    for(i = 0; i < game_getsizeforeign(game);i++){
        player = game_getforeigneplayer(game, i);
        name = player_get_name(player);
        id = player_get_id(player);
        size = strlen(name)+ 2*sizeof(short);
        *(short *)message = size;
        message += sizeof(short);
        *(short *)message = id;
        strcpy(message + sizeof(id), name);
        message -= sizeof(short);
        send(sockfd, message, strlen(name) + 2*sizeof(short), 0);
    }
    name = player_get_name(game_getme(game));
    size = strlen(name)+2*sizeof(short);
    *(short *)message = size;
    message += sizeof(short);
    *(short *)message = 0;
    strcpy(message +sizeof(short), name);
    message -= sizeof(short);
    send(sockfd, message, strlen(name)+2*sizeof(short), 0);
    free(message);
    return OK;
}

/*function used by the queue to insert data to the queue*/
void * buffercopier(const void *data){
    char * ret;
    ret = malloc(512);
    memcpy(ret, data, 512);
    return ret;
}