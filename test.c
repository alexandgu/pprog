#include "construction/map.h"
#include "construction/block.h"
#include "utils/includes.h"
#include "construction/building.h"
#include <stdio.h>
#include <stdlib.h>
#include "utils/permutaciones.h"
#include "utils/utils.h"

#define NUMCITIES 10
#define NUMVILLAGES 30
int main(int argc, char const *argv[])
{
    Map *m;
    Block *b;
    int ** cities;
    int ** villages;
    int * perm;
    int pos;
    int * city1;
    int * city2;
    unsigned short i,j, k = 0, n = 0;
    cities = (int **)malloc(NUMCITIES*sizeof(*cities));
    for(i = 0; i < NUMCITIES;i++)
    {
     cities[i] = (int *)malloc(2*sizeof(cities));   
    }
    villages = (int **)malloc(NUMVILLAGES*sizeof(*villages));
    for(i = 0; i < NUMVILLAGES;i++)
    {
     villages[i] = (int *)malloc(2*sizeof(villages));   
    }
    m = map_ini(20, 20);
    for(i = 0; i < 20; i++)
    {
        for(j = 0; j < 20; j++)
        {
            b = block_ini(DESERT, i, j);
            m->cells[i][j]  = b;
            
        }
    }
    for(n = 0; n< NUMVILLAGES; n++){
        i = aleat_num(0, 19);
        j = aleat_num(0, 19);
        villages[n][0] = i;
        villages[n][1] = j;
        m->cells[i][j]->type = VILLAGE;
    }
    for(n = 0; n< NUMCITIES; n++){
        i = aleat_num(0, 19);
        j = aleat_num(0, 19);
        cities[n][0] = i;
        cities[n][1] = j;
        m->cells[i][j]->type = CITY;
    }
    city1 = (int *)malloc(2*sizeof(int));
    city2 = (int *)malloc(2*sizeof(int));
    perm = genera_perm(NUMCITIES);
    for(i = 0; i < NUMCITIES; i++){
        pos = aleat_num(0, NUMCITIES-1);
        if(pos != i){
            city1[0] = cities[i][0];
            city1[1] = cities[i][1]; 
            city2[0] = cities[pos][0];
            city2[1] = cities[pos][1];
            if(map_joincitywithhighway(m, city1, city2) == ERROR)
                exit(EXIT_FAILURE);
        }
    }
    printf("1\n");

    for(i = 0; i < NUMVILLAGES; i++)
    {
        if(map_joinvillage(m, villages[i][0], villages[i][1]) == ERROR)
            exit(EXIT_FAILURE);
    }
        printf("1\n");

    printf("Voy a printear un mapa\n");
    map_print(m);
    map_savetofile(m, "prueba.txt");
    map_destruct(m);
    return 0;
}
